<section class="container mt-2">
	<h3>Editar studio</h3>
	<div class="form-row">
		<div class="alert col-md-12" role="alert" style="display: none" id="info">
			
		</div>
		<?php extract($dados); ?>
		<input type="hidden" id="id_studio" value="{{$studio->id}}">
		<div class="form-group col-md-10">
			<label for="studio">Studio:</label>
      		<input type="text" class="form-control" id="studio" autofocus="" value="{{$studio->studio}}">
		</div>
		<div class="form-group col-md-2 mt-4">
			<button id="salvar" class="btn btn-primary">Salvar</button>
		</div>
	</div>
</section>
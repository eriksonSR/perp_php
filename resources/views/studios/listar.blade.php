<section class="container mt-2">
	<h3>Listar</h3>
	<?php extract($dados); ?>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_studios">
			<thead>
				<th>ID</th>
				<th>Studio</th>
				<th>Editar</th>
			</thead>
			<tbody>
				@foreach ($studios as $s)
					<tr>
						<td>{{$s->id}}</td>
						<td>{{$s->studio}}</td>
						<td>
							<a href="{{route('studios.editar', ['id' => $s->id])}}" class="btn btn-success btn-sm text-center" target="_blank">
								<i class="fa fa-edit"></i>
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
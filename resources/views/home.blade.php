<?php
	use App\Tarefas;
	use App\Helpers\ListaTarefasHelper;
	extract($dados);
?>
<section class="mt-2 ml-2 mr-2">
	<ul class="nav nav-pills" id="myTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#dashboard" role="tab" aria-controls="dashboard" aria-selected="false">Dashboard</a>
		</li>
		<li class="nav-item">
    		<a class="nav-link active" data-toggle="tab" href="#tarefas" role="tab" aria-controls="tarefas" aria-selected="true">Tarefas</a>
		</li>
	</ul>
	<hr>
	<div class="tab-content">
		<div class="tab-pane fade" id="dashboard">
			<div class="row">
				<div class="col-md-7">
					<h4>Distâncias por período e exercício</h4>
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
						   	<a class="nav-link active" id="grafico_dist_p_periodo_e_exerc-tab" data-toggle="tab" href="#grafico_dist_p_periodo_e_exerc" role="tab" aria-controls="dist_p_periodo_e_exerc" aria-selected="true">
						   		Gráfico
						   	</a>
						</li>
						<li class="nav-item">
						    <a class="nav-link" id="tabela_dist_tot_p_exercio_ano-tab" data-toggle="tab" href="#tabela_dist_tot_p_exercio_ano" role="tab" aria-controls="dist_tot_p_exercio_ano" aria-selected="false">
						    	Tabela
						    </a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade show active" id="grafico_dist_p_periodo_e_exerc" role="tabpanel" aria-labelledby="grafico_dist_p_periodo_e_exerc-tab">
							<canvas id="canvas_dist_p_periodo_e_exerc" data-dist_p_periodo_e_exerc="{{json_encode($dist_p_periodo_e_exerc)}}"></canvas>
						</div>
						<div class="tab-pane fade show" id="tabela_dist_tot_p_exercio_ano" role="tabpanel" aria-labelledby="tabela_dist_tot_p_exercio_ano-tab">
							<?php
								$mes_corrente = '';
								$mes_anterior = '';
								$totais_mes = array();
							?>
							<table class="table table-striped table-hover">
								<thead>
									<th>Mês</th>
									<th>Tipo</th>
									<th>Distância(km)</th>
								</thead>
								<tbody>
									@foreach($dist_p_periodo_e_exerc as $pe)
										<?php
											if(isset($totais_mes[$pe->mes])) {
												$totais_mes[$pe->mes] += $pe->distancia;
											}else{
												$totais_mes[$pe->mes] = $pe->distancia;
											}

											if($mes_anterior != $pe->mes && $mes_anterior != ''){
										?>
											<tr>
												<td></td>
												<td></td>
												<td>{{$totais_mes[$mes_anterior]}}</td>
											</tr>
										<?php } ?>

										<?php $mes_anterior = $pe->mes; ?>
										<tr>
											<td>{{$pe->mes}}</td>
											<td>{{$pe->tipo}}</td>
											<td>{{$pe->distancia}}</td>
										</tr>
									@endforeach
									<tr>
										<td></td>
										<td></td>
										<td>{{$totais_mes[$mes_anterior]}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<h4>Distâncias totais por exercício</h4>
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
						   	<a class="nav-link active" id="grafico_dist_total_p_exerc-tab" data-toggle="tab" href="#grafico_dist_total_p_exerc" role="tab" aria-controls="dist_total_p_exerc" aria-selected="true">
						   		Gráfico
						   	</a>
						</li>
						<li class="nav-item">
						    <a class="nav-link" id="tabela_dist_total_p_exerc-tab" data-toggle="tab" href="#tabela_dist_total_p_exerc" role="tab" aria-controls="dist_dist_total_p_exerc" aria-selected="false">
						    	Tabela
						    </a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade show active" id="grafico_dist_total_p_exerc" role="tabpanel" aria-labelledby="dist_total_p_exerc-tab">
							<canvas id="canvas_dist_tot_p_exerc" data-dist_tot_p_exercio="{{json_encode($dist_tot_p_exercio)}}"></canvas>
						</div>
						<div class="tab-pane fade show" id="tabela_dist_total_p_exerc" role="tabpanel" aria-labelledby="tabela_dist_total_p_exerc-tab">
							<table class="table table-striped table-hover">
								<thead>
									<th>Ano</th>
									<th>Tipo</th>
									<th>Distância(km)</th>
								</thead>
								<tbody>
									<?php $dist_total = 0; ?>
									@foreach($dist_tot_p_exercio as $pe)
										<tr>
											<td>{{$pe->ano}}</td>
											<td>{{$pe->tipo}}</td>
											<td>{{$pe->distancia}}</td>
										</tr>
										<?php $dist_total += $pe->distancia; ?>
									@endforeach
									<tr>
										<td colspan="2">Total</td>
										<td>{{$dist_total}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<h4>Últimas leituras</h4>
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
					   		<a class="nav-link active" id="ult_livros-tab" data-toggle="tab" href="#ult_livros" role="tab" aria-controls="data_hist" aria-selected="true">
					   		Livros
					   		</a>
					  	</li>
						<li class="nav-item">
					    	<a class="nav-link" id="ult_hqs-tab" data-toggle="tab" href="#ult_hqs" role="tab" aria-controls="ult_hqs" aria-selected="false">
					    	Hqs
					    	</a>
						</li>
						<li class="nav-item">
					    	<a class="nav-link" id="ult_mangas-tab" data-toggle="tab" href="#ult_mangas" role="tab" aria-controls="ult_mangas" aria-selected="false">
					    	Mangás
					    	</a>
						</li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="ult_livros" role="tabpanel" aria-labelledby="ult_livros-tab">
							<table class="table">
								<thead>
									<tr>
										<th>Título</th><th>Data</th>
									</tr>
								</thead>
								<tbody>
									@foreach($ultimos_livros as $ul)
										<tr>
											<td>{{$ul->titulo}}</td>
											<td>{{PgUtils::DataPgToBr($ul->dt_finalizacao, '/')}}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="tab-pane fade" id="ult_hqs" role="tabpanel" aria-labelledby="ult_hqs-tab">
							<div class="tab-pane fade show active" id="ult_hqs" role="tabpanel" aria-labelledby="ult_hqs-tab">
								<table class="table">
									<thead>
										<tr>
											<th>Título</th><th>Data</th>
										</tr>
									</thead>
									<tbody>
										@foreach($ultimos_hqs as $uhq)
											<tr>
												<td>{{$uhq->titulo}}</td>
												<td>{{PgUtils::DataPgToBr($uhq->dt_finalizacao, '/')}}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane fade" id="ult_mangas" role="tabpanel" aria-labelledby="ult_mangas-tab">
							<div class="tab-pane fade show active" id="ult_mangas" role="tabpanel" aria-labelledby="ult_mangas-tab">
								<table class="table">
									<thead>
										<tr>
											<th>Título</th><th>Data</th>
										</tr>
									</thead>
									<tbody>
										@foreach($ultimos_mangas as $umg)
											<tr>
												<td>{{$umg->titulo}}</td>
												<td>{{PgUtils::DataPgToBr($umg->dt_finalizacao, '/')}}</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- Fim coluna últimas leituras -->
				<div class="col-md-4">
					<h4>Assuntos mais lidos</h4>
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
					   		<a class="nav-link active" id="assuntos_livros_mais_lidos_grafico-tab" data-toggle="tab" href="#assuntos_livros_mais_lidos_grafico" role="tab" aria-controls="assuntos_livros_mais_lidos_grafico" aria-selected="true">
					   		Livros
					   		</a>
					  	</li>
						<li class="nav-item">
					    	<a class="nav-link" id="assuntos_hqs_mais_lidos_grafico-tab" data-toggle="tab" href="#assuntos_hqs_mais_lidos_grafico" role="tab" aria-controls="assuntos_hqs_mais_lidos_grafico" aria-selected="false">
					    	Hqs
					    	</a>
						</li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active mt-4" id="assuntos_livros_mais_lidos_grafico" role="tabpanel" aria-labelledby="assuntos_livros_mais_lidos_grafico-tab">
							<canvas id="canvas_assuntos_livros_mais_lidos_grafico" data-dados_assuntos_livros_grafico="{{json_encode($assuntos_mais_lidos_livros)}}"></canvas>
						</div>
						<div class="tab-pane fade mt-4" id="assuntos_hqs_mais_lidos_grafico" role="tabpanel" aria-labelledby="assuntos_hqs_mais_lidos_grafico-tab">
							<canvas id="canvas_assuntos_hqs_mais_lidos_grafico" data-dados_assuntos_hqs_grafico="{{json_encode($assuntos_mais_lidos_hqs)}}"></canvas>
						</div>
					</div>
				</div><!-- Fim coluna assuntos mais lidos -->
				<div class="col-md-4">
					<h4>Principais gastos</h4>
					<div>
						<canvas id="canvas_principais_gastos_grafico" data-dados_principais_gastos_grafico="{{json_encode($principais_gastos)}}"></canvas>
					</div>
				</div><!-- Fim coluna principais gastos -->
			</div><!-- Fim da primeira linha -->
			<hr>
			<div class="row">
				<div class="col-md-4">
					<h4>Últimos games</h4>
					<table class="table">
						<thead>
							<tr>
								<th>Título</th><th>Data</th>
							</tr>
						</thead>
						<tbody>
							@foreach($ultimos_games as $ug)
								<tr>
									<td>{{$ug->titulo}}</td>
									<td>{{PgUtils::DataPgToBr($ug->dt_finalizacao, '/')}}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="col-md-4">
					<h4>Consoles mais jogados</h4>
					<canvas id="canvas_consoles_mais_jogados_grafico" data-dados_consoles_mais_jogados_grafico="{{json_encode($consoles_mais_jogados)}}"></canvas>
				</div>
			</div><!-- Fim da segunda linha -->
			<div class="row">
				<div class="col-md-6">
					<h4>Midias por mês</h4>
					<canvas id="canvas_midias_fin_p_mes" data-dados_livros_finalizados_p_mes="{{json_encode($livros_fin_p_mes)}}"  data-dados_hqs_finalizados_p_mes="{{json_encode($hqs_fin_p_mes)}}" data-dados_mangas_finalizados_p_mes="{{json_encode($mangas_fin_p_mes)}}" data-dados_games_finalizados_p_mes="{{json_encode($games_fin_p_mes)}}"></canvas>
				</div>
				<div class="col-md-6">
					<h4>Midias finalizadas x não iniciadas</h4>
					<canvas id="canvas_midias_fin_x_nao_fin" data-dados_midias_fin_x_nao_fin="{{json_encode($midias_finalizadas_x_nao_finalizadas)}}"></canvas>
				</div>
			</div><!-- Fim da terceira linha -->
		</div><!-- Fim do conteúdo da aba Dashboard -->
		<div class="tab-pane fade show active" id="tarefas">
			<div class="row ml-1">
				@foreach($listas_tarefas as $lt)
					<?php 
						$tarefas = Tarefas::where('id_lista', $lt->id)->get();
						$cor_lista = ListaTarefasHelper::CorDaLista($tarefas);
						if (date('Y-m-d') == $lt->data) {
							$cor_lista = 'secondary';
						}
					?>
					<div class="col-md-4">
						<div class="alert alert-{{$cor_lista}}">
						<h5 class="pt-2">{{PgUtils::DataPgToBr($lt->data, '/')}} -> {{$lt->dia}}</h5>
						<table class="table">
							<thead>
								<tr>
									<th>Tarefa</th>
									<th>Situação</th>
								</tr>
							</thead>
							<tbody>
								@foreach($tarefas as $t)
									<tr>
										<td>{{$t->tarefa}}</td>
										<td class="text-center">
											<select class="form-control" onchange="MudarSituacaoTarefa(this)" data-id_tarefa="{{$t->id}}">
												@foreach($situacoes_tarefas as $s)
													@if($t->id_situacao == $s->id)
														<option value="{{$s->id}}" selected="">{{$s->situacao}}</option>
													@else
														<option value="{{$s->id}}">{{$s->situacao}}</option>
													@endif
												@endforeach
											</select>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div><!-- Fim do conteudo das abas -->
</section>
<?php
	use App\Helpers\Datas;
	extract($dados); 
?>
<section class="container mt-2">
	<h3>Listar</h3>
	<button class="btn btn-primary ml-2 mb-2" id="bt_filtros">Filtros</button>
	<button class="btn btn-primary ml-2 mb-2" id="bt_modal_add_exercicio">Add exercício</button>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_pesos">
			<thead>
				<th>Data</th>
				<th>Tipo</th>
				<th>Distância</th>
				<th>Tempo</th>
				<th>Obs</th>
				<th>Açoes</th>
			</thead>
			<tbody>
				@foreach($exercicios as $e)
					<tr>
						<td>{{Datas::GetDiaDaSemana($e->data)['extenso']}}, {{PgUtils::DataPgToBr($e->data, '/')}}
						</td>
						<td>{{$e->TipoExercicio->tipo}}</td>
						<td>{{str_replace('.', ',', $e->distancia)}} Km</td>
						<td>{{$e->tempo}}</td>
						<td>{{$e->obs}}</td>
						<td>
							<button class="btn btn-danger btn-sm text-center" onclick="Excluir({{$e->id}}, this, '{{str_replace('.', ',', $e->TipoExercicio->tipo)}}');">
								<i class="fa fa-remove"></i>
							</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
@include('pesos.md_add_peso')
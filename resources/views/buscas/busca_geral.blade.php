<section class="container mt-2">
	<?php extract($dados); ?>
	<div class="form-inline">Termo buscado: <h3 class="ml-2">{{$termo}}</h3></div>
	<div class="row">
		<div class="form-inline"><h5>{{count($games)}} games encontrados</h5></div>
		<table class="table table-striped table-hover" id="tb_games">
			<thead>
				<th>Titulo</th>
				<th>Console</th>
				<th>Editar</th>
				<th>Ficha</th>
			</thead>
			<tbody>
				@foreach ($games as $g)
					<tr>
						<td>{{$g->titulo}}</td>
						<td>{{$g->console}}</td>
						<td>
							<a href="{{route('games.editar', ['id' => $g->id])}}" class="btn btn-success btn-sm text-center" target="_blank">
								<i class="fa fa-edit"></i>
							</a>
						</td>
						<td>
							<a href="{{route('games.ficha', ['id' => $g->id])}}" class="btn btn-success btn-sm text-center" target="_blank">
								<i class="fa fa-search"></i>
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row mt-2">
		<div class="form-inline"><h5>{{count($leituras)}} leituras encontradas</h5></div>
		<table class="table table-striped table-hover" id="tb_leituras">
			<thead>
				<th>Titulo</th>
				<th>Tipo</th>
				<th>Situação</th>
				<th>Editar</th>
				<th>Ficha</th>
			</thead>
			<tbody>
				@foreach ($leituras as $l)
					<tr>
						<td>{{$l->titulo}}</td>
						<td>{{$l->tipo}}</td>
						<td>{{$l->situacao}}</td>
						<td>
							<a href="{{route('leituras.editar', ['id' => $l->id])}}" class="btn btn-success btn-sm text-center bt_lancamentos" target="_blank">
								<i class="fa fa-edit"></i>
							</a>
						</td>
						<td>
							<a href="{{route('leituras.ficha', ['id' => $l->id])}}" class="btn btn-success btn-sm text-center bt_lancamentos" target="_blank">
								<i class="fa fa-search"></i>
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row mt-2">
		<div class="form-inline"><h5>{{count($lancamentos)}} lançamentos encontradas</h5></div>
		<table class="table table-striped table-hover" id="tb_lancamentos">
			<thead>
				<th>Conta creditada</th>
				<th>Conta debitada</th>
				<th>Histórico</th>
				<th>Valor</th>
				<th>Data</th>
			</thead>
			<tbody>
				@foreach ($lancamentos as $l)
					<tr>
						<td>{{$l->c_cre}}</td>
						<td>{{$l->c_deb}}</td>
						<td>{{$l->historico}}</td>
						<td>RS {{number_format($l->valor, 2,',','.')}}</td>
						<td>{{$l->dt}}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
<section class="container mt-2">
	<h3>Editar editora</h3>
	<div class="form-row">
		<div class="alert col-md-12" role="alert" style="display: none" id="info">
			
		</div>
		<?php extract($dados); ?>
		<input type="hidden" id="id_editora" value="{{$editora->id}}">
		<div class="form-group col-md-10">
			<label for="editora">Autor:</label>
      		<input type="text" class="form-control" id="editora" autofocus="" value="{{$editora->editora}}">
		</div>
		<div class="form-group col-md-2 mt-4">
			<button id="salvar" class="btn btn-primary">Salvar</button>
		</div>
	</div>
</section>
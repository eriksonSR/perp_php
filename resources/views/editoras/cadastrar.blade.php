<section class="container mt-2">
	<h3>Cadastrar editora</h3>
	<div class="form-row">
		<div class="alert col-md-12" role="alert" style="display: none" id="info">
			
		</div>
		<div class="form-group col-md-10">
			<label for="editora">Editora:</label>
      		<input type="text" class="form-control" id="editora" autofocus="">
		</div>
		<div class="form-group col-md-2 mt-4">
			<button id="cadastrar" class="btn btn-primary">Cadastrar</button>
		</div>
	</div>
</section>
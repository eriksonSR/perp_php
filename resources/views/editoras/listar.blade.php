<section class="container mt-2">
	<h3>Listar</h3>
	<?php extract($dados); ?>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_editoras">
			<thead>
				<th>ID</th>
				<th>Editora</th>
				<th>Editar</th>
			</thead>
			<tbody>
				@foreach ($editoras as $e)
					<tr>
						<td>{{$e->id}}</td>
						<td>{{$e->editora}}</td>
						<td>
							<a href="{{route('editoras.editar', ['id' => $e->id])}}" class="btn btn-success btn-sm text-center" target="_blank">
								<i class="fa fa-edit"></i>
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
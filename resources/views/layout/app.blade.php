<!DOCTYPE html>
<html>
<head>
	<title>PERP</title>
	<link rel="stylesheet" type="text/css" href="<?= asset('css/bootstrap.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= asset('css/app.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= asset('css/jquery-ui.min.css') ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" crossorigin="anonymous">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
	<meta name="csrf-token" content="<?= csrf_token() ?>" id="csrf-token">
</head>
<body>
	@include('layout.menu')
	@include('layout.localizacao')
	@if(isset($view))
		@include($view)
	@else
		@include('layout.inicial')
	@endif
	@include('layout.scripts')
</body>
</html>
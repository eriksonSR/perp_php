<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<?= asset('js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?= asset('js/jquery-ui.min.js') ?>"></script>
<script type="text/javascript" src="<?= asset('js/app.js') ?>"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@if(isset($scripts))
	@foreach($scripts as $script)
		@if($script['tipo'] == 'local')
			<script type="text/javascript" src="<?= asset("js/{$script['caminho']}.js") ?>"></script>
		@else
			<script type="text/javascript" src="<?= $script['caminho'] ?>"></script>
		@endif
	@endforeach
@endif
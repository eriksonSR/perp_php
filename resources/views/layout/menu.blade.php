<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="<?= route('home') ?>">PERP</a>
	<div class="container">
		<button class="navbar-toggler" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse">
			<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
						Relatórios
					</a>
					<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Games</a>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="<?= route('games.dlc_vs_jogo_base') ?>">DLC vs Jogo Base</a></li>
								<li><a class="dropdown-item" href="<?= route('games.consolidado_por_ano') ?>">Consolidado por ano</a></li>
								<li><a class="dropdown-item" href="<?= route('games.consolidado_por_console_ao_ano') ?>">Consolidado por console ao ano</a></li>
								<li><a class="dropdown-item" href="<?= route('games.consolidado_por_desenvolvedora') ?>">Consolidado por Desenvolvedora</a></li>
								<li><a class="dropdown-item" href="<?= route('games.consolidado_por_genero_ao_ano') ?>">Consolidado por gênero ao ano</a></li>
								<li><a class="dropdown-item" href="<?= route('games.consolidado_por_mes') ?>">Consolidado por mês</a></li>
								<li><a class="dropdown-item" href="<?= route('games.consolidado_por_plataforma_ao_ano') ?>">Consolidado por plataforma ao ano</a></li>
								<li><a class="dropdown-item" href="<?= route('games.consolidado_por_publicadora_ao_ano') ?>">Consolidado por publicadora ao ano</a></li>
								<li><a class="dropdown-item" href="<?= route('games.consolidado_por_portatil_vs_mesa_ao_ano') ?>">Consolidado portátil vs mesa</a></li>
							</ul>
						</li>
						<li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Leituras</a>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="<?= route('leituras.consolidado_por_ano') ?>">Consolidado por ano</a></li>
								<li><a class="dropdown-item" href="<?= route('leituras.consolidado_por_assunto_ao_ano') ?>">Consolidado por assunto ano</a></li>
								<li><a class="dropdown-item" href="<?= route('leituras.consolidado_por_autor') ?>">Consolidado por autor</a></li>
								<li><a class="dropdown-item" href="<?= route('leituras.consolidado_por_editora') ?>">Consolidado por editora</a></li>
								<li><a class="dropdown-item" href="<?= route('leituras.consolidado_por_formato_ao_ano') ?>">Consolidado por formato ao ano</a></li>
								<li><a class="dropdown-item" href="<?= route('leituras.consolidado_por_mes') ?>">Consolidado por mês</a></li>
								<li><a class="dropdown-item" href="<?= route('leituras.consolidado_por_tipo_midia_ao_mes') ?>">Consolidado por tipo mídia ao mês</a></li>
							</ul>
						</li>
						<li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Contábil</a>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="<?= route('relatorios.gastos_conta_periodo') ?>">Gastos por conta e periodo</a></li>
							</ul>
						</li>
						<li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Gráficos</a>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="<?= route('graficos.games_e_leituras_por_mes') ?>">Games e leituras por mês</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
						Contábil
					</a>
					<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<li class="dropdown-submenu">
							<a class="dropdown-item dropdown-toggle" href="#">Plano de contas</a>
							<ul class="dropdown-menu">
								<li>
									<a class="dropdown-item" href="<?= route('plano_contas.cadastrar') ?>">Cadastrar
									</a>
								</li>
								<li>
									<a class="dropdown-item" href="<?= route('plano_contas.listar') ?>">Listar
									</a>
								</li>
							</ul>
						</li>
						<li class="dropdown-submenu">
							<a class="dropdown-item dropdown-toggle" href="#">Lançamentos</a>
							<ul class="dropdown-menu">
								<li>
									<a class="dropdown-item" href="<?= route('lancamentos.cadastrar') ?>">Cadastrar
									</a>
								</li>
								<li>
									<a class="dropdown-item" href="<?= route('lancamentos.listar') ?>">Listar
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
						Tarefas
					</a>
					<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<li class="dropdown-submenu">
							<a class="dropdown-item dropdown-toggle" href="#">Listas</a>
							<ul class="dropdown-menu">
								<li>
									<a class="dropdown-item" href="<?= route('listas.cadastrar') ?>">Cadastrar
									</a>
								</li>
								<li>
									<a class="dropdown-item" href="<?= route('listas.listar') ?>">Listar
									</a>
								</li>
							</ul>
						</li>
						<li class="dropdown-submenu">
							<a class="dropdown-item dropdown-toggle" href="#">Categorias</a>
							<ul class="dropdown-menu">
								<li>
									<a class="dropdown-item" href="<?= route('categorias_tarefa.cadastrar') ?>">Cadastrar
									</a>
								</li>
								<li>
									<a class="dropdown-item" href="<?= route('categorias_tarefa.listar') ?>">Listar
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
						Leituras
					</a>							
					<ul class="dropdown-menu">
						<li>
							<a class="dropdown-item" href="<?= route('leituras.cadastrar') ?>">Cadastrar</a>
							<a class="dropdown-item" href="<?= route('leituras.listar') ?>">Listar</a>
						</li>
					</ul>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
						Games
					</a>							
					<ul class="dropdown-menu">
						<li>
							<a class="dropdown-item" href="<?= route('games.cadastrar') ?>">Cadastrar</a>
						</li>
						<li>
							<a class="dropdown-item" href="<?= route('games.listar') ?>">Listar</a>
						</li>
					</ul>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
						Autores
					</a>							
					<ul class="dropdown-menu">
						<li>
							<a class="dropdown-item" href="<?= route('autores.cadastrar') ?>">Cadastrar</a>
						</li>
						<li>
							<a class="dropdown-item" href="<?= route('autores.listar') ?>">Listar</a>
						</li>
					</ul>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
						Studios
					</a>							
					<ul class="dropdown-menu">
						<li>
							<a class="dropdown-item" href="<?= route('studios.cadastrar') ?>">Cadastrar</a>
						</li>
						<li>
							<a class="dropdown-item" href="<?= route('studios.listar') ?>">Listar</a>
						</li>
					</ul>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
						Editoras
					</a>							
					<ul class="dropdown-menu">
						<li>
							<a class="dropdown-item" href="<?= route('editoras.listar') ?>">Listar</a>
						</li>
						<li>
							<a class="dropdown-item" href="<?= route('editoras.cadastrar') ?>">Cadastrar</a>
						</li>
					</ul>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?= route('pesos.listar') ?>">Pesos</a>
				</li>
			</ul>
			<div class="form-inline">
				<input class="form-control mr-sm-2" type="search" placeholder="Busca..." id="busca_geral">
				<button class="btn btn-success btn-sm text-center" id="bt_busca_geral" onclick="EfetuarBuscaGeral()">
					<i class="fa fa-search"></i>
				</button>
			</div>
		</div>
	</div>
</nav>
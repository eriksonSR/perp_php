<section class="container mt-2">
	<h3>Listar</h3>
	<?php extract($dados); ?>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_categorias_tarefas">
			<thead>
				<th>Categoria</th>
				<th>Editar</th>
			</thead>
			<tbody>
				@foreach ($categorias as $c)
					<tr data-id_categoria="{{$c->id}}">
						<td>{{$c->categoria}}</td>
						<td>
							<button class="btn btn-success btn-sm text-center" onclick="ExibeFormEditaCategoria('{{$c->id}}')">
								<i class="fa fa-edit"></i>
							</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
@include('listas_tarefas.categorias.md_editar_categoria')
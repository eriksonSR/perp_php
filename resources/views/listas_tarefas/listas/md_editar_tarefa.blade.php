<div class="modal fade" id="md_editar_tarefa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar tarefa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
          <input type="hidden" id="frm_edit_tarefa_id" value="">
          <div class="form-group col-md-12">
            <label for="lista">Tarefa:</label>
            <input type="text" class="form-control" id="frm_edit_tarefa_tarefa" value="">
          </div>
          <div class="form-group col-md-12">
            <label for="valor">Categoria:</label>
            <select class="form-control" id="frm_edit_tarefa_categoria">
              @foreach($categorias as $c)
                <option value="{{$c->id}}">{{$c->categoria}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-12">
            <label for="valor">Situações:</label>
            <select class="form-control" id="frm_edit_tarefa_situacao">
              @foreach($situacoes_tarefas as $st)
                <option value="{{$st->id}}">{{$st->situacao}}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary" id='bt_atualizar_tarefa'>Salvar</button>
      </div>
    </div>
  </div>
</div>
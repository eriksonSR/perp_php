<div class="modal fade" id="md_add_tarefa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Adicionar tarefa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
          <input type="hidden" name="inp_id_lista" id="inp_id_lista">
          <div class="form-group col-md-12">
            <label for="inp_tarefa">Tarefa:</label>
            <input type="text" class="form-control" id="inp_tarefa" value="">
          </div>
          <div class="form-group col-md-12">
            <label for="inp_categoria">Categoria:</label>
            <select class="form-control" id="inp_categoria">
              @foreach($categorias as $c)
                <option value="{{$c->id}}">{{$c->categoria}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-12">
            <label for="inp_situacao">Situação:</label>
            <select class="form-control" id="inp_situacao">
              @foreach($situacoes_tarefas as $st)
                <option value="{{$st->id}}">{{$st->situacao}}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary" id='bt_salvar_tarefa'>Salvar</button>
      </div>
    </div>
  </div>
</div>
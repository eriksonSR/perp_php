<div class="modal fade" id="md_listar_tarefas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5><span id="titulo_lista"></span> -> <span id="data_lista"></span> -> <span id="dia_lista"></span></h5>
      </div>
      <div class="modal-body">
        <div class="form-inline"><h5>Tarefas</h5></div>
        <div class="row">
          <table class="table table-striped table-hover" id="tb_tarefas">
            <thead>
              <th>Tarefa</th>
              <th>Categoria</th>
              <th>Situação</th>
              <th>Ações</th>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <button class="btn btn-success btn-sm text-center" id="inp_add_tarefa">Add tarefa</button>
      </div>
    </div>
  </div>
</div>
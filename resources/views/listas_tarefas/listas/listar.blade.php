<section class="container mt-2">
	<h3>Listar</h3>
	<?php extract($dados); ?>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_games">
			<thead>
				<th>Lista</th>
				<th>Data</th>
				<th>Dia</th>
				<th>Tarefas</th>
				<th>Editar</th>
			</thead>
			<tbody>
				@foreach ($listas as $l)
					<tr data-id_lista="{{$l->id}}">
						<td>{{$l->lista}}</td>
						<td>{{PgUtils::DataPgToBr($l->data, '/')}}</td>
						<td>{{$l->dia}}</td>
						<td>
							<button class="btn btn-success btn-sm text-center" onclick="ExibeModalTarefas('{{$l->id}}')">
								<i class="fa fa-th-list"></i>
							</button>
						</td>
						<td>
							<button class="btn btn-success btn-sm text-center" onclick="ExibeFormEditaLista('{{$l->id}}')">
								<i class="fa fa-edit"></i>
							</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
@include('listas_tarefas.listas.md_editar_lista')
@include('listas_tarefas.listas.md_listar_tarefas')
@include('listas_tarefas.listas.md_editar_tarefa')
@include('listas_tarefas.listas.md_add_tarefa')
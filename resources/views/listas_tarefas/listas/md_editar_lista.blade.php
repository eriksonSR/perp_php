<div class="modal fade" id="md_editar_lista" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar lista</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
          <input type="hidden" id="frm_id_lista" value="">
          <div class="form-group col-md-12">
            <label for="lista">Lista:</label>
            <input type="text" class="form-control" id="frm_edit_lista" value="">
          </div>
          <div class="form-group col-md-12">
            <label for="data">Data:</label>
            <input type="text" class="form-control data" id="frm_edit_data" value="">
          </div>
          <div class="form-group col-md-12">
            <label for="valor">Dia:</label>
            <select class="form-control" id="dia">
              @foreach($dias_semana as $d)
                <option value="{{$d->dia_abv}}">{{$d->dia}}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary" id='bt_salvar_lista'>Salvar</button>
      </div>
    </div>
  </div>
</div>
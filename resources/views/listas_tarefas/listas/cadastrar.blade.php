<?php
	extract($dados);
?>
<section class="container mt-2">
	<h3>Cadastrar lista</h3>
	<div class="row">
		<div class="col-md-7">
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item">
			   		<a class="nav-link active" id="lista-tab" data-toggle="tab" href="#cont_lista" role="tab" aria-controls="cont_lista" aria-selected="true">Lista</a>
			  	</li>
				<li class="nav-item">
			    	<a class="nav-link" id="tarefa-tab" data-toggle="tab" href="#cont_tarefa" role="tab" aria-controls="cont_tarefa" aria-selected="false">Tarefas</a>
				</li>
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="cont_lista" role="tabpanel" aria-labelledby="lista-tab">
					<div class="row mt-3">
						<div class="alert col-md-12" role="alert" style="display: none" id="info">
							
						</div>
						<div class="form-group col-md-12">
							<label for="lista">Lista:</label>
							<input type="text" class="form-control" id="lista">
						</div>
						<div class="form-group col-md-12">
							<label for="data">Data:</label>
				      		<input type="text" class="form-control" id="data" autocomplete="off">
						</div>
						<div class="form-group col-md-12">
							<label for="valor">Dia:</label>
							<select class="form-control" id="dia">
								@foreach($dias_semana as $d)
									<option value="{{$d->dia_abv}}">{{$d->dia}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group col-md-6">
							<button id="cadastrar" class="btn btn-primary">Cadstrar</button>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="cont_tarefa" role="tabpanel" aria-labelledby="tarefa-tab">
					<div class="mt-3">
						<div class="form-group">
							<button id="add_tarefa" class="btn btn-primary">Add tarefa</button>
						</div>
						<table id="tb_tarefas" class="table">
							<thead>
								<tr>
									<th>Tarefa</th><th>Categoria</th><th>Excluir</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<table id="container_ult_listas" class="table">
				<thead>
					<tr>
						<th>Lista</th><th>Data</th><th>Dia</th>
					</tr>
				</thead>
				<tbody>
					@foreach($ult_listas as $l)
						<tr>
							<td>{{$l->lista}}</td>
							<td>{{PgUtils::DataPgToBr($l->data, '/')}}</td>
							<td>{{$l->dia}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>
@include('listas_tarefas.listas.md_add_tarefa')
@include('games.md_lista')
<section class="container mt-2">
	<h3>Consolidado por plataforma ao ano</h3>
	<button class="btn btn-primary ml-2 mb-2" id="bt_filtros">Filtros</button>
	<div class="row jumbotron" style="display: none;" id="div_filtros">
		<div class="form-inline">
			<label>Plataformas:</label>
			<select class="form-control mb-2 ml-sm-2 mr-sm-2" id="plataformas" multiple="">
				@foreach ($plataformas as $p)
					<option value="{{$p->id}}">{{$p->plataforma}}</option>
				@endforeach
			</select>
			<label for="ano" class="mr-sm-2">Ano:</label>
			<input type="text" class="form-control mb-2 mr-sm-2" id="ano">
	  		<button class="btn btn-primary mb-2" id="bt_filtrar">Filtrar</button>
	  		<button class="btn btn-primary ml-2 mb-2" id="resetar_filtros">Resetar</button>
		</div>
	</div>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_relatorio">
			<thead>
				<th>PLATAFORMA</th>
				<th>ANO</th>
				<th>TOTAL</th>
				<th class="text-center">VISUALIZAR</th>
			</thead>
			<tbody>
				@foreach($dados as $d)
					<tr>
						<td>{{$d->plataforma}}</td>
						<td>{{$d->ano}}</td>
						<td>{{$d->total}}</td>
						<td class="text-center"><span class="fas fa-search" onclick="visualizar_games('{{$d->plataforma}}','{{$d->ano}}')"></span></td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
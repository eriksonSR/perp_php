@include('games.md_lista')
<section class="container mt-2">
	<h3>Consolidado por desenvolvedora</h3>
	<button class="btn btn-primary ml-2 mb-2" id="bt_filtros">Filtros</button>
	<div class="row jumbotron" style="display: none;" id="div_filtros">
		<div class="form-inline">
			<label>Console:</label>
			<select class="form-control mb-2 ml-sm-2 mr-sm-2" id="desenvolvedoras" multiple="">
				@foreach ($desenvolvedoras as $d)
					<option value="{{$d->id}}">{{$d->studio}}</option>
				@endforeach
			</select>
	  		<button class="btn btn-primary mb-2" id="bt_filtrar">Filtrar</button>
	  		<button class="btn btn-primary ml-2 mb-2" id="resetar_filtros">Resetar</button>
		</div>
	</div>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_relatorio">
			<thead>
				<th>DESENVOLVEDORA</th>
				<th>TOTAL</th>
				<th class="text-center">VISUALIZAR</th>
			</thead>
			<tbody>
				@foreach ($dados as $d)
					<tr>
						<td>{{$d->desenvolvedora}}</td>
						<td>{{$d->total}}</td>
						<td class="text-center"><span class="fas fa-search" onclick="visualizar_games('{{$d->desenvolvedora}}')"></span></td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
@include('games.md_lista')
<section class="container mt-2">
	<h3>DLC vs Jogo Base</h3>
	<div class="row">
		<table class="table table-striped table-hover">
			<thead>
				<th>TIPO</th>
				<th>TOTAL</th>
				<th>VISUALIZAR</th>
			</thead>
			<tbody>
				@foreach ($dados as $d)
					<tr>
						<td>{{$d->tipo}}</td>
						<td>{{$d->total}}</td>
						@if ($d->tipo == 'DLC')
							<td><span class="fas fa-search bt_visualizar" data-tipo="1"></span></td>
						@else
							<td><span class="fas fa-search bt_visualizar" data-tipo="0"></span></td>
						@endif
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
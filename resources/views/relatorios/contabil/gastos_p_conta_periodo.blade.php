<?php
use App\Helpers\Datas;
?>
<section class="container mt-2">
	<h3>Consolidado por período</h3>
	<button class="btn btn-primary ml-2 mb-2" id="bt_filtros">Filtros</button>
	<div class="row jumbotron" style="display: none;" id="div_filtros">
		<div class="form-group col-md-2">
			<label>Ano:</label>
			<input class="form-control" type="text" id="ano">
		</div>
		<div class="form-group col-md-6">
			<label>Conta creditada:</label>
			<select class="form-control mb-2" id="id_conta" multiple="">
				<option value="">Selecione a conta...</option>
				@foreach ($dados['contas'] as $c)
					@if($c['tipo'] === 'd' || $c['analitica'] === 1)
						<option value="{{$c['id']}}">{{$c['grau']}} - {{$c['conta']}}</option>>
					@endif
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2 mt-4">
			<button id="bt_filtrar" class="btn btn-primary">Filtrar</button>
		</div>
		<div class="form-group col-md-2 mt-4">
			<button id="bt_resetar" class="btn btn-primary">Resetar</button>
		</div>
	</div>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_relatorio">
			<thead>
				<th class="text-center">CONTA</th>
				<th class="text-center">ANO</th>
				<th class="text-center">MÊS</th>
				<th class="text-center">VALOR</th>
				<th class="text-center">LANÇAMENTOS</th>
			</thead>
			<tbody>
				@foreach($dados['gastos'] as $g)
					<tr>
						<td>{{$g->conta}}</td>
						<td class="text-center">{{$g->ano}}</td>
						<td class="text-center">{{Datas::GetMesExtensoByNumMes($g->mes)}}</td>
						<td class="text-center">{{$g->valor}}</td>
						<td class="text-center">
							<button class="btn btn-success btn-sm text-center bt_visualizar" onclick="ExibeGastos('{{$g->conta}}', '{{$g->id}}', '{{$g->ano}}', '{{$g->mes}}')"><i class="fa fa-search"></i></button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
@include('relatorios.contabil.md_lista_gastos_p_conta_periodo')
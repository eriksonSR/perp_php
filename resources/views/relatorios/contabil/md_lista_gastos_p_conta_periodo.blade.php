<div class="modal fade" id="md_lista_gastos_p_conta_periodo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Lançamentos -> <span id="md_lista_lanc_conta"></span>, em <span id="md_lista_lanc_mes"></span>/<span id="md_lista_lanc_ano"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <table class="table table-striped table-hover" id="tb_lancamentos">
            <thead>
              <th>Historico</th>
              <th>Valor</th>
              <th>Data</th>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
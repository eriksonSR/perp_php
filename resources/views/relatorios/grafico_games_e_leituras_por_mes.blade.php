<section class="container mt-2">
	<h3>Games e leituras por mês</h3>
	<div class="form-inline">
		<label for="ano" class="mr-sm-2">Ano:</label>
		<input type="text" class="form-control mb-2 mr-sm-2" id="ano" value="<?= date("Y") ?>">
	  	<button class="btn btn-primary ml-2 mb-2" id="bt_gera_grafico">Gráfico</button>
	</div>
	<div class="row">
		<canvas id="grafico_games_e_leituras"></canvas>
	</div>
</section>
<section class="container mt-2">
	<h3>Leituras</h3>
	<button class="btn btn-primary ml-2 mb-2" id="bt_filtros">Filtros</button>
	<a href="{{route('leituras.exp_tabela')}}" class="btn btn-success ml-2 mb-2" target="_blank" id="exp_tabela">Exportar tabela</a>
	<?php extract($dados); ?>
	<div class="form-row jumbotron" style="display: none;" id="div_filtros">
		<div class="form-group col-md-4">
			<label>Titulo:</label>
			<input class="form-control" type="text" id="titulo">
		</div>
		<div class="form-group col-md-4">
			<label>Subtitulo:</label>
			<input class="form-control" type="text" id="subtitulo">
		</div>
		<div class="form-group col-md-4">
			<label>Autores:</label>
			<select class="form-control" id="ids_autores" multiple="">
				<option value="0">Selecione...</option>
				@foreach ($autores as $a)
					<option value="{{$a->id}}">{{$a->autor}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Editoras:</label>
			<select class="form-control" id="ids_editoras" multiple="">
				<option value="0">Selecione...</option>
				@foreach ($editoras as $e)
					<option value="{{$e->id}}">{{$e->editora}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label>Assuntos:</label>
			<select class="form-control mb-2" id="ids_assuntos" multiple="">
				<option value="0">Selecione...</option>
				@foreach ($assuntos as $a)
					<option value="{{$a->id}}">{{$a->assunto}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label>Tipo mídia:</label>
			<select class="form-control mb-2" id="id_tipo_midia">
				<option value="0">Selecione...</option>
				@foreach ($tipos_midia as $tm)
					<option value="{{$tm->id}}">{{$tm->tipo_midia}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Situação:</label>
			<select class="form-control mb-2" id="id_situacao">
				<option value="0">Selecione...</option>
				@foreach ($situacoes as $s)
					<option value="{{$s->id}}">{{$s->situacao}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-1">
			<input type="checkbox" class="form-check-label mt-5" id="favorito" value="1">
			<label for="digital">Favorito</label>
		</div>
		<div class="form-group col-md-1">
			<input type="checkbox" class="form-check-label mt-5" id="digital" value="1">
			<label for="digital">Digital</label>
		</div>
		<div class="form-group col-md-3">
			<label>Critério dt. inicial finalização:</label>
			<select class="form-control" id="criterio_ini_dt_finalizacao">
				<option value=""></option>
				<option value=">">></option>
				<option value="<"><</option>
				<option value="=">=</option>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Data inicial:</label>
			<input class="form-control data" type="text" id="data_ini">
		</div>
		<div class="form-group col-md-3">
			<label>Critério dt. final finalização:</label>
			<select class="form-control" id="criterio_final_dt_finalizacao">
				<option value=""></option>
				<option value=">">></option>
				<option value="<"><</option>
				<option value="=">=</option>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Data final:</label>
			<input class="form-control data" type="text" id="data_final">
		</div>
		<div class="form-group col-md-2">
			<button id="bt_filtrar" class="btn btn-primary">Filtrar</button>
		</div>
		<div class="form-group col-md-2">
			<button id="bt_resetar" class="btn btn-primary">Resetar</button>
		</div>
		<div class="form-group col-md-2">
			<button id="bt_excel" class="btn btn-success">Excel</button>
		</div>
	</div>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_leituras">
			<thead>
				<th>Titulo</th>
				<th>Autores</th>
				<th>Editoras</th>
				<th>Tipo</th>
				<th>Situação</th>
				<th>Formato</th>
				<th>Editar</th>
				<th>Ficha</th>
			</thead>
			<tbody>
				@foreach ($leituras as $l)
					<tr>
						<td>{{$l->titulo}}</td>
						<td>{{$l->autores}}</td>
						<td>{{$l->editoras}}</td>
						<td>
							@if(isset($l->TipoMidia->tipo_midia))
								{{$l->TipoMidia->tipo_midia}}
							@endif
						</td>
						<td>{{$l->Situacao->situacao}}</td>
						<td>
							@if($l->digital == 1)
								Digital
							@else
								Fisíco
							@endif
						</td>
						<td>
							<a href="{{route('leituras.editar', ['id' => $l->id])}}" class="btn btn-success btn-sm text-center bt_lancamentos" target="_blank">
								<i class="fa fa-edit"></i>
							</a>
						</td>
						<td>
							<a href="{{route('leituras.ficha', ['id' => $l->id])}}" class="btn btn-success btn-sm text-center bt_lancamentos" target="_blank">
								<i class="fa fa-search"></i>
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
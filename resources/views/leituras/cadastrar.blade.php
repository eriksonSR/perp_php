<section class="container mt-2">
	@include('autores.md_add_autor')
	@include('editoras.md_add_editora')
	@include('assuntos.md_add_assunto')
	<h3>Cadastrar leitura</h3>
	<div class="form-row">
		<div class="alert col-md-12" role="alert" style="display: none" id="info">
			
		</div>
		<div class="form-group col-md-2">
			<label for="id_skoob">ID Skoob:</label>
      		<input type="text" class="form-control" id="id_skoob" autofocus="">
		</div>
		<div class="col-md-2 mt-4 mr-auto">
			<button id="dwd_dados_skoob" class="btn btn-primary">Baixar dados do Skoob</button>
		</div>
		<div class="col-md-2 mt-4 mr-auto">
			<button id="add_autor" class="btn btn-primary">Add autor</button>
		</div>
		<div class="col-md-2 mt-4 mr-auto">
			<button id="add_assunto" class="btn btn-primary">Add assunto</button>
		</div>
		<div class="col-md-2 mt-4 mr-auto">
			<button id="add_editora" class="btn btn-primary">Add editora</button>
		</div>
		<div class="form-group col-md-5">
			<label for="titulo">Titulo:</label>
      		<input type="text" class="form-control" id="titulo">
		</div>
		<div class="form-group col-md-5">
			<label for="subtitulo">Subtitulo:</label>
      		<input type="text" class="form-control" id="subtitulo">
		</div>
		<div class="form-group col-md-2">
			<label for="isbn">ISBN:</label>
      		<input type="text" class="form-control" id="isbn" value="0">
		</div>
		<div class="form-group col-md-4">
			<label for="ids_autores">Autores:</label>
      		<select class="form-control" id="ids_autores" multiple="">
      			@foreach($dados['autores'] as $a)
      				<option value="{{$a->id}}">{{$a->autor}}</option>
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-4">
			<label for="ids_editoras">Editoras:</label>
      		<select class="form-control" id="ids_editoras" multiple="">
      			@foreach($dados['editoras'] as $e)
      				<option value="{{$e->id}}">{{$e->editora}}</option>
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-4">
			<label for="ids_assuntos">Assuntos:</label>
      		<select class="form-control" id="ids_assuntos" multiple="">
      			@foreach($dados['assuntos'] as $a)
      				<option value="{{$a->id}}">{{$a->assunto}}</option>
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-1">
			<label for="ano">Ano:</label>
      		<input type="text" class="form-control" id="ano">
		</div>
		<div class="form-group col-md-2">
			<label for="num_pags">Nº de páginas:</label>
      		<input type="text" class="form-control" id="num_pags">
		</div>
		<div class="form-group col-md-1">
			<label for="nota">Nota:</label>
      		<select class="form-control" id="nota">
      			<option value="0">0</option>
      			<option value="1">1</option>
      			<option value="2">2</option>
      			<option value="3">3</option>
      			<option value="4">4</option>
      			<option value="5">5</option>
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="id_tipo_midia">Tipo mídia:</label>
      		<select class="form-control" id="id_tipo_midia">
      			@foreach($dados['tipos_midia'] as $tm)
      				<option value="{{$tm->id}}">{{$tm->tipo_midia}}</option>
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="id_situacao">Situação:</label>
      		<select class="form-control" id="id_situacao">
      			@foreach($dados['situacoes'] as $s)
      				<option value="{{$s->id}}">{{$s->situacao}}</option>
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="data">Data termino:</label>
      		<input type="text" class="form-control data" id="data">
		</div>
		<div class="form-group col-md-1">
			<input type="checkbox" class="form-check-label mt-5" id="digital" value="1">
			<label for="digital">Digital</label>
		</div>
		<div class="form-group col-md-1">
			<input type="checkbox" class="form-check-label mt-5" id="favorito" value="1">
			<label for="favorito">Favorito</label>
		</div>
		<div class="form-group col-md-6">
			<label for="sinopse">Sinopse:</label>
			<textarea class="form-control block_id_skoob" id="sinopse" rows="15"></textarea>
		</div>
		<div class="form-group col-md-6">
			<label for="resenha">Resenha:</label>
			<textarea class="form-control" id="resenha" rows="15"></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="marcacoes">Marcações:</label>
			<textarea class="form-control" id="marcacoes" rows="7"></textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="url_capa">URL capa:</label>
			<input class="form-control block_id_skoob" type="text" id="url_capa">
		</div>
		<div class="form-group col-md-1">
			<button id="cadastrar" class="btn btn-primary">Cadastrar</button>
		</div>
	</div>
</section>
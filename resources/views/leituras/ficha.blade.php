<section class="container mt-2">
	<?php extract($dados); ?>
	<div class="row">
		<div class="card col-md-3">
			<img class="card-img-top mt-1" src="{{asset('images/leituras/' . $leitura->id_skoob . '/' . 'capa_grande.jpg')}}">
			<div class="card-body">
		  		<h5 class="card-title">{{$leitura->titulo}}</h5>
		  		<h5 class="card-title">{{$leitura->subtitulo}}</h5>
			</div>
		</div>
		<div class="col-md-9">
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item">
		    		<a class="nav-link active" id="infos-tab" data-toggle="tab" href="#infos" role="tab" aria-controls="infos" aria-selected="true">Infos</a>
		  		</li>
				<li class="nav-item">
		    		<a class="nav-link" id="sinopse-tab" data-toggle="tab" href="#sinopse" role="tab" aria-controls="sinopse" aria-selected="false">Sinopse</a>
				</li>
				<li class="nav-item">
		    		<a class="nav-link" id="resenha-tab" data-toggle="tab" href="#resenha" role="tab" aria-controls="resenha" aria-selected="false">Resenha</a>
		  		</li>
		  		<li class="nav-item">
		    		<a class="nav-link" id="marcacoes-tab" data-toggle="tab" href="#marcacoes" role="tab" aria-controls="marcacoes" aria-selected="false">Marcações</a>
		  		</li>
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="infos" role="tabpanel" aria-labelledby="infos-tab">
					<div class="row mt-3">
						<div class="col-md-12">
							<span><strong>Autores: </strong></span>
							<span>{{$leitura->autores}}</span>
						</div>
						<div class="col-md-12">
							<span><strong>Editoras: </strong></span>
							<span>{{$leitura->editoras}}</span>
						</div>
						<div class="col-md-12">
							<span><strong>Assuntos: </strong></span>
							<span>{{$leitura->assuntos}}</span>
						</div>
						<div class="col-md-4">
							<span><strong>ISBN: </strong></span>
							<span>{{$leitura->isbn}}</span>
						</div>
						<div class="col-md-4">
							<span><strong>Ano: </strong></span>
							<span>{{$leitura->ano}}</span>
						</div>
						<div class="col-md-4">
							<span><strong>ID Skoob: </strong></span>
							<span>{{$leitura->id_skoob}}</span>
						</div>
						<div class="col-md-4">
							<span><strong>Páginas: </strong></span>
							<span>{{$leitura->paginas}}</span>
						</div>
						<div class="col-md-4">
							<span><strong>Formato: </strong></span>
							<span>{{$leitura->digital == 1 ? 'Físico' : 'Digital'}}</span>
						</div>
						<div class="col-md-4">
							<span><strong>Situação: </strong></span>
							<span>{{$leitura->situacao->situacao}}</span>
						</div>
						<div class="col-md-4">
							<span><strong>Favorito: </strong></span>
							<span>{{$leitura->favorito == 1 ? 'Sim' : 'Não'}}</span>
						</div>
						<div class="col-md-4">
							<span><strong>Nota: </strong></span>
							<span>{{$leitura->nota}}</span>
						</div>
						<div class="col-md-4">
							<span><strong>Data finalização: </strong></span>
							@if(empty($leitura->dt_finalizacao))
								<span></span>
							@else
								<span>{{PgUtils::DataPgToBr($leitura->dt_finalizacao, '/')}}</span>
							@endif
						</div>
					</div>
				</div>
				<div class="tab-pane fade col-md-12" id="sinopse" role="tabpanel" aria-labelledby="sinopse-tab">
					<div class="row mt-3">
						<p>{{$leitura->sinopse}}</p>
					</div>
				</div>
				<div class="tab-pane fade col-md-12" id="resenha" role="tabpanel" aria-labelledby="imagens-tab">
					<div class="row mt-3">
						<p>{{$leitura->resenha}}</p>
					</div>
				</div>
				<div class="tab-pane fade col-md-12" id="marcacoes" role="tabpanel" aria-labelledby="videos-tab">
					<div class="row mt-3">
						<p>{{$leitura->marcacoes}}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
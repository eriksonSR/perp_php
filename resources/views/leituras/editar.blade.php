<section class="container mt-2">
	<h3>Editar leitura</h3>
	<div class="form-row">
		<div class="alert col-md-12" role="alert" style="display: none" id="info">
			
		</div>
		<?php $l = $dados['leitura']; ?>
		<input type="hidden" id="id_leitura" value="{{$l->id}}">
		<div class="form-group col-md-2">
			<label for="id_skoob">ID Skoob:</label>
      		<input type="text" class="form-control" id="id_skoob" autofocus="" value="{{$l->id_skoob}}">
		</div>
		<div class="form-group col-md-6 mt-4">
			<button id="dwd_dados_skoob" class="btn btn-primary">Baixar dados do Skoob</button>
		</div>
		<div class="form-group col-md-5">
			<label for="titulo">Titulo:</label>
      		<input type="text" class="form-control" id="titulo" value="{{$l->titulo}}">
		</div>
		<div class="form-group col-md-5">
			<label for="subtitulo">Subtitulo:</label>
      		<input type="text" class="form-control" id="subtitulo" value="{{$l->subtitulo}}">
		</div>
		<div class="form-group col-md-2">
			<label for="isbn">ISBN:</label>
      		<input type="text" class="form-control" id="isbn" value="{{$l->isbn}}">
		</div>
		<div class="form-group col-md-4">
			<label for="ids_autores">Autores:</label>
      		<select class="form-control" id="ids_autores" multiple="">
      			@foreach($dados['autores'] as $a)
      				@if(in_array($a->id, PgUtils::ArrayPgToArPhp($l->ids_autores)))
      					<option value="{{$a->id}}" selected="">{{$a->autor}}</option>
      				@else
      					<option value="{{$a->id}}">{{$a->autor}}</option>
      				@endif
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-4">
			<label for="ids_editoras">Editoras:</label>
      		<select class="form-control" id="ids_editoras" multiple="">
      			@foreach($dados['editoras'] as $e)
      				@if(in_array($e->id, PgUtils::ArrayPgToArPhp($l->ids_editoras)))
      					<option value="{{$e->id}}" selected="">{{$e->editora}}</option>
      				@else
      					<option value="{{$e->id}}">{{$e->editora}}</option>
      				@endif
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-4">
			<label for="ids_assuntos">Assuntos:</label>
      		<select class="form-control" id="ids_assuntos" multiple="">
      			@foreach($dados['assuntos'] as $a)
      				@if(in_array($a->id, PgUtils::ArrayPgToArPhp($l->ids_assuntos)))
      					<option value="{{$a->id}}" selected="">{{$a->assunto}}</option>
      				@else
      					<option value="{{$a->id}}">{{$a->assunto}}</option>
      				@endif
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-1">
			<label for="ano">Ano:</label>
      		<input type="text" class="form-control" id="ano" value="{{$l->ano}}">
		</div>
		<div class="form-group col-md-2">
			<label for="num_pags">Nº de páginas:</label>
      		<input type="text" class="form-control" id="num_pags" value="{{$l->paginas}}">
		</div>
		<div class="form-group col-md-1">
			<label for="nota">Nota:</label>
      		<select class="form-control" id="nota">
      			@for($i = 0; $i < 6; $i++)
      				@if($i == $l->nota)
      					<option value="{{$i}}" selected="">{{$i}}</option>
      				@else
      					<option value="{{$i}}">{{$i}}</option>
      				@endif
      			@endfor
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="id_tipo_midia">Tipo mídia:</label>
      		<select class="form-control" id="id_tipo_midia">
      			@foreach($dados['tipos_midia'] as $tm)
      				@if($tm->id == $l->id_tipo_midia)
      					<option value="{{$tm->id}}" selected="">{{$tm->tipo_midia}}</option>
      				@else
      					<option value="{{$tm->id}}">{{$tm->tipo_midia}}</option>
      				@endif
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="id_situacao">Situação:</label>
      		<select class="form-control" id="id_situacao">
      			@foreach($dados['situacoes'] as $s)
      				@if($s->id == $l->id_situacao)
      					<option value="{{$s->id}}" selected="">{{$s->situacao}}</option>
      				@else
      					<option value="{{$s->id}}">{{$s->situacao}}</option>
      				@endif
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="data">Data termino:</label>
			@if(isset($l->dt_finalizacao))
      			<input type="text" class="form-control data" id="data" value="{{PgUtils::DataPgToBr($l->dt_finalizacao, '/')}}">
      		@else
      			<input type="text" class="form-control data" id="data" value="">
      		@endif
		</div>
		<div class="form-group col-md-1">
			@if($l->digital == 1)
				<input type="checkbox" class="form-check-label mt-5" id="digital" value="1" checked="">
			@else
				<input type="checkbox" class="form-check-label mt-5" id="digital" value="1">
			@endif
			<label for="digital">Digital</label>
		</div>
		<div class="form-group col-md-1">
			@if($l->favorito == 1)
				<input type="checkbox" class="form-check-label mt-5" id="favorito" value="1" checked="">
			@else
				<input type="checkbox" class="form-check-label mt-5" id="favorito" value="1">
			@endif
			<label for="favorito">Favorito</label>
		</div>
		<div class="form-group col-md-6">
			<label for="sinopse">Sinopse:</label>
			<textarea class="form-control block_id_skoob" id="sinopse" rows="15">{{$l->sinopse}}</textarea>
		</div>
		<div class="form-group col-md-6">
			<label for="resenha">Resenha:</label>
			<textarea class="form-control" id="resenha" rows="15">{{$l->resenha}}</textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="marcacoes">Marcações:</label>
			<textarea class="form-control" id="marcacoes" rows="7">{{$l->marcacoes}}</textarea>
		</div>
		<div class="form-group col-md-12">
			<label for="url_capa">URL capa:</label>
			<input class="form-control block_id_skoob" type="text" id="url_capa" value="{{$l->url_capa}}">
		</div>
		<div class="form-group col-md-1">
			<button id="salvar" class="btn btn-primary">Salvar</button>
		</div>
	</div>
</section>
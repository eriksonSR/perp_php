<section class="container mt-2">
	<?php extract($dados); ?>
	<div class="row">
		<div class="card col-md-3">
			<img class="card-img-top mt-1" src="{{asset('images/games/boxarts/' . $game->id . '/' . $boxarts[2])}}">
			<div class="card-body">
		  		<h5 class="card-title">{{$game->titulo}}</h5>
			</div>
		</div>
		<div class="col-md-9">
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item">
		    		<a class="nav-link active" id="infos-tab" data-toggle="tab" href="#infos" role="tab" aria-controls="infos" aria-selected="true">Infos</a>
		  		</li>
				<li class="nav-item">
		    		<a class="nav-link" id="sinopse-tab" data-toggle="tab" href="#sinopse" role="tab" aria-controls="sinopse" aria-selected="false">Sinopse</a>
				</li>
				<li class="nav-item">
		    		<a class="nav-link" id="imagens-tab" data-toggle="tab" href="#imagens" role="tab" aria-controls="imagens" aria-selected="false">Imagens</a>
		  		</li>
		  		<li class="nav-item">
		    		<a class="nav-link" id="videos-tab" data-toggle="tab" href="#videos" role="tab" aria-controls="videos" aria-selected="false">Videos</a>
		  		</li>
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="infos" role="tabpanel" aria-labelledby="infos-tab">
					<div class="row mt-3">
						<div class="col-md-12">
							<span>Publicadoras: </span>
							<span>{{$game->publicadoras}}</span>
						</div>
						<div class="col-md-12">
							<span>Desenvolvedoras: </span>
							<span>{{$game->desenvolvedoras}}</span>
						</div>
						<div class="col-md-12">
							<span>Genêros: </span>
							<span>{{$game->generos}}</span>
						</div>
						<div class="col-md-12">
							<span>Digital: </span>
							@if($game->digital == 1)
								<span>Sim</span>
							@else
								<span>Não</span>
							@endif
						</div>
						<div class="col-md-12">
							<span>DLC: </span>
							@if($game->dlc == 1)
								<span>Sim</span>
							@else
								<span>Não</span>
							@endif
						</div>
						<div class="col-md-12">
							<span>Tenho: </span>
							@if($game->tenho == 1)
								<span>Sim</span>
							@else
								<span>Não</span>
							@endif
						</div>
						<div class="col-md-12">
							<span>Data lançamento: {{PgUtils::DataPgToBr($game->dt_release, '/')}}</span>
						</div>
						<div class="col-md-12">
							<span>Data finalização: {{PgUtils::DataPgToBr($game->dt_finalizacao, '/')}}</span>
						</div>
					</div>
				</div>
				<div class="tab-pane fade col-md-12" id="sinopse" role="tabpanel" aria-labelledby="sinopse-tab">
					{{$game->sinopse}}
				</div>
				<div class="tab-pane fade col-md-12" id="imagens" role="tabpanel" aria-labelledby="imagens-tab">
					<h5 class="mt-2 ml-2">Fanarts</h5>
					<hr>
					<div class="row">
						@if(!empty($fanarts))
							@foreach($fanarts as $f)
								<div class="col-md-3">
									<a href="{{asset('images/games/fanarts/' . $game->id . '/' . $f)}}" target="_blank">
										<img src="{{asset('images/games/fanarts/' . $game->id . '/thumbs/' . $f)}}" class="img-fluid img-thumbnail">
									</a>
								</div>
							@endforeach()
						@endif
					</div>
					<h5 class="mt-2 ml-2">Screenshots</h5>
					<hr>
					<div class="row">
						@if(!empty($screenshots))
							@foreach($screenshots as $f)
								<div class="col-md-3">
									<a href="{{asset('images/games/screenshots/' . $game->id . '/' . $f)}}" target="_blank">
										<img src="{{asset('images/games/screenshots/' . $game->id . '/thumbs/' . $f)}}" class="img-fluid img-thumbnail">
									</a>
								</div>
							@endforeach()
						@endif
					</div>
				</div>
				<div class="tab-pane fade col-md-12" id="videos" role="tabpanel" aria-labelledby="videos-tab">
						@if(!empty($game->videos))
							@foreach($game->videos as $v)
								<div class="col-md-12 mt-3">
									@if(isset($v->name))
										<h4>{{$v->name}}</h4>
									@elseif(isset($v->title))
										<h4>{{$v->title}}</h4>
									@endif()
									<hr>
									<div class="embed-responsive embed-responsive-16by9">
										@if(isset($v->video_id))
									 		<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{$v->video_id}}" allowfullscreen></iframe>
									 	@elseif(isset($v->url))
									 		<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{$v->url}}" allowfullscreen></iframe>
										@endif()
									</div>
								</div>
							@endforeach()
						@endif
				</div>
			</div>
		</div>
	</div>
</section>
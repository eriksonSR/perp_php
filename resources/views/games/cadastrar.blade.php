<section class="container mt-2">
	@include('studios.md_add_studio')
	<h3>Cadastrar game</h3>
	<div class="form-row">
		<div class="alert col-md-12" role="alert" style="display: none" id="info">
			
		</div>
		<input type="hidden" name="id_igdb" id="id_igdb" value="">
		<div class="form-group col-md-6">
			<label for="url_igdb">Url IGDB:</label>
      		<input type="text" class="form-control" id="url_igdb" autofocus="">
		</div>
		<div class="form-group col-md-3 mt-4">
			<button id="dwd_dados" class="btn btn-primary">Baixar dados</button>
		</div>
		<div class="form-group col-md-3 mt-4">
			<button id="add_studio" class="btn btn-primary">Add studio</button>
		</div>
		<div class="form-group col-md-3">
			<label for="titulo">Titulo:</label>
      		<input type="text" class="form-control" id="titulo">
		</div>
		<div class="form-group col-md-2">
			<label for="id_console">Console:</label>
      		<select class="form-control" id="id_console">
      			@foreach($dados['consoles'] as $c)
      				<option value="{{$c->id}}">{{$c->console}}</option>
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="ids_publicadoras">Publicadora:</label>
      		<select class="form-control" id="ids_publicadoras" multiple="">
      			@foreach($dados['studios'] as $p)
      				<option value="{{$p->id}}">{{$p->studio}}</option>
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="ids_desenvolvedoras">Desenvolvedora:</label>
      		<select class="form-control" id="ids_desenvolvedoras" multiple="">
      			@foreach($dados['studios'] as $p)
      				<option value="{{$p->id}}">{{$p->studio}}</option>
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-3">
			<label for="ids_generos">Gêneros:</label>
      		<select class="form-control" id="ids_generos" multiple="">
      			@foreach($dados['generos'] as $g)
      				<option value="{{$g->id}}">{{$g->genero}}</option>
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="id_situacao">Situação:</label>
      		<select class="form-control" id="id_situacao">
      			@foreach($dados['situacoes'] as $s)
      				<option value="{{$s->id}}">{{$s->situacao}}</option>
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="id_origem">Origem:</label>
      		<select class="form-control" id="id_origem">
      			@foreach($dados['origens'] as $o)
      				<option value="{{$o->id}}">{{$o->origem}}</option>
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="data_release">Data release:</label>
      		<input type="text" class="form-control data" id="data_release">
		</div>
		<div class="form-group col-md-2">
			<label for="data_termino">Data termino:</label>
      		<input type="text" class="form-control data" id="data_termino">
		</div>
		<div class="form-group col-md-1">
			<input type="checkbox" class="form-check-label mt-5" id="digital" value="1">
			<label for="digital">Digital</label>
		</div>
		<div class="form-group col-md-1">
			<input type="checkbox" class="form-check-label mt-5" id="dlc" value="1">
			<label for="dlc">DLC</label>
		</div>
		<div class="form-group col-md-1">
			<input type="checkbox" class="form-check-label mt-5" id="tenho" value="1">
			<label for="tenho">Tenho</label>
		</div>
		<div class="form-group col-md-12">
			<label for="sinopse">Sinopse:</label>
			<textarea class="form-control block_id_skoob" id="sinopse" rows="8"></textarea>
		</div>
		<div class="form-group col-md-1">
			<button id="cadastrar" class="btn btn-primary">Cadstrar</button>
		</div>
	</div>
</section>
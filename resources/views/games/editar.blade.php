<section class="container mt-2">
	<h3>Editar game</h3>
	<div class="form-row">
		<div class="alert col-md-12" role="alert" style="display: none" id="info">
			
		</div>
		<?php extract($dados); ?>
		<input type="hidden" id="id_game" value="{{$game->id}}">
		<div class="form-group col-md-2">
			<label for="id_igdb">ID IGDB:</label>
      		<input type="text" class="form-control" id="id_igdb" autofocus="" value="{{$game->id_igdb}}">
		</div>
		<div class="form-group col-md-6">
			<label for="url_igdb">URL IGDB:</label>
      		<input type="text" class="form-control" id="url_igdb" value="{{$game->url_igdb}}">
		</div>
		<div class="row col-md-6 offset-md-2">
			<div class="form-group col-md-12">
				<label for="titulo">Titulo:</label>
	      		<input type="text" class="form-control" id="titulo" value="{{$game->titulo}}">
			</div>
		</div>

		<div class="form-group col-md-2">
			<label for="id_console">Console:</label>
      		<select class="form-control" id="id_console">
      			@foreach($consoles as $c)
      				@if($c->id == $game->id_console)
      					<option value="{{$c->id}}" selected="">{{$c->console}}</option>
      				@else
      					<option value="{{$c->id}}">{{$c->console}}</option>
      				@endif
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="ids_publicadoras">Publicadora:</label>
      		<select class="form-control" id="ids_publicadoras" multiple="">
      			@foreach($studios as $s)
      				@if(in_array($s->id, PgUtils::ArrayPgToArPhp($game->ids_publicadoras)))
      					<option value="{{$s->id}}" selected="">{{$s->studio}}</option>
      				@else
      					<option value="{{$s->id}}">{{$s->studio}}</option>
      				@endif
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="ids_desenvolvedoras">Desenvolvedora:</label>
      		<select class="form-control" id="ids_desenvolvedoras" multiple="">
      			@foreach($studios as $s)
      				@if(in_array($s->id, PgUtils::ArrayPgToArPhp($game->ids_desenvolvedoras)))
      					<option value="{{$s->id}}" selected="">{{$s->studio}}</option>
      				@else
      					<option value="{{$s->id}}">{{$s->studio}}</option>
      				@endif
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-3">
			<label for="ids_generos">Gêneros:</label>
      		<select class="form-control" id="ids_generos" multiple="">
      			@foreach($generos as $g)
      				@if(in_array($g->id, PgUtils::ArrayPgToArPhp($game->ids_generos)))
      					<option value="{{$g->id}}" selected="">{{$g->genero}}</option>
      				@else
      					<option value="{{$g->id}}">{{$g->genero}}</option>
      				@endif
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="id_situacao">Situação:</label>
      		<select class="form-control" id="id_situacao">
      			@foreach($situacoes as $s)
      				@if($s->id == $game->id_situacao)
      					<option value="{{$s->id}}" selected="">{{$s->situacao}}</option>
      				@else
      					<option value="{{$s->id}}">{{$s->situacao}}</option>
      				@endif
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="id_origem">Origem:</label>
      		<select class="form-control" id="id_origem">
      			@foreach($dados['origens'] as $o)
      				@if($o->id == $game->id_origem)
      					<option value="{{$o->id}}" selected="">{{$o->origem}}</option>
      				@else
      					<option value="{{$o->id}}">{{$o->origem}}</option>
      				@endif
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="data_release">Data release:</label>
			@if(isset($game->dt_release))
      			<input type="text" class="form-control data" id="data_release" value="{{PgUtils::DataPgToBr($game->dt_release, '/')}}">
      		@else
      			<input type="text" class="form-control data" id="data_release" value="">
      		@endif
		</div>
		<div class="form-group col-md-2">
			<label for="data_termino">Data termino:</label>
			@if(isset($game->dt_finalizacao))
      			<input type="text" class="form-control data" id="data_termino" value="{{PgUtils::DataPgToBr($game->dt_finalizacao, '/')}}">
      		@else
      			<input type="text" class="form-control data" id="data_termino" value="">
      		@endif
		</div>
		<div class="form-group col-md-1">
			@if($game->digital == 1)
				<input type="checkbox" class="form-check-label mt-5" id="digital" value="1" checked="">
			@else
				<input type="checkbox" class="form-check-label mt-5" id="digital" value="1">
			@endif
			<label for="digital">Digital</label>
		</div>
		<div class="form-group col-md-1">
			@if($game->dlc == 1)
				<input type="checkbox" class="form-check-label mt-5" id="dlc" value="1" checked="">
			@else
				<input type="checkbox" class="form-check-label mt-5" id="dlc" value="1">
			@endif
			<label for="dlc">DLC</label>
		</div>
		<div class="form-group col-md-1">
			@if($game->tenho == 1)
				<input type="checkbox" class="form-check-label mt-5" id="tenho" value="1" checked="">
			@else
				<input type="checkbox" class="form-check-label mt-5" id="tenho" value="1">
			@endif
			<label for="tenho">Tenho</label>
		</div>
		<div class="form-group col-md-12">
			<label for="sinopse">Sinopse:</label>
			<textarea class="form-control" id="sinopse" rows="8">{{$game->sinopse}}</textarea>
		</div>
		<div class="form-group col-md-1">
			<button id="salvar" class="btn btn-primary">Salvar</button>
		</div>
	</div>
</section>
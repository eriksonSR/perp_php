<section class="container mt-2">
	<h3>Listar</h3>
	<button class="btn btn-primary ml-2 mb-2" id="bt_filtros">Filtros</button>
	<a href="{{route('games.exp_tabela')}}" class="btn btn-success ml-2 mb-2" target="_blank">Exportar tabela</a>
	<?php extract($dados); ?>
	<div class="form-row jumbotron" style="display: none;" id="div_filtros">
		<div class="form-group col-md-3">
			<label>Titulo:</label>
			<input class="form-control" type="text" id="titulo">
		</div>
		<div class="form-group col-md-2">
			<label>Plataformas:</label>
			<select class="form-control mb-2" id="ids_plataformas" multiple="">
				<option value="0">Selecione...</option>
				@foreach ($plataformas as $p)
					<option value="{{$p->id}}">{{$p->plataforma}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label>Consoles:</label>
			<select class="form-control mb-2" id="ids_consoles" multiple="">
				<option value="0">Selecione...</option>
				@foreach ($consoles as $c)
					<option value="{{$c->id}}">{{$c->console}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Genêros:</label>
			<select class="form-control mb-2" id="ids_generos" multiple="">
				<option value="0">Selecione...</option>
				@foreach ($generos as $g)
					<option value="{{$g->id}}">{{$g->genero}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label>Situação:</label>
			<select class="form-control mb-2" id="id_situacao">
				<option value="0">Selecione...</option>
				@foreach ($situacoes as $s)
					<option value="{{$s->id}}">{{$s->situacao}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Publicadoras:</label>
			<select class="form-control mb-2" id="ids_publicadoras" multiple="">
				<option value="0">Selecionar publicadoras...</option>
				@foreach ($studios as $s)
					<option value="{{$s->id}}">{{$s->studio}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Desenvolvedoras:</label>
			<select class="form-control mb-2" id="ids_desenvolvedoras" multiple="">
				<option value="0">Selecionar desenvolvedoras...</option>
				@foreach ($studios as $s)
					<option value="{{$s->id}}">{{$s->studio}}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<input type="checkbox" class="form-check-label mt-5" id="dlc" value="1">
			<label for="dlc">DLC</label>
		</div>
		<div class="form-group col-md-2">
			<input type="checkbox" class="form-check-label mt-5" id="digital" value="1">
			<label for="digital">Digital</label>
		</div>
		<div class="form-group col-md-2">
			<input type="checkbox" class="form-check-label mt-5" id="tenho" value="1">
			<label for="tenho">Tenho</label>
		</div>
		<div class="form-group col-md-3">
			<label>Critério dt. inicial finalização:</label>
			<select class="form-control" id="criterio_ini_dt_finalizacao">
				<option value=""></option>
				<option value=">">></option>
				<option value="<"><</option>
				<option value="=">=</option>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Data inicial:</label>
			<input class="form-control data" type="text" id="data_ini">
		</div>
		<div class="form-group col-md-3">
			<label>Critério dt. final finalização:</label>
			<select class="form-control" id="criterio_final_dt_finalizacao">
				<option value=""></option>
				<option value=">">></option>
				<option value="<"><</option>
				<option value="=">=</option>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label>Data final:</label>
			<input class="form-control data" type="text" id="data_final">
		</div>
		<div class="form-group col-md-2">
			<button id="bt_filtrar" class="btn btn-primary">Filtrar</button>
		</div>
		<div class="form-group col-md-2">
			<button id="bt_resetar" class="btn btn-primary">Resetar</button>
		</div>
		<div class="form-group col-md-2">
			<button id="bt_excel" class="btn btn-success">Excel</button>
		</div>
	</div>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_games">
			<thead>
				<th>Titulo</th>
				<th>Console</th>
				<th>Publicadoras</th>
				<th>Digital</th>
				<th>Editar</th>
				<th>Ficha</th>
			</thead>
			<tbody>
				@foreach ($games as $g)
					<tr>
						<td>{{$g->titulo}}</td>
						<td>{{$g->Console->console}}</td>
						<td>{{$g->publicadoras}}</td>
						<td>
							@if($g->digital == 1)
								Digital
							@else
								Fisíco
							@endif
						</td>
						<td>
							<a href="{{route('games.editar', ['id' => $g->id])}}" class="btn btn-success btn-sm text-center" target="_blank">
								<i class="fa fa-edit"></i>
							</a>
						</td>
						<td>
							<a href="{{route('games.ficha', ['id' => $g->id])}}" class="btn btn-success btn-sm text-center" target="_blank">
								<i class="fa fa-search"></i>
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
<div class="modal fade" id="md_add_peso" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Adicionar peso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
          <div class="form-group col-md-12">
            <label for="data">Data:</label>
            <input type="text" class="form-control data" id="frm_add_peso_data" autofocus="">
          </div>
          <div class="form-group col-md-12">
            <label for="peso">Peso:</label>
            <input type="text" class="form-control peso" id="frm_add_peso_peso">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary" id='bt_add_peso'>Cadastrar</button>
      </div>
    </div>
  </div>
</div>
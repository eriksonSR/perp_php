<?php
	use App\Helpers\Datas;
	extract($dados); 
?>
<section class="container mt-2">
	<h3>Listar</h3>
	<button class="btn btn-primary ml-2 mb-2" id="bt_filtros">Filtros</button>
	<button class="btn btn-primary ml-2 mb-2" id="bt_modal_add_peso">Add peso</button>
	<div class="form-row jumbotron" style="display: none;" id="div_filtros">
		<div class="form-group col-md-2">
			<label>Critério peso:</label>
			<select class="form-control" id="criterio_peso">
				<option value=""></option>
				<option value=">">></option>
				<option value="<"><</option>
				<option value="=">=</option>
			</select>
		</div>

		<div class="form-group col-md-2">
			<label>Peso:</label>
			<input class="form-control" type="text" id="peso">
		</div>

		<div class="form-group col-md-2">
			<label>Critério data inicial:</label>
			<select class="form-control" id="criterio_data_ini">
				<option value=""></option>
				<option value=">">></option>
				<option value="<"><</option>
				<option value="=">=</option>
			</select>
		</div>

		<div class="form-group col-md-2">
			<label>Data inicial:</label>
			<input class="form-control data" type="text" id="data_ini">
		</div>

		<div class="form-group col-md-2">
			<label>Critério data final:</label>
			<select class="form-control" id="criterio_data_fim">
				<option value=""></option>
				<option value=">">></option>
				<option value="<"><</option>
				<option value="=">=</option>
			</select>
		</div>

		<div class="form-group col-md-2">
			<label>Data final:</label>
			<input class="form-control data" type="text" id="data_fim">
		</div>
	  	
		<div class="form-group col-md-2">
			<button id="bt_filtrar" class="btn btn-primary">Filtrar</button>
		</div>
		<div class="form-group col-md-2">
			<button id="bt_resetar" class="btn btn-primary">Resetar</button>
		</div>
	</div>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_pesos">
			<thead>
				<th>Data</th>
				<th>Peso</th>
				<th>Açoes</th>
			</thead>
			<tbody>
				@foreach($pesos as $p)
					<tr>
						<td>{{Datas::GetDiaDaSemana($p->data)['extenso']}}, {{PgUtils::DataPgToBr($p->data, '/')}}</td>
						<td>{{str_replace('.', ',', $p->peso)}} Kg</td>
						<td>
							<button class="btn btn-danger btn-sm text-center" onclick="Excluir({{$p->id}}, this, '{{str_replace('.', ',', $p->peso)}}');">
								<i class="fa fa-remove"></i>
							</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
@include('pesos.md_add_peso')
<section class="container mt-2">
	<h3>Listar</h3>
	<?php extract($dados); ?>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_games">
			<thead>
				<th>ID</th>
				<th>Autor</th>
				<th>Editar</th>
			</thead>
			<tbody>
				@foreach ($autores as $a)
					<tr>
						<td>{{$a->id}}</td>
						<td>{{$a->autor}}</td>
						<td>
							<a href="{{route('autores.editar', ['id' => $a->id])}}" class="btn btn-success btn-sm text-center" target="_blank">
								<i class="fa fa-edit"></i>
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
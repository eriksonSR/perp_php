<section class="container mt-2">
	<h3>Editar autor</h3>
	<div class="form-row">
		<div class="alert col-md-12" role="alert" style="display: none" id="info">
			
		</div>
		<?php extract($dados); ?>
		<input type="hidden" id="id_autor" value="{{$autor->id}}">
		<div class="form-group col-md-10">
			<label for="autor">Autor:</label>
      		<input type="text" class="form-control" id="autor" autofocus="" value="{{$autor->autor}}">
		</div>
		<div class="form-group col-md-2 mt-4">
			<button id="cadastrar" class="btn btn-primary">Salvar</button>
		</div>
	</div>
</section>
<?php
	extract($dados);
?>
<section class="container mt-2">
	<h3>Lançamentos</h3>
	<button class="btn btn-primary ml-2 mb-2" id="bt_filtros">Filtros</button>
	<div class="form-row jumbotron" style="display: none;" id="div_filtros">
		<div class="form-group col-md-6">
			<label>Conta creditada:</label>
			<select class="form-control mb-2" id="conta_cred">
				<option value="">Selecione a conta...</option>
				@foreach ($contas as $c)
					@if($c->tipo != 'r' && $c->analitica == 1)
						<option value="{{$c['id']}}">{{$c['conta']}}</option>
					@endif
				@endforeach
			</select>
		</div>

		<div class="form-group col-md-6">
			<label>Conta debitada:</label>
			<select class="form-control mb-2 mr-sm-2" id="conta_deb">
				<option value="">Selecione a conta...</option>
				@foreach ($contas as $c)
					@if($c->tipo != 'd' && $c->analitica == 1)
						<option value="{{$c['id']}}">{{$c['conta']}}</option>
					@endif
				@endforeach
			</select>
		</div>

		<div class="form-group col-md-6">
			<label>Histórico:</label>
			<input class="form-control" type="text" id="historico">
		</div>

		<div class="form-group col-md-2">
			<label>Critério valor:</label>
			<select class="form-control" id="criterio_valor">
				<option value=""></option>
				<option value=">">></option>
				<option value="<"><</option>
				<option value="=">=</option>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label>Valor:</label>
			<input class="form-control monetario" type="text" id="valor">
		</div>

		<div class="form-group col-md-3">
			<label>Critério data inicial:</label>
			<select class="form-control" id="criterio_data_ini">
				<option value=""></option>
				<option value=">">></option>
				<option value="<"><</option>
				<option value="=">=</option>
			</select>
		</div>

		<div class="form-group col-md-3">
			<label>Data inicial:</label>
			<input class="form-control data" type="text" id="data_ini">
		</div>

		<div class="form-group col-md-3">
			<label>Critério data final:</label>
			<select class="form-control" id="criterio_data_fim">
				<option value=""></option>
				<option value=">">></option>
				<option value="<"><</option>
				<option value="=">=</option>
			</select>
		</div>

		<div class="form-group col-md-3">
			<label>Data final:</label>
			<input class="form-control data" type="text" id="data_fim">
		</div>
	  	
		<div class="form-group col-md-2">
			<button id="bt_filtrar" class="btn btn-primary">Filtrar</button>
		</div>
		<div class="form-group col-md-2">
			<button id="bt_excel" class="btn btn-primary">Excel</button>
		</div>
		<div class="form-group col-md-2">
			<button id="bt_resetar" class="btn btn-primary">Resetar</button>
		</div>
	</div>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_lancamentos">
			<thead>
				<th>ID</th>
				<th>CONTA CREDITADA</th>
				<th>CONTA DEBITADA</th>
				<th>HISTÓRICO</th>
				<th>VALOR</th>
				<th>DATA</th>
				<th>EDITAR</th>
			</thead>
			<tbody>
				@foreach ($lancamentos as $l)
					<tr>
						<td>{{$l->id}}</td>
						<td>{{$l->c_cre}}</td>
						<td>{{$l->c_deb}}</td>
						<td>{{$l->historico}}</td>
						<td>RS {{number_format($l->valor, 2,',','.')}}</td>
						<td>{{$l->dt}}</td>
						<td class="text-center">
							<button class="btn btn-success btn-sm text-center" data-id_lancamento="{{$l->id}}" onclick="ExibeFormEditaLancamento('{{$l->id}}')">
								<i class="fa fa-edit"></i>
							</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
@include('contabil.lancamentos.md_editar_lancamento')
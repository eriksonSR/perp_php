<div class="modal fade" id="md_editar_lancamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar lançamento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-row">
          <input type="hidden" id="frm_id_lancamento" value="">
          <div class="form-group col-md-12">
            <label for="historico">Histórico:</label>
            <input type="text" class="form-control" id="frm_edit_historico" value="">
          </div>
          <div class="form-group col-md-12">
            <label for="data">Data:</label>
            <input type="text" class="form-control data" id="frm_edit_data" value="">
          </div>
          <div class="form-group col-md-12">
            <label for="data">Valor:</label>
            <input type="text" class="form-control monetario" id="frm_edit_valor" value="">
          </div>
          <div class="form-group col-md-12">
            <label for="tipo">Conta debitada:</label>
            <select class="form-control" id="frm_edit_cd">
              @foreach($contas as $c)
                @if($c->tipo != 'd' && $c->analitica == 1)
                  <option value="{{$c->id}}">{{$c->conta}}</option>
                @endif
              @endforeach
            </select>
          </div>
          <div class="form-group col-md-12">
            <label for="tipo">Conta creditada:</label>
            <select class="form-control" id="frm_edit_cc">
              @foreach($contas as $c)
                @if($c->tipo != 'r' && $c->analitica == 1)
                  <option value="{{$c->id}}">{{$c->conta}}</option>
                @endif
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary" id='bt_salvar_lancamento'>Salvar</button>
      </div>
    </div>
  </div>
</div>
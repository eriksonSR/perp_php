<?php
	extract($dados);
?>
<section class="container mt-2">
	<h3>Cadastrar lançamento</h3>
	<div class="form-row">
		<div class="col-md-5">
			<div class="alert col-md-12" role="alert" style="display: none" id="info">
				
			</div>
			<div class="form-group col-md-12">
				<label for="historico">Histórico:</label>
	      		<input type="text" class="form-control" id="historico">
			</div>
			<div class="form-group col-md-12">
				<label for="valor">Data:</label>
	      		<input type="text" class="form-control" id="data">
			</div>
			<div class="form-group col-md-12">
				<label for="valor">Valor:</label>
	      		<input type="text" class="form-control" id="valor" class="monetario">
			</div>
			<div class="form-group col-md-12">
				<label for="tipo">Contra debitada:</label>
	      		<select class="form-control" id="id_conta_debitada">
	      			@foreach($contas as $c)
	      				@if($c->tipo != 'd' && $c->analitica == 1)
	      					<option value="{{$c->id}}">{{$c->conta}}</option>
	      				@endif
	      			@endforeach
	      		</select>
			</div>
			<div class="form-group col-md-12">
				<label for="tipo">Contra creditada:</label>
	      		<select class="form-control" id="id_conta_creditada">
	      			@foreach($contas as $c)
	      				@if($c->tipo != 'r' && $c->analitica == 1)
	      					<option value="{{$c->id}}">{{$c->conta}}</option>
	      				@endif
	      			@endforeach
	      		</select>
			</div>
			<div class="form-group col-md-6">
				<button id="cadastrar" class="btn btn-primary">Cadastrar</button>
			</div>
		</div>
		<div class="col-md-7">
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item">
			   		<a class="nav-link active" id="data_hist-tab" data-toggle="tab" href="#data_hist" role="tab" aria-controls="data_hist" aria-selected="true">Últimos por data histórico</a>
			  	</li>
				<li class="nav-item">
			    	<a class="nav-link" id="data_inc-tab" data-toggle="tab" href="#data_inc" role="tab" aria-controls="data_inc" aria-selected="false">Últimos por data inclusão</a>
				</li>
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="data_hist" role="tabpanel" aria-labelledby="data_hist-tab">
					<div class="row mt-3">
						<table id="container_ult_lancamentos_hist" class="table">
							<thead>
								<tr>
									<th>Histórico</th><th>Data</th><th>Valor</th><th>CC</th><th>CD</th>
								</tr>
							</thead>
							<tbody>
								@foreach($lancamentos_hist as $l)
									<tr>
										<td>{{$l->historico}}</td>
										<td>{{PgUtils::DataPgToBr($l->data, '/')}}</td>
										<td>{{$l->valor}}</td>
										<td>{{$l->ContaCreditada->conta}}</td>
										<td>{{$l->ContaDebitada->conta}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane fade" id="data_inc" role="tabpanel" aria-labelledby="data_inc-tab">
					<div class="row mt-3">
						<table id="container_ult_lancamentos_inc" class="table">
							<thead>
								<tr>
									<th>Histórico</th><th>Data</th><th>Valor</th><th>CC</th><th>CD</th>
								</tr>
							</thead>
							<tbody>
								@foreach($lancamentos_inc as $l)
									<tr>
										<td>{{$l->historico}}</td>
										<td>{{PgUtils::DataPgToBr($l->created_at, '/', 1)}}</td>
										<td>{{$l->valor}}</td>
										<td>{{$l->ContaCreditada->conta}}</td>
										<td>{{$l->ContaDebitada->conta}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="container mt-2">
	<h3>Cadastrar conta</h3>
	<div class="form-row">
		<div class="form-group col-md-4">
			<label for="conta">Conta:</label>
      		<input type="text" class="form-control" id="conta">
		</div>
		<div class="form-group col-md-4">
			<label for="tipo">Tipo:</label>
      		<select class="form-control" id="tipo">
      			<option value=""></option>
      			<option value="d">Despesa</option>
      			<option value="r">Receita</option>
      			<option value="m">Mista</option>
      		</select>
		</div>
		<div class="form-group col-md-4">
			<input type="checkbox" class="form-check-label mt-5" id="analitica" value="1">
			<label for="analitica">Analítica</label>
		</div>
		<div class="form-group col-md-4">
			<label for="id_conta_mae">Conta mãe:</label>
      		<select class="form-control" id="id_conta_mae">
      			<option value=""></option>
      			@foreach($dados as $d)
      				<option value="{{$d->id}}">{{$d->grau . ' - ' . $d->conta}}</option>
      			@endforeach
      		</select>
		</div>
		<div class="form-group col-md-2">
			<label for="grau">Grau:</label>
      		<input type="text" class="form-control" id="grau">
		</div>
		<div class="form-group col-md-7">
			<button id="cadastrar" class="btn btn-primary">Cadstrar</button>
		</div>
	</div>
</section>
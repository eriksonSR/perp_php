<section class="container mt-2">
	<h3>Contas</h3>
	</div>
	<div class="row">
		<table class="table table-striped table-hover" id="tb_relatorio">
			<thead>
				<th>GRAU</th>
				<th>CONTA</th>
				<th>ANALÍTICA</th>
				<th>TIPO</th>
				<th>SALDO</th>
				<th class="text-center">LANÇAMENTOS</th>
			</thead>
			<tbody>
				@foreach ($dados as $d)
					<tr>
						<td>{{$d['grau']}}</td>
						<td>{{$d['conta']}}</td>
						<td>{{$d['analitica'] === 0 ? 'Não' : 'Sim'}}</td>
						@switch($d['tipo'])
							@case('r')
								<td>Receita</td>
								@break
							@case('d')
								<td>Despesa</td>
								@break
							@default
								<td>Mista</td>
						@endswitch
						<td>0</td>
						<td class="text-center"><button class="btn btn-success btn-sm text-center bt_lancamentos" data-id_conta={{$d['id']}}><i class="fa fa-search"></i></button></td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
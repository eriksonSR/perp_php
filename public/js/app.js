mask_money_config = {
	prefix:'R$ ', 
	allowNegative: false, 
	thousands:'.', 
	decimal:',',
	affixesStay: false
}

date_picker_config = {
	closeText: 'Fechar',
	prevText: '&#x3c;Anterior',
	nextText: 'Pr&oacute;ximo&#x3e;',
	currentText: 'Hoje',
	monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho', 'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
	dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
	dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
	dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
	weekHeader: 'Sm',
	dateFormat: 'dd/mm/yy',
	firstDay: 0,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: ''
}

$(document).ready(function(){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
	  if (!$(this).next().hasClass('show')) {
	    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
	  }
	  var $subMenu = $(this).next(".dropdown-menu");
	  $subMenu.toggleClass('show');


	  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
	    $('.dropdown-submenu .show').removeClass("show");
	  });


	  return false;
	});
});



function MudarSituacaoTarefa(el)
{
    var id_tarefa = $(el).data('id_tarefa');
	var id_situacao = $(el).val();
	$.get('http://' + location.host + '/listas_tarefas/tarefas/mudarsituacao/' + id_tarefa + '/' + id_situacao);
}


function EfetuarBuscaGeral()
{
	var url = 'http://' + location.host + '/busca/' + $('#busca_geral').val();
	$('#busca_geral').val('');
	window.open(url, '_blank');
}
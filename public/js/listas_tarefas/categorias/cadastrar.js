$(document).ready(function(){
	$('#cadastrar').on('click', function(){
		var dados = {
			'categoria': $('#categoria_tarefa').val()
		}
		
		$.ajax({
			url: 'http://' + location.host + '/listas_tarefas/categorias/salvar',
			type: "POST",
			data: dados,
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");
				}else if (data.status == "s"){
					LimpaCampos();
					$("#info").addClass("alert-success");
				}
				$("#info").show();
			}
		});
	});
});

function LimpaCampos()
{
	$('#cadastrar').val('');
}
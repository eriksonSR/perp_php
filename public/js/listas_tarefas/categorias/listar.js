$(document).ready(function(){
	$('#bt_salvar_categoria').on('click', function(){
		var dados = {
			'id_categoria': $('#frm_id_categoria').val(),
			'categoria': $('#frm_edit_categoria').val()
		}
		
		$.ajax({
			url: 'http://' + location.host + '/listas_tarefas/categorias/atualizar',
			type: "POST",
			data: dados,
			dataType: "json",
			success: function(data) {
				$('#md_editar_categoria').modal('toggle');
				alert(data.msg);
				linha_categoria = $('[data-id_categoria="' + dados.id_categoria + '"]');
				colunas = $(linha_categoria).find('td');
				$(colunas).eq(0).text(dados.categoria);
			}
		});
	});
});

function ExibeFormEditaCategoria(id_categoria)
{
	$('#md_editar_categoria').modal('toggle');
	$.ajax({
		url: 'http://' + location.host + '/listas_tarefas/categorias/get_categoria_json/' + id_categoria,
		type: "GET",
		dataType: "json",
		success: function(data) {
			$('#frm_id_categoria').val(id_categoria);
			$('#frm_edit_categoria').val(data.categoria);
		}
	});
}
$(document).ready(function(){
	$(".data").datepicker(date_picker_config);
	situacoes_tarefas = 0;
	categorias_tarefas = 0;
	$.getJSON('http://' + location.host + '/listas_tarefas/situacoes/get_situacoes_json', function(dados){
		situacoes_tarefas = dados;
	});
	$.getJSON('http://' + location.host + '/listas_tarefas/categorias/get_categorias_json', function(dados){
		categorias_tarefas = dados;
	});

	$('#bt_salvar_lista').on('click', function(){
		var dados = {
			'id_lista': $('#frm_id_lista').val(),
			'lista': $('#frm_edit_lista').val(),
			'data': $('#frm_edit_data').val(),
			'dia': $('#dia').val()
		}
		
		$.ajax({
			url: 'http://' + location.host + '/listas_tarefas/listas/atualizar',
			type: "POST",
			data: dados,
			dataType: "json",
			success: function(data) {
				$('#md_editar_lista').modal('toggle');
				alert(data.msg);
				linha_lista = $('[data-id_lista="' + dados.id_lista + '"]');
				colunas = $(linha_lista).find('td');
				$(colunas).eq(0).text(dados.lista);
				$(colunas).eq(1).text(dados.data);
				$(colunas).eq(2).text(dados.dia);
			}
		});
	});

	$('#inp_add_tarefa').on('click', function(){
		$("#inp_tarefa").val('');
		$('#md_add_tarefa').modal('toggle');
	});

	$('#bt_salvar_tarefa').on('click', function(){
		var dados = {
			'tarefa': $('#inp_tarefa').val(),
			'id_categoria': $('#inp_categoria').val(),
			'categoria': $("#inp_categoria option:selected").text(),
			'id_situacao': $('#inp_situacao').val(),
			'id_lista': $('#inp_id_lista').val()
		}

		$.ajax({
			url: 'http://' + location.host + '/listas_tarefas/tarefas/salvar',
			type: "POST",
			data: dados,
			dataType: "json",
			success: function(data) {
				if (data.status == 's'){
					var select = `<select class="form-control" onchange="MudarSituacaoTarefa(this)" data-id_tarefa="${data.id}">`;
					$(situacoes_tarefas).each(function(is, vs){
						selecionado = '';

						if (vs.id == dados.id_situacao){
							selecionado = 'selected=""';
						}
						select += `<option value="${vs.id}" ${selecionado}>${vs.situacao}</option>`;
					});
					select += '</select>';

					$('#tb_tarefas tbody').append(
						`<tr data-tr_id_tarefa="${data.id}">
							<td>${dados.tarefa}</td>
							<td>${dados.categoria}</td>
							<td>${select}</td>
							<td>
								<button class="btn btn-success btn-sm text-center" onclick="EditarTarefa(${data.id})">
									<i class="fa fa-edit"></i>
								</button>
								<button class="btn btn-danger btn-sm text-center" onclick="ExcluirTarefa(${data.id})">
									<i class="fa fa-trash"></i>
								</button>
							</td>
						</tr>`
					);
				}else{
					alert(data.msg)
				}
				$('#md_add_tarefa').modal('toggle');
			}
		});
	});
});

$('#bt_atualizar_tarefa').on('click', function(){
	var dados = {
		'id_tarefa': $('#frm_edit_tarefa_id').val(),
		'tarefa': $('#frm_edit_tarefa_tarefa').val(),
		'categoria': $('#frm_edit_tarefa_categoria').val(),
		'situacao': $('#frm_edit_tarefa_situacao').val()
	}
	console.log(dados);

	$.ajax({
		url: 'http://' + location.host + '/listas_tarefas/tarefas/atualizar',
		type: "POST",
		data: dados,
		dataType: "json",
		success: function(data) {
			if (data.status == 'Sucesso'){
				AtualizaTabelaTarefas(dados.id_tarefa, dados.tarefa, dados.categoria, dados.situacao);
				$('#md_editar_tarefa').modal('hide');
			}else{
				alert(data.msg);
			}
		}
	});
});

function ExibeFormEditaLista(id_lista)
{
	$('#md_editar_lista').modal('toggle');
	$.ajax({
		url: 'http://' + location.host + '/listas_tarefas/listas/get_lista_json/' + id_lista,
		type: "GET",
		dataType: "json",
		success: function(data) {
			$('#frm_id_lista').val(id_lista);
			$('#frm_edit_lista').val(data.lista);
			$('#frm_edit_data').val(data.data);
			$('#dia option').each(function(i, v){
				if ($(v).val() == data.dia){
					$(v).prop('selected', true);
				}
			});
		}
	});
}

function ExibeModalTarefas(id_lista)
{
	$('#inp_id_lista').val(id_lista);
	$('#md_listar_tarefas').modal('toggle');
	$.ajax({
		url: 'http://' + location.host + '/listas_tarefas/listas/get_lista_c_tarefas_json/' + id_lista,
		type: "GET",
		dataType: "json",
		success: function(data) {
			$('#titulo_lista').text(data[0].lista);
			$('#data_lista').text(data[0].data);
			$('#dia_lista').text(data[0].dia);
			$('#tb_tarefas tbody').html('');
			$(data).each(function(i, v){
				var select = `<select class="form-control" onchange="MudarSituacaoTarefa(this)" data-id_tarefa="${v.id}">`;
				$(situacoes_tarefas).each(function(is, vs){
					selecionado = '';
					if (vs.id == v.id_situacao){
						selecionado = 'selected=""';
					}
					select += `<option value="${vs.id}" ${selecionado}>${vs.situacao}</option>`;
				});
				select += '</select>';

				$('#tb_tarefas tbody').append(
					`<tr data-tr_id_tarefa="${v.id}">
						<td>${v.tarefa}</td>
						<td>${v.categoria}</td>
						<td>${select}</td>
						<td>
							<button class="btn btn-success btn-sm text-center" onclick="EditarTarefa(${v.id})">
								<i class="fa fa-edit"></i>
							</button>
							<button class="btn btn-danger btn-sm text-center" onclick="ExcluirTarefa(${v.id})">
								<i class="fa fa-trash"></i>
							</button>
						</td>
					</tr>`
				);
			});
		}
	});
}

function ExcluirTarefa(id_tarefa)
{
	$('#tb_tarefas tbody tr[data-tr_id_tarefa="' + id_tarefa + '"]').remove();
	var resp = confirm('Deseja realmente exluir a tarefa?');
	if(resp){
		$.ajax({
			url: 'http://' + location.host + '/listas_tarefas/tarefas/excluir/' + id_tarefa,
			type: "DELETE",
			dataType: "json",
			success: function(data) {
				if (data.status == 'Sucesso'){
					$('#tb_tarefas tbody tr[data-tr_id_tarefa="' + id_tarefa + '"]').remove();
				}else{
					alert(data.msg);
				}
			}
		});
	}
}

function EditarTarefa(id_tarefa)
{
	$.ajax({
		url: 'http://' + location.host + '/listas_tarefas/tarefas/get_tarefa_json/' + id_tarefa,
		type: "GET",
		dataType: "json",
		success: function(data) {
			$('#md_editar_tarefa').modal('toggle');			
			$('#frm_edit_tarefa_id').val(data.id);
			$('#frm_edit_tarefa_tarefa').val(data.tarefa);
			$('#frm_edit_tarefa_categoria option').each(function(i, v){
				if ($(v).val() == data.categoria.id){
					$(v).prop('selected', true);
				}
			});
			$('#frm_edit_tarefa_situacao option').each(function(i, v){
				if ($(v).val() == data.situacao.id){
					$(v).prop('selected', true);
				}
			});
		}
	});
}

function AtualizaTabelaTarefas(id_tarefa, tarefa, id_categoria, id_situacao)
{
	var select = `<select class="form-control" onchange="MudarSituacaoTarefa(this)" data-id_tarefa="${id_tarefa}">`;
	$(situacoes_tarefas).each(function(is, vs){
		selecionado = '';

		if (vs.id == id_situacao){
			selecionado = 'selected=""';
		}
		select += `<option value="${vs.id}" ${selecionado}>${vs.situacao}</option>`;
	});
	select += '</select>';

	var categoria = '';
	$(categorias_tarefas).each(function(is, vs){
		if (vs.id == id_categoria){
			categoria = vs.categoria;
		}
	});

	$(`[data-tr_id_tarefa="${id_tarefa}"]`).html('');
	$(`[data-tr_id_tarefa="${id_tarefa}"]`).append(
		`<td>${tarefa}</td>
		<td>${categoria}</td>
		<td>${select}</td>
		<td>
			<button class="btn btn-success btn-sm text-center" onclick="EditarTarefa(${id_tarefa})">
				<i class="fa fa-edit"></i>
			</button>
			<button class="btn btn-danger btn-sm text-center" onclick="ExcluirTarefa(${id_tarefa})">
				<i class="fa fa-trash"></i>
			</button>
		</td>`
	);
}
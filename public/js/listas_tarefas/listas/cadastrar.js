$(document).ready(function(){
	dados = {
		'lista': '',
		'dia': '',
		'data': '',
		'tarefas': []
	}

	$("#data").datepicker(date_picker_config);
	$('#cadastrar').on('click', function(e){
		$('#cadastrar').attr('disabled');

    	dados.lista = $("#lista").val();
    	dados.dia = $("#dia").val();
    	dados.data = $("#data").val();

		$.ajax({
			url: 'http://' + location.host + '/listas_tarefas/listas/salvar',
			type: "POST",
			data: dados,
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");
				}else if (data.status == "s"){
					LimpaCampos();
					$("#info").addClass("alert-success");
					$("#tb_tarefas tbody").html("");
					$("#container_ult_listas tbody").html("");
					dados.lista = '';
					dados.dia = '';
					dados.data = '';
					dados.tarefas = [];
					$(data.ult_listas).each(function(i, v){
						$("#container_ult_listas tbody").append(`
							<tr>
								<td>${v.lista}</td>
								<td>${v.data}</td>
								<td>${v.dia}</td>
							</tr>
						`);
					});
				}
				$("#info").show();
			},
			complete: function(data){
				$("#cadastrar").removeAttr("disabled");
			}
		});
	});

	$('#add_tarefa').on('click', function(){
		$("#inp_tarefa").val('');
		$('#md_add_tarefa').modal('toggle');
	});

	$('#bt_salvar_tarefa').on('click', function(){
		var tarefa = $("#inp_tarefa").val();
		var id_categoria = $("#inp_categoria").val();
		var categoria = $('#inp_categoria option:selected').text();
		
		dados.tarefas.push({
			'tarefa': tarefa,
			'id_categoria': id_categoria,
			'categoria': categoria
		})
        
        $('#tb_tarefas tbody').append(
			`
			<tr>
				<td>${tarefa}</td>
				<td>${categoria}</td>
				<td>
					<button class="btn btn-danger" onClick="ExcluirTarefa(this, '${tarefa}', '${categoria}')">
						<i class="fa fa-times"></i>
					</button>
				</td>
			</tr>
			`
        );
		$('#md_add_tarefa').modal('toggle');
	});
});

function LimpaCampos()
{
	$("#lista").val('');
	$("#data").val('');
}

function ExcluirTarefa(el, tarefa, categoria)
{
	$(el).parent().parent().remove();
	for(i in dados.tarefas){
		if (dados.tarefas[i].tarefa == tarefa && dados.tarefas[i].categoria == categoria){
			delete dados.tarefas[i];
		}
	}
}
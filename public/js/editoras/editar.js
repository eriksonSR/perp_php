$(document).ready(function(){
	$("#salvar").on('click', function(e){
		$("#salvar").prop("disabled",true);

		$.ajax({
			url: 'http://' + location.host + '/editoras/atualizar',
			type: "POST",
			data: {'id_editora' :  $('#id_editora').val(), 'editora' :  $('#editora').val()},
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");
				}else if (data.status == "s"){
					$("#info").addClass("alert-success");
				}
				$("#info").show();
				window.scrollTo(0, 0);
			},
			complete: function(data){
				$("#salvar").removeAttr("disabled");
			}
		});
	});
});
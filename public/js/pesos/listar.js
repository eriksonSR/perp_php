$(document).ready(function(){
	$('.peso').mask('#00,00', {reverse: true});
	$('.data').datepicker(date_picker_config);

	$('#bt_filtros').on('click', function(){
		$("#div_filtros").toggle();
	});

	$('#bt_modal_add_peso').on('click', function(){
		$('#md_add_peso').modal('toggle');
	});	

	$('#bt_add_peso').on('click', function(e){
		var dados = {
			'data': $('#frm_add_peso_data').val(),
			'peso': $('#frm_add_peso_peso').val()
		}
		
		$.ajax({
			url: 'http://' + location.host + '/pesos/adicionar/',
			type: "POST",
			data: dados,
			dataType: "json",
			success: function(data) {
				$('#md_add_peso').modal('toggle');
				if (data.status = 's'){
					$('#tb_pesos tbody').html('');
					$.getJSON('http://' + location.host + '/pesos/listar_json/', function(pesos){						
					  	$.each(pesos, function(i, v) {
					    	$("#tb_pesos tbody").append(`
								<tr>
									<td>${pesos[i].data_c_dia}</td>
									<td>${pesos[i].peso_br}</td>
								</tr>
							`);
					  	});
					});

				}
				alert(data.msg);
			}
		});

    	return false;
	});
});

function Excluir(id, el, peso){
	if (confirm("Deseja realmente excluir o peso " + peso + "?")){
		$.ajax({
			url: 'http://' + location.host + '/pesos/excluir/' + id,
			type: "DELETE",
			dataType: "json",
			success: function(data){
				if (data.status == 'Sucesso'){
					$(el).parent().parent().remove();
				}
				alert(data.msg);
			}
		});
	}
}
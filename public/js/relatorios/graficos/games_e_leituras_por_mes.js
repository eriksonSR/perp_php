$(document).ready(function(){
	chart = null;
	exibeGrafico();

	$('#bt_gera_grafico').on('click', function(){
		exibeGrafico(chart);
	});
});

function exibeGrafico(){
	var ano = $('#ano').val();
	if(ano.length == 0){
		alert('Informe um ano');
		return false;
	}

	if(parseInt(ano) < 2000){
		alert('Informe um ano válido');
		return false;
	}

	if(chart != null){
		console.log('Tem dados, destruindo');
		chart.destroy();
	}else{
		console.log('Não tem dados');
	}

	var options = {
		type: 'bar',
		data: {
			labels: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
			datasets:[]
		},
		options: {
			responsive: true,
			legend: {
				position: 'top',
			},
			title: {
				display: true,
				text: 'Games, livros, hqs e mangás finalizados por mês'
			},
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true,
						stepSize: 1
					}
				}]
			}
		}
	}

	var dataset_games = {
		label: 'Games',
		backgroundColor: 'red',
		data: [0,0,0,0,0,0,0,0,0,0,0,0]
	};

	var dataset_livros = {
		label: 'Livros',
		backgroundColor: 'blue',
		data: [0,0,0,0,0,0,0,0,0,0,0,0]
	};

	var dataset_hqs = {
		label: 'Hqs',
		backgroundColor: 'green',
		data: [0,0,0,0,0,0,0,0,0,0,0,0]
	};

	var dataset_mangas = {
		label: 'Mangás',
		backgroundColor: 'yellow',
		data: [0,0,0,0,0,0,0,0,0,0,0,0]
	};

	$.ajax({
		url: 'http://' + location.host + '/relatorios/games_e_leituras_por_mes_json',
		type: "POST",
		data: {"ano":ano},
		dataType: "json",
		success: function(data) {
			setaValorMes(dataset_games, data.games)
			options.data.datasets.push(dataset_games)
			setaValorMes(dataset_livros, data.livros)
			options.data.datasets.push(dataset_livros)
			setaValorMes(dataset_hqs, data.hqs)
			options.data.datasets.push(dataset_hqs)
			setaValorMes(dataset_mangas, data.mangas)
			options.data.datasets.push(dataset_mangas)

			var ctx = document.getElementById('grafico_games_e_leituras').getContext('2d');
			chart = new Chart(ctx, options);
		}
	});
}

function setaValorMes(obj, dados) {
	$(dados).each(function(i, v){
		switch(v.mes){
			case 'Jan':
				obj.data[0] = v.total;
				break;
			case 'Fev':
				obj.data[1] = v.total;
				break;
			case 'Mar':
				obj.data[2] = v.total;
				break;
			case 'Abr':
				obj.data[3] = v.total;
				break;
			case 'Mai':
				obj.data[4] = v.total;
				break;
			case 'Jun':
				obj.data[5] = v.total;
				break;
			case 'Jul':
				obj.data[6] = v.total;
				break;
			case 'Ago':
				obj.data[7] = v.total;
				break;
			case 'Set':
				obj.data[8] = v.total;
				break;
			case 'Out':
				obj.data[9] = v.total;
				break;
			case 'Nov':
				obj.data[10] = v.total;
				break;
			case 'Dez':
				obj.data[11] = v.total;
				break;
		}
	});
}
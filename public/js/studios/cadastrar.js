$(document).ready(function(){
	$("#cadastrar").on('click', function(e){
		$("#cadastrar").prop("disabled",true);

		$.ajax({
			url: 'http://' + location.host + '/studios/salvar',
			type: "POST",
			data: {'studio' : $('#studio').val()},
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");
				}else if (data.status == "s"){
					LimpaCampos();
					$("#info").addClass("alert-success");
				}
				$("#info").show();
			},
			complete: function(data){
				$("#cadastrar").removeAttr("disabled");
			}
		});
	});
});

function LimpaCampos()
{
	$('#studio').val('');
}
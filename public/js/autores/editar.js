$(document).ready(function(){
	$("#cadastrar").on('click', function(e){
		$("#cadastrar").prop("disabled",true);

		$.ajax({
			url: 'http://' + location.host + '/autores/atualizar',
			type: "POST",
			data: {'id_autor' :  $('#id_autor').val(), 'autor' :  $('#autor').val()},
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");
				}else if (data.status == "s"){
					$("#info").addClass("alert-success");
				}
				$("#info").show();
				window.scrollTo(0, 0);
			},
			complete: function(data){
				$("#cadastrar").removeAttr("disabled");
			}
		});
	});
});
$(document).ready(function(){
	$(".data").datepicker(date_picker_config);
	
	$("#add_screen").on('click', function(e){
		qtd_screens = qtd_screens + 1;
		add_input_url('screen_', qtd_screens, 'div_bt_add_screen', '');
	});
	
	$("#add_fanart").on('click', function(e){
		qtd_fanarts = qtd_fanarts + 1;
		add_input_url('fanart_', qtd_fanarts, 'div_bt_add_fanart', '');
	});

	$("#dwd_dados").on('click', function(e){
		var url_igdb = $("#url_igdb").val();
		$("#dwd_dados").prop("disabled",true);

		$.ajax({
			url: 'http://' + location.host + '/games/infos_apis/',
			data: {'url_igdb': url_igdb},
			type: "GET",
			dataType: "json",
			success: function(data){
				$('#titulo').val(data.titulo);
				$('#sinopse').val(data.sinopse);
				$('#data_release').val(data.dt_release);
				$('#id_igdb').val(data.id_igdb);
			},
			complete: function(data){
				$("#dwd_dados").removeAttr("disabled");
			}
		});
	});

	$("#cadastrar").on('click', function(e){
		$("#cadastrar").prop("disabled",true);
		game = {
			id_igdb: $('#id_igdb').val(),
			url_igdb: $('#url_igdb').val(),
			titulo: $('#titulo').val(),
			sinopse: $('#sinopse').val(),
			id_console: $('#id_console').val(),
			id_origem: $('#id_origem').val(),
			ids_publicadoras: [],
			ids_desenvolvedoras: [],
			ids_generos: [],
			id_situacao: $('#id_situacao').val(),
			data_release: $('#data_release').val(),
			data_termino: $('#data_termino').val(),
			digital: '',
			dlc: '',
			tenho: ''
		}

		if($("#digital").is(':checked')){
			game.digital = $("#digital").val();
		}else{
			game.digital = 0;
		}

		if($("#dlc").is(':checked')){
			game.dlc = $("#dlc").val();
		}else{
			game.dlc = 0;
		}

		if($("#tenho").is(':checked')){
			game.tenho = $("#tenho").val();
		}else{
			game.tenho = 0;
		}

		$("#ids_publicadoras option:selected").each(function(){
			game.ids_publicadoras.push($(this).val());
		});

		$("#ids_desenvolvedoras option:selected").each(function(){
			game.ids_desenvolvedoras.push($(this).val());
		});

		$("#ids_generos option:selected").each(function(){
			game.ids_generos.push($(this).val());
		});
		$.ajax({
			url: 'http://' + location.host + '/games/salvar',
			type: "POST",
			data: game,
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");
				}else if (data.status == "s"){
					LimpaCampos();
					$("#info").addClass("alert-success");
				}
				$("#info").show();
				window.scrollTo(0, 0);
			},
			complete: function(data){
				$("#cadastrar").removeAttr("disabled");
			}
		});
	});

	$('#add_studio').on('click', function(e){
		$('#md_add_studio').modal('toggle');
		$('#studio').val();
	});

	$('#bt_add_studio').on('click', function(e){
		$("#bt_add_studio").prop("disabled",true);
		$.ajax({
			url: 'http://' + location.host + '/studios/salvar',
			type: "POST",
			data: {'studio' : $('#studio').val()},
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");
				}else if (data.status == "s"){
					$("#info").addClass("alert-success");
					$('#ids_publicadoras option:first').after($('<option />', { 'value': data.id, 'text': $('#studio').val()}));
					$('#ids_desenvolvedoras option:first').after($('<option />', { 'value': data.id, 'text': $('#studio').val()}));
				}
				$("#info").show();
			},
			complete: function(data){
				$("#bt_add_studio").removeAttr("disabled");
				$('#studio').val('');
				$('#md_add_studio').modal('toggle');
				$("select option[value='add_studio']").prop("selected", false);
			}
		});
	});	

	$('#ids_publicadoras').select2();
	$('#ids_desenvolvedoras').select2();
	$('#ids_generos').select2();
});

function LimpaCampos()
{
	$('#id_igdb').val('');
	$('#id_gdb').val('');
	$('#id_origem').prop("selectedIndex", 0);
	$('#titulo').val('');
	$('#sinopse').val('');
	$('#id_console').prop("selectedIndex", 0);
	$('#ids_publicadoras').val('').trigger('change');
	$('#ids_desenvolvedoras').val('').trigger('change');
	$('#ids_generos').val('').trigger('change');
	$('#id_situacao').prop("selectedIndex", 0);
	$('#dlc').prop('checked', false);
	$('#digital').prop('checked', false);
	$('#tenho').prop('checked', false);
	$('#data_release').val('');
	$('#data_termino').val('');
}
$(document).ready(function(){
	$(".data").datepicker(date_picker_config);

	$('#bt_filtros').on('click', function(){
		$("#div_filtros").toggle();
	});

	$('#bt_filtrar').on('click', function(e){
		filtros = {
			'titulo' : $('#titulo').val(),
			'ids_plataformas' : [],
			'ids_consoles' : [],
			'ids_generos' : [],
			'id_situacao' : '',
			'ids_publicadoras' : [],
			'ids_desenvolvedoras' : [],
			'dlc' : 0,
			'digital' : 0,
			'tenho' : 0,
			'criterio_ini_dt_finalizacao' : $('#criterio_ini_dt_finalizacao :selected').val(),
			'data_ini' : $('#data_ini').val(),
			'criterio_final_dt_finalizacao' : $('#criterio_final_dt_finalizacao :selected').val(),
			'data_final' : $('#data_final').val()
		}

		$.each($("#ids_plataformas :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_plataformas.push($(this).val());
			}
		});

		$.each($("#ids_consoles :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_consoles.push($(this).val());
			}
		});

		$.each($("#ids_generos :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_generos.push($(this).val());
			}
		});

		$.each($("#id_situacao :selected"), function(){
			if($(this).val() > 0){
				filtros.id_situacao = $(this).val();
			}
		});

		$.each($("#ids_publicadoras :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_publicadoras.push($(this).val());
			}
		});

		$.each($("#ids_desenvolvedoras :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_desenvolvedoras.push($(this).val());
			}
		});

		if($("#dlc").is(':checked')){
			filtros.dlc = $("#dlc").val();
		}

		if($("#digital").is(':checked')){
			filtros.digital = $("#digital").val();
		}

		if($("#tenho").is(':checked')){
			filtros.tenho = $("#tenho").val();
		}

		$.ajax({
			url: 'http://' + location.host + '/games/get_games_json',
			type: "POST",
			data: {"filtros":filtros},
			dataType: "json",
			success: function(data) {
				$('#tb_games tbody').html('')
				$(data).each(function(i, v){
					$('#tb_games tbody').append(						
						`<tr>
							<td>${v.titulo}</td>
							<td>${v.console}</td>
							<td>${v.publicadoras}</td>
							<td>${v.digital}</td>
							<td>
								<a href='editar/${v.id}' class='btn btn-success btn-sm text-center' target='_blank'>
									<i class="fa fa-edit"></i>
								</a>
							</td>
							<td>
								<a href='ficha/${v.id}' class='btn btn-success btn-sm text-center' target='_blank'>
									<i class="fa fa-search"></i>
								</a>
							</td>
						`);
				});
			}
		});

    	return false;
	});

	$('#bt_resetar').on('click', function(e){
		$('#ids_plataformas').prop("selectedIndex", 0);
		$('#id_situacao').prop("selectedIndex", 0);
		$('#ids_consoles').prop("selectedIndex", 0);
		$('#ids_generos').prop("selectedIndex", 0);
		$('#ids_publicadoras').prop("selectedIndex", 0);
		$('#ids_desenvolvedoras').prop("selectedIndex", 0);
		$('#criterio_ini_dt_finalizacao').prop("selectedIndex", 0);
		$('#criterio_final_dt_finalizacao').prop("selectedIndex", 0);
		$('#titulo').val('');
		$('#data_ini').val('');
		$('#data_final').val('');
		$('#dlc').prop('checked', false);
		$('#digital').prop('checked', false);
		$('#tenho').prop('checked', false);
		
		$.ajax({
			url: 'http://' + location.host + '/games/get_games_json',
			type: "POST",
			data: {"filtros":''},
			dataType: "json",
			success: function(data) {
				$('#tb_games tbody').html('')
				$(data).each(function(i, v){
					$('#tb_games tbody').append(						
						`<tr>
							<td>${v.titulo}</td>
							<td>${v.console}</td>
							<td>${v.publicadoras}</td>
							<td>${v.digital}</td>
							<td>
								<a href='editar/${v.id}' class='btn btn-success btn-sm text-center' target='_blank'>
									<i class="fa fa-edit"></i>
								</a>
							</td>
							<td>
								<a href='ficha/${v.id}' class='btn btn-success btn-sm text-center' target='_blank'>
									<i class="fa fa-search"></i>
								</a>
							</td>
						`);
				});
			}
		});

    	return false;
	});

	$('#bt_excel').on('click', function(e){
		filtros = {
			'titulo' : $('#titulo').val(),
			'ids_plataformas' : [],
			'ids_consoles' : [],
			'ids_generos' : [],
			'id_situacao' : '',
			'ids_publicadoras' : [],
			'ids_desenvolvedoras' : [],
			'dlc' : 0,
			'digital' : 0,
			'tenho' : 0,
			'criterio_ini_dt_finalizacao' : $('#criterio_ini_dt_finalizacao :selected').val(),
			'data_ini' : $('#data_ini').val(),
			'criterio_final_dt_finalizacao' : $('#criterio_final_dt_finalizacao :selected').val(),
			'data_final' : $('#data_final').val()
		}

		$.each($("#ids_plataformas :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_plataformas.push($(this).val());
			}
		});

		$.each($("#ids_consoles :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_consoles.push($(this).val());
			}
		});

		$.each($("#ids_generos :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_generos.push($(this).val());
			}
		});

		$.each($("#id_situacao :selected"), function(){
			if($(this).val() > 0){
				filtros.id_situacao = $(this).val();
			}
		});

		$.each($("#ids_publicadoras :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_publicadoras.push($(this).val());
			}
		});

		$.each($("#ids_desenvolvedoras :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_desenvolvedoras.push($(this).val());
			}
		});

		if($("#dlc").is(':checked')){
			filtros.dlc = $("#dlc").val();
		}

		if($("#digital").is(':checked')){
			filtros.digital = $("#digital").val();
		}

		if($("#tenho").is(':checked')){
			filtros.tenho = $("#tenho").val();
		}
		
		var url = 'http://' + location.host + '/games/gera_excel?' + decodeURIComponent($.param(filtros));
    	window.open(url, '_blank');
	});
});
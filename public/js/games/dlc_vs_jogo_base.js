$(document).ready(function(){
	$('.bt_visualizar').on('click', function(e){
		filtros = {'dlc' : $(this).data('tipo')}

		$.ajax({
			url: 'http://' + location.host + '/games/get_games_json',
			type: "POST",
			data: {"filtros":filtros},
			dataType: "json",
			success: function(data) {
				$('#tb_games tbody').html('')
				$(data).each(function(i, v){
					$('#tb_games tbody').append('<tr><td>' 
						+ v.titulo 
						+ '</td><td>' 
						+ v.console 
						+ '</td><td class="text-center">'
						+ '<a href="' + window.location.origin + '/games/ficha/' + v.id 
						+ '" class="btn btn-success btn-sm text-center" target="_blank">'
						+ '<i class="fa fa-search"></i></a></td></tr>');
				});
				$('#md_games_visualizar').modal('toggle');
			}
		});
    	return false;
	});
});
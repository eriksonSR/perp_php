$(document).ready(function(){
	$(".data").datepicker(date_picker_config);

	$("#salvar").on('click', function(e){
		$("#salvar").prop("disabled",true);
		game = {
			id_game: $('#id_game').val(),
			id_igdb: $('#id_igdb').val(),
			url_igdb: $('#url_igdb').val(),
			titulo: $('#titulo').val(),
			sinopse: $('#sinopse').val(),
			id_console: $('#id_console').val(),
			id_origem: $('#id_origem').val(),
			ids_publicadoras: [],
			ids_desenvolvedoras: [],
			ids_generos: [],
			id_situacao: $('#id_situacao').val(),
			data_release: $('#data_release').val(),
			data_termino: $('#data_termino').val(),
			digital: '',
			dlc: '',
			tenho: ''
		}

		if($("#digital").is(':checked')){
			game.digital = $("#digital").val();
		}else{
			game.digital = 0;
		}

		if($("#dlc").is(':checked')){
			game.dlc = $("#dlc").val();
		}else{
			game.dlc = 0;
		}

		if($("#tenho").is(':checked')){
			game.tenho = $("#tenho").val();
		}else{
			game.tenho = 0;
		}

		$("#ids_publicadoras option:selected").each(function(){
			game.ids_publicadoras.push($(this).val());
		});

		$("#ids_desenvolvedoras option:selected").each(function(){
			game.ids_desenvolvedoras.push($(this).val());
		});

		$("#ids_generos option:selected").each(function(){
			game.ids_generos.push($(this).val());
		});
		$.ajax({
			url: 'http://' + location.host + '/games/atualizar',
			type: "POST",
			data: game,
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");rt
				}else if (data.status == "s"){
					$("#info").addClass("alert-success");
				}
				$("#info").show();
				window.scrollTo(0, 0);
			},
			complete: function(data){
				$("#salvar").removeAttr("disabled");
			}
		});
	});
});
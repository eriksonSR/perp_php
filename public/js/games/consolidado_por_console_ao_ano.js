$(document).ready(function(){
	$('#bt_filtrar').on('click', function(e){
		var ano = $('#ano').val();
		var consoles = []

		$("#consoles option:selected").each(function(){
			consoles.push($(this).val());
		});

		$.ajax({
			url: 'http://' + location.host + '/relatorios/games/consolidado_por_console_ao_ano_json',
			type: "POST",
			data: {"ano":ano, "consoles":consoles},
			dataType: "json",
			success: function(data) {
				$('#tb_relatorio tbody').html('')
				$(data).each(function(i, v){
					var tr = $("<tr></tr>");
					var td_cons = $("<td>" + v.console + "</td>");
					var td_ano = $("<td>" + v.ano + "</td>");
					var td_total = $("<td>" + v.total + "</td>");
					var td_btn = $("<td class='text-center'></td>");
					var span_btn = $("<span class='fas fa-search bt_visualizar' onclick=\"visualizar_games('" + v.console + "', '" + v.ano + "')\")></span>");
					$(tr).append(td_cons);
					$(tr).append(td_ano);
					$(tr).append(td_total);
					$(td_btn).append(span_btn);
					$(tr).append(td_btn);
					$('#tb_relatorio tbody').append(tr);
				});
			}
		});

    	return false;
	});

	$('#bt_filtros').on('click', function(){
		$("#div_filtros").toggle();
	})

	$('#resetar_filtros').on('click', function(e){
		$("#consoles").val('');
		$('#ano').val('');
		$.ajax({
			url: 'http://' + location.host + '/relatorios/games/consolidado_por_console_ao_ano_json',
			type: "POST",
			data: {"ano":""},
			dataType: "json",
			success: function(data) {
				$('#tb_relatorio tbody').html('');
				$(data).each(function(i, v){
					var tr = $("<tr></tr>");
					var td_cons = $("<td>" + v.console + "</td>");
					var td_ano = $("<td>" + v.ano + "</td>");
					var td_total = $("<td>" + v.total + "</td>");
					var td_btn = $("<td class='text-center'></td>");
					var span_btn = $("<span class='fas fa-search bt_visualizar' onclick=\"visualizar_games('" + v.console + "', '" + v.ano + "')\")></span>");
					$(tr).append(td_cons);
					$(tr).append(td_ano);
					$(tr).append(td_total);
					$(td_btn).append(span_btn);
					$(tr).append(td_btn);
					$('#tb_relatorio tbody').append(tr);
				});
			}
		});

    	return false;
	});
});

function visualizar_games(console, ano){
		filtros = {'ano' : ano, 'console_text': console}
		
		$.ajax({
			url: 'http://' + location.host + '/games/get_games_json',
			type: "POST",
			data: {"filtros":filtros},
			dataType: "json",
			success: function(data) {
				$('#tb_games tbody').html('')
				$(data).each(function(i, v){
					$('#tb_games tbody').append('<tr><td>' 
						+ v.titulo 
						+ '</td><td>' 
						+ v.console 
						+ '</td><td class="text-center">'
						+ '<a href="' + window.location.origin + '/games/ficha/' + v.id 
						+ '" class="btn btn-success btn-sm text-center" target="_blank">'
						+ '<i class="fa fa-search"></i></a></td></tr>');
				});
				$('#md_games_visualizar').modal('toggle');
			}
		});
    	return false;
}
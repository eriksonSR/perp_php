$(document).ready(function(){
	$('#bt_filtrar').on('click', function(e){
		var ano = $('#ano').val();
		var generos = []

		$("#generos option:selected").each(function(){
			generos.push($(this).val());
		});

		$.ajax({
			url: 'http://' + location.host + '/relatorios/games/consolidado_por_genero_ao_ano_json',
			type: "POST",
			data: {"ano":ano, "generos":generos},
			dataType: "json",
			success: function(data) {
				$('#tb_relatorio tbody').html('')
				$(data).each(function(i, v){
					$('#tb_relatorio tbody').append('<tr><td>' 
						+ v.genero 
						+ '</td><td>' 
						+ v.ano
						+ '</td><td>'
						+ v.total 
						+ '</td><td class="text-center">'
						+ '<span class="fas fa-search" onclick="visualizar_games(\'' + v.genero +'\',\'' + v.ano + '\')"></span></td></tr>');
				});
			}
		});

    	return false;
	});

	$('#bt_filtros').on('click', function(){
		$("#div_filtros").toggle();
	})

	$('#resetar_filtros').on('click', function(e){
		$("#generos").val('');
		$('#ano').val('');
		$.ajax({
			url: 'http://' + location.host + '/relatorios/games/consolidado_por_genero_ao_ano_json',
			type: "POST",
			data: {"ano":"", "generos":""},
			dataType: "json",
			success: function(data) {
				$('#tb_relatorio tbody').html('')
				$(data).each(function(i, v){
					$('#tb_relatorio tbody').append('<tr><td>' 
						+ v.genero 
						+ '</td><td>' 
						+ v.ano
						+ '</td><td>'
						+ v.total 
						+ '</td><td class="text-center">'
						+ '<span class="fas fa-search" onclick="visualizar_games(\'' + v.genero +'\',\'' + v.ano + '\')"></span></td></tr>');
				});
			}
		});

    	return false;
	});
});

function visualizar_games(genero, ano){
	filtros = {'genero_text': genero, 'ano':ano}
		
	$.ajax({
		url: 'http://' + location.host + '/games/get_games_json',
		type: "POST",
		data: {"filtros":filtros},
		dataType: "json",
		success: function(data) {
			$('#tb_games tbody').html('')
			$(data).each(function(i, v){
				$('#tb_games tbody').append('<tr><td>' 
					+ v.titulo 
					+ '</td><td>' 
					+ v.console 
					+ '</td><td class="text-center">'
					+ '<a href="' + window.location.origin + '/games/ficha/' + v.id 
					+ '" class="btn btn-success btn-sm text-center" target="_blank">'
					+ '<i class="fa fa-search"></i></a></td></tr>');
			});
			$('#md_games_visualizar').modal('toggle');
		}
	});
    return false;
}
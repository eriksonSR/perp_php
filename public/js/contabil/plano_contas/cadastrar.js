$(document).ready(function(){
	$('#cadastrar').on('click', function(e){
    	var conta = $("#conta").val();
    	var tipo = $("#tipo").val();
    	var analitica = 0;
    	if($("#analitica").is(':checked')){
    		analitica = 1;
    	}
    	var id_conta_mae = $("#id_conta_mae").val();
    	var grau = $("#grau").val();

		$.ajax({
			url: 'http://' + location.host + '/contabil/plano_contas/salvar',
			type: "POST",
			data: {'conta':conta, 'tipo':tipo, 'analitica':analitica, 'id_conta_mae':id_conta_mae, 'grau':grau},
			dataType: "json",
			success: function(data) {
				if(data > 0 && analitica == 0){
					conta_mae = $("#id_conta_mae");
					conta_mae.prepend('<option value="' + data + '">' + conta + '</option>');
				}
			}
		});
	});

	$('#tipo').on('change', function(e){
		var filtros = {'tipo':$("#tipo").val(), 'analitica':0}

		if(tipo != ''){
			conta_mae = $("#id_conta_mae");
			conta_mae.html('');
			$.ajax({
				url: 'http://' + location.host + '/contabil/plano_contas/get_contas_json',
				type: "POST",
				data: {"filtros":filtros},
				dataType: "json",
				success: function(data) {
					conta_mae.append('<option value=""></option>')
					$(data).each(function(i, v){
						conta_mae.append('<option value="' + v.id + '">' + v.conta + '</option>')
					});
				}
			});
		}
	});
});
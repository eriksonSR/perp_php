$(document).ready(function(){
	$("#valor").maskMoney(mask_money_config);
	$("#data").datepicker(date_picker_config);

	$('#cadastrar').on('click', function(e){
    	var historico = $("#historico").val();
    	var id_conta_debitada = $("#id_conta_debitada").val();
    	var id_conta_creditada = $("#id_conta_creditada").val();
    	var valor = $("#valor").val();
    	var data = $("#data").val();

		$.ajax({
			url: 'http://' + location.host + '/contabil/lancamentos/salvar',
			type: "POST",
			data: {'historico':historico, 'id_conta_debitada':id_conta_debitada, 'id_conta_creditada':id_conta_creditada, 'data':data, 'valor':valor},
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");
				}else if (data.status == "s"){
					LimpaCampos();
					$("#info").addClass("alert-success");
					$("#container_ult_lancamentos_hist tbody").html("");
					$("#container_ult_lancamentos_inc tbody").html("");
					$(data.lancamentos_hist).each(function(i, v){
						$("#container_ult_lancamentos_hist tbody").append(`
							<tr>
								<td>${v.historico}</td>
								<td>${v.data}</td>
								<td>${v.valor}</td>
								<td>${v.cc}</td>
								<td>${v.cd}</td>
							</tr>
						`);
					});
					$(data.lancamentos_inc).each(function(i, v){
						$("#container_ult_lancamentos_inc tbody").append(`
							<tr>
								<td>${v.historico}</td>
								<td>${v.data}</td>
								<td>${v.valor}</td>
								<td>${v.cc}</td>
								<td>${v.cd}</td>
							</tr>
						`);
					});
				}
				$("#info").show();
			},
			complete: function(data){
				$("#cadastrar").removeAttr("disabled");
			}
		});
	});
});

function LimpaCampos()
{
	$("#historico").val('');
	$("#valor").val('');
}
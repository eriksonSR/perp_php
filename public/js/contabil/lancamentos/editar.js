$(document).ready(function(){
	$("#valor").maskMoney(mask_money_config);
	$("#data").datepicker(date_picker_config);
	
	$('#bt_salvar_lancamento').on('click', function(e){
    	var id_lancamento = $("#id_lancamento").val();
    	var historico = $("#historico").val();
    	var id_conta_debitada = $("#id_conta_debitada").val();
    	var id_conta_creditada = $("#id_conta_creditada").val();
    	var valor = $("#valor").val();
    	var data = $("#data").val();
		$.ajax({
			url: 'http://' + location.host + '/contabil/lancamentos/atualizar',
			type: "POST",
			data: {
				'id_lancamento':id_lancamento, 
				'historico':historico, 
				'id_conta_debitada':id_conta_debitada, 
				'id_conta_creditada':id_conta_creditada, 
				'data':data, 
				'valor':valor
			},
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "Erro"){
					$("#info").addClass("alert-danger");
				}else{
					$("#info").addClass("alert-success");
				}
				$("#info").show();
			},
			complete: function(data){
				$("#salvar").removeAttr("disabled");
			}
		});
	});
});
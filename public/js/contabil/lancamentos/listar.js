$(document).ready(function(){
	$(".monetario").maskMoney(mask_money_config);
	$(".data").datepicker(date_picker_config);

	$('#bt_filtros').on('click', function(){
		$("#div_filtros").toggle();
	});

	$('#bt_filtrar').on('click', function(e){
		filtros = {
			'conta_cred' : $('#conta_cred :selected').val(),
			'conta_deb' : $('#conta_deb :selected').val(),
			'historico' : $('#historico').val(),
			'criterio_valor' : $('#criterio_valor').val(),
			'valor' : $('#valor').val(),
			'criterio_data_ini' : $('#criterio_data_ini').val(),
			'data_ini' : $('#data_ini').val(),
			'criterio_data_fim' : $('#criterio_data_fim').val(),
			'data_fim' : $('#data_fim').val()
		}

		$.ajax({
			url: 'http://' + location.host + '/contabil/lancamentos/get_lancamentos_json',
			type: "POST",
			data: {"filtros":filtros},
			dataType: "json",
			success: function(data) {
				$('#tb_lancamentos tbody').html('')
				$(data).each(function(i, v){
					$('#tb_lancamentos tbody').append(
						`<tr>
							<td>${v.id}</td>
							<td>${v.c_cre}</td>
							<td>${v.c_deb}</td>
							<td>${v.historico}</td>
							<td>${v.valor}</td>
							<td>${v.dt}</td>
							<td class="text-center">
								<button class="btn btn-success btn-sm text-center" data-id_lancamento="${v.id}" onclick="ExibeFormEditaLancamento('${v.id}')">
									<i class="fa fa-edit"></i>
								</button>
							</td>
						</tr>`);
				});
			}
		});

    	return false;
	});

	$('#bt_resetar').on('click', function(e){
		$('#conta_cred').prop("selectedIndex", 0);
		$('#conta_deb').prop("selectedIndex", 0);
		$('#historico').val('');
		$('#criterio_valor').val('');
		$('#valor').val('');
		$('#criterio_data_ini').val('');
		$('#data_ini').val('');
		$('#criterio_data_fim').val('');
		$('#data_fim').val('');
		
		$.ajax({
			url: 'http://' + location.host + '/contabil/lancamentos/get_lancamentos_json',
			type: "POST",
			data: {"filtros":''},
			dataType: "json",
			success: function(data) {
				$('#tb_lancamentos tbody').html('')
				$(data).each(function(i, v){
					$('#tb_lancamentos tbody').append(
						`<tr>
							<td>${v.id}</td>
							<td>${v.c_cre}</td>
							<td>${v.c_deb}</td>
							<td>${v.historico}</td>
							<td>${v.valor}</td>
							<td>${v.dt}</td>
							<td class="text-center">
								<button class="btn btn-success btn-sm text-center" data-id_lancamento="${v.id}" onclick="ExibeFormEditaLancamento('${v.id}')">
									<i class="fa fa-edit"></i>
								</button>
							</td>
						</tr>`);
				});
			}
		});

    	return false;
	});

	$('#bt_excel').on('click', function(e){
		filtros = {
			'conta_cred' : $('#conta_cred :selected').val(),
			'conta_deb' : $('#conta_deb :selected').val(),
			'historico' : $('#historico').val(),
			'criterio_valor' : $('#criterio_valor').val(),
			'valor' : $('#valor').val(),
			'criterio_data_ini' : $('#criterio_data_ini').val(),
			'data_ini' : $('#data_ini').val(),
			'criterio_data_fim' : $('#criterio_data_fim').val(),
			'data_fim' : $('#data_fim').val()
		}
		
		var url = 'http://' + location.host + '/contabil/lancamentos/gera_excel?' + decodeURIComponent($.param(filtros));
    	window.open(url, '_blank');
	});

	$('#bt_salvar_lancamento').on('click', function(e){
		var dados = {
			'id_lancamento': $('#frm_id_lancamento').val(),
			'historico': $('#frm_edit_historico').val(),
			'data': $('#frm_edit_data').val(),
			'valor': $('#frm_edit_valor').val(),
			'id_conta_creditada': $('#frm_edit_cc').val(),
			'id_conta_debitada': $('#frm_edit_cd').val(),
			'conta_creditada': $('#frm_edit_cc :selected').text(),
			'conta_debitada': $('#frm_edit_cd :selected').text()
		}
		
		$.ajax({
			url: 'http://' + location.host + '/contabil/lancamentos/atualizar',
			type: "POST",
			data: dados,
			dataType: "json",
			success: function(data) {
				$('#md_editar_lancamento').modal('toggle');
				alert(data.msg);
				linha_lancamento = $('[data-id_lancamento="' + dados.id_lancamento + '"]').parent().parent();
				colunas = $(linha_lancamento).find('td');
				$(colunas).eq(1).text(dados.conta_creditada);
				$(colunas).eq(2).text(dados.conta_debitada);
				$(colunas).eq(3).text(dados.historico);
				$(colunas).eq(4).text('R$ ' + dados.valor);
				$(colunas).eq(5).text(dados.data);
			}
		});

    	return false;
	});
});

function ExibeFormEditaLancamento(id_lancamento){
	$('#frm_edit_cd').removeProp("selected");
	$('#frm_edit_cc').removeProp("selected");
	$('#md_editar_lancamento').modal('toggle');
		
	$.ajax({
		url: 'http://' + location.host + '/contabil/lancamentos/get_lancamento_json/' + id_lancamento,
		type: "GET",
		dataType: "json",
		success: function(data) {
			$('#frm_id_lancamento').val(id_lancamento);
			$('#frm_edit_historico').val(data.historico);
			$('#frm_edit_data').val(data.data);
			$('#frm_edit_valor').val(data.valor);
			$('#frm_edit_cd option').each(function(i, v){
				if ($(v).val() == data.id_conta_debitada){
					$(v).prop('selected', true);
				}
			});
			$('#frm_edit_cc option').each(function(i, v){
				if ($(v).val() == data.id_conta_creditada){
					$(v).prop('selected', true);
				}
			});
		}
	});
}
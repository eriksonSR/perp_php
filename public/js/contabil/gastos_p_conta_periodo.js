$(document).ready(function(){
	$('#bt_filtros').on('click', function(){
		$("#div_filtros").toggle();
	});

	$('#bt_filtrar').on('click', function(e){
		filtros = {
			'ano' : $('#ano').val(),
			'ids_contas' : []
		}

		$.each($("#id_conta :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_contas.push($(this).val());
			}
		});

		$.ajax({
			url: 'http://' + location.host + '/relatorios/contabil/gastos_p_conta_e_periodo_json',
			type: "POST",
			data: {"filtros":filtros},
			dataType: "json",
			success: function(data) {
				$('#tb_relatorio tbody').html('');
				$(data).each(function(i, v){
					$('#tb_relatorio tbody').append(						
						`<tr>
							<td>${v.conta}</td>
							<td class="text-center">${v.ano}</td>
							<td class="text-center">${v.mes}</td>
							<td class="text-center">${v.valor}</td>
							<td class="text-center">
								<button class="btn btn-success btn-sm text-center bt_visualizar" onclick="ExibeGastos('${v.conta}', '${v.id}', '${v.ano}', '${v.mes}')"><i class="fa fa-search"></i></button>
							</td>
						</tr>
					`);
				});
			}
		});

    	return false;
	});

	$('#bt_resetar').on('click', function(e){
		var filtros = {
			'ano' : '',
			'ids_contas' : []
		}

		$.ajax({
			url: 'http://' + location.host + '/relatorios/contabil/gastos_p_conta_e_periodo_json',
			type: "POST",
			data: {"filtros":filtros},
			dataType: "json",
			success: function(data) {
				$('#tb_relatorio tbody').html('');
				$(data).each(function(i, v){
					$('#tb_relatorio tbody').append(						
						`<tr>
							<td>${v.conta}</td>
							<td class="text-center">${v.ano}</td>
							<td class="text-center">${v.mes}</td>
							<td class="text-center">${v.valor}</td>
							<td class="text-center">
								<button class="btn btn-success btn-sm text-center bt_visualizar" onclick="ExibeGastos('${v.conta}', '${v.id}', '${v.ano}', '${v.mes}')"><i class="fa fa-search"></i></button>
							</td>
						</tr>
					`);
				});
			}
		});

    	return false;
	});
});

function ExibeGastos(conta, id_conta, ano, mes){
	var filtros = {
		'conta_cred': id_conta,
		'ano': ano,
		'mes': mes
	}		

	$('#md_lista_lanc_conta').text(conta);
	$('#md_lista_lanc_mes').text(mes);
	$('#md_lista_lanc_ano').text(ano);

	$.ajax({
		url: 'http://' + location.host + '/contabil/lancamentos/get_lancamentos_json',
		type: "POST",
		data: {"filtros":filtros},
		dataType: "json",
		success: function(data) {
			$('#tb_lancamentos tbody').html('');
			$(data).each(function(i, v){
				$('#tb_lancamentos tbody').append(
					`<tr>
						<td>${v.historico}</td>
						<td>${v.valor}</td>
						<td>${v.dt}</td>
					</tr>`);
			});
			$('#md_lista_gastos_p_conta_periodo').modal('toggle');
		}
	});
}
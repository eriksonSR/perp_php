$(document).ready(function(){
	cores_graficos = ['#1FB6F2','#FF4949','#51C15A','#794AD6','#FFEB38']

	ExibeRelAssuntosMaisLidosLivros();
	ExibeRelAssuntosMaisLidosHqs();
	ExibeRelPrincipaisGastos();
	ExibeRelConsolesMaisJogados();
	ExibeGraficoMidiasFinalizadasPorMes();
	ExibeGraficoMidiasFinalizadasVsNaoFinalizadas();
	ExibeGraficoDistanciasPorPeriodoExercicio();
	ExibeGraficoDistanciasTotaisPorExercicio();
});

function ExibeRelAssuntosMaisLidosLivros()
{
	var data_obj = JSON.parse(JSON.stringify($('#canvas_assuntos_livros_mais_lidos_grafico').data('dados_assuntos_livros_grafico')));
	var options = {
			type: 'doughnut',
			data: {
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
				labels: []
			},
			options: {
				responsive: true,
				legend: {
					position: 'right',
				},
				animation: {
					animateScale: true,
					animateRotate: true
				}
			}
		};

	$(data_obj).each(function(i, v){
		options.data.datasets[0].data.push(data_obj[i].qtd)
		options.data.datasets[0].backgroundColor.push(cores_graficos[i])
		options.data.labels.push(data_obj[i].assunto)
	});

	var ctx = document.getElementById('canvas_assuntos_livros_mais_lidos_grafico').getContext('2d');
	chart = new Chart(ctx, options);
}

function ExibeRelAssuntosMaisLidosHqs()
{
	var data_obj = JSON.parse(JSON.stringify($('#canvas_assuntos_hqs_mais_lidos_grafico').data('dados_assuntos_hqs_grafico')));
	var options = {
			type: 'doughnut',
			data: {
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
				labels: []
			},
			options: {
				responsive: true,
				legend: {
					position: 'right',
				},
				animation: {
					animateScale: true,
					animateRotate: true
				}
			}
		};	
	$(data_obj).each(function(i, v){
		options.data.datasets[0].data.push(data_obj[i].qtd)
		options.data.datasets[0].backgroundColor.push(cores_graficos[i])
		options.data.labels.push(data_obj[i].assunto)
	});

	var ctx = document.getElementById('canvas_assuntos_hqs_mais_lidos_grafico').getContext('2d');
	chart = new Chart(ctx, options);
}

function ExibeRelPrincipaisGastos()
{
	var data_obj = JSON.parse(JSON.stringify($('#canvas_principais_gastos_grafico').data('dados_principais_gastos_grafico')));
	var options = {
			type: 'doughnut',
			data: {
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
				labels: []
			},
			options: {
				responsive: true,
				legend: {
					position: 'right',
				},
				animation: {
					animateScale: true,
					animateRotate: true
				}
			}
		};	
	$(data_obj).each(function(i, v){
		options.data.datasets[0].data.push(data_obj[i].valor)
		options.data.datasets[0].backgroundColor.push(cores_graficos[i])
		options.data.labels.push(data_obj[i].conta)
	});

	var ctx = document.getElementById('canvas_principais_gastos_grafico').getContext('2d');
	chart = new Chart(ctx, options);
}

function ExibeRelConsolesMaisJogados()
{
	var data_obj = JSON.parse(JSON.stringify($('#canvas_consoles_mais_jogados_grafico').data('dados_consoles_mais_jogados_grafico')));
	var options = {
			type: 'doughnut',
			data: {
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
				labels: []
			},
			options: {
				responsive: true,
				legend: {
					position: 'right',
				},
				animation: {
					animateScale: true,
					animateRotate: true
				}
			}
		};
	$(data_obj).each(function(i, v){
		options.data.datasets[0].data.push(data_obj[i].total)
		options.data.datasets[0].backgroundColor.push(cores_graficos[i])
		options.data.labels.push(data_obj[i].console)
	});
	
	var ctx = document.getElementById('canvas_consoles_mais_jogados_grafico').getContext('2d');
	chart = new Chart(ctx, options);
}

function ExibeGraficoMidiasFinalizadasPorMes(){
	var data_livros_obj = JSON.parse(JSON.stringify($('#canvas_midias_fin_p_mes').data('dados_livros_finalizados_p_mes')));
	var data_hqs_obj = JSON.parse(JSON.stringify($('#canvas_midias_fin_p_mes').data('dados_hqs_finalizados_p_mes')));
	var data_mangas_obj = JSON.parse(JSON.stringify($('#canvas_midias_fin_p_mes').data('dados_mangas_finalizados_p_mes')));
	var data_games_obj = JSON.parse(JSON.stringify($('#canvas_midias_fin_p_mes').data('dados_games_finalizados_p_mes')));

	var options = {
		type: 'bar',
		data: {
			labels: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
			datasets:[]
		},
		options: {
			responsive: true,
			legend: {
				position: 'top',
			},
			title: {
				display: false
			},
			scales: {
				xAxes: [{
					stacked: true,
				}],
					yAxes: [{
					stacked: true
				}]
			}
		}
	}

	var dataset_games = {
		label: 'Games',
		backgroundColor: '#FF4949',
		data: [0,0,0,0,0,0,0,0,0,0,0,0]
	};

	var dataset_livros = {
		label: 'Livros',
		backgroundColor: '#1FB6F2',
		data: [0,0,0,0,0,0,0,0,0,0,0,0]
	};

	var dataset_hqs = {
		label: 'Hqs',
		backgroundColor: '#51C15A',
		data: [0,0,0,0,0,0,0,0,0,0,0,0]
	};

	var dataset_mangas = {
		label: 'Mangás',
		backgroundColor: '#FFEB38',
		data: [0,0,0,0,0,0,0,0,0,0,0,0]
	};

	setaValorMes(dataset_games, data_games_obj)
	options.data.datasets.push(dataset_games)

	setaValorMes(dataset_livros, data_livros_obj)
	options.data.datasets.push(dataset_livros)

	setaValorMes(dataset_hqs, data_hqs_obj)
	options.data.datasets.push(dataset_hqs)

	setaValorMes(dataset_mangas, data_mangas_obj)
	options.data.datasets.push(dataset_mangas)
	
	var ctx = document.getElementById('canvas_midias_fin_p_mes').getContext('2d');
	chart = new Chart(ctx, options);
}

function ExibeGraficoMidiasFinalizadasVsNaoFinalizadas(){
	var data = JSON.parse(JSON.stringify($('#canvas_midias_fin_x_nao_fin').data('dados_midias_fin_x_nao_fin')));
	var options = {
		type: 'bar',
		data: {
			labels: ['Livros', 'Hqs', 'Mangás', 'Games'],
			datasets:[]
		},
		options: {
			responsive: true,
			legend: {
				position: 'top',
			},
			title: {
				display: false
			},
			scales: {
				xAxes: [{
					stacked: true,
				}],
					yAxes: [{
					stacked: true
				}]
			}
		}
	}

	var dataset_finalizados = {
		label: 'Finalizados',
		backgroundColor: '#51C15A',
		data: [
			data.qtd_livros_finalizados[0].tot,
			data.qtd_hqs_finalizados[0].tot,
			data.qtd_mangas_finalizados[0].tot,
			data.qtd_games_finalizados[0].tot
		]
	};
	options.data.datasets.push(dataset_finalizados);
	
	var dataset_n_iniciados = {
		label: 'Não iniciados',
		backgroundColor: '#FF4949',
		data: [
			data.qtd_livros_nao_iniciados[0].tot,
			data.qtd_hqs_nao_iniciados[0].tot,
			data.qtd_mangas_nao_iniciados[0].tot,
			data.qtd_games_nao_iniciados[0].tot
		]
	};
	options.data.datasets.push(dataset_n_iniciados);
	
	var ctx = document.getElementById('canvas_midias_fin_x_nao_fin').getContext('2d');
	chart = new Chart(ctx, options);
}

function setaValorMes(obj, dados) {
	$(dados).each(function(i, v){
		switch(v.mes){
			case 'Jan':
				obj.data[0] = v.total;
				break;
			case 'Fev':
				obj.data[1] = v.total;
				break;
			case 'Mar':
				obj.data[2] = v.total;
				break;
			case 'Abr':
				obj.data[3] = v.total;
				break;
			case 'Mai':
				obj.data[4] = v.total;
				break;
			case 'Jun':
				obj.data[5] = v.total;
				break;
			case 'Jul':
				obj.data[6] = v.total;
				break;
			case 'Ago':
				obj.data[7] = v.total;
				break;
			case 'Set':
				obj.data[8] = v.total;
				break;
			case 'Out':
				obj.data[9] = v.total;
				break;
			case 'Nov':
				obj.data[10] = v.total;
				break;
			case 'Dez':
				obj.data[11] = v.total;
				break;
		}
	});
}

function ExibeGraficoDistanciasPorPeriodoExercicio(){
	var dados = JSON.parse(JSON.stringify($('#canvas_dist_p_periodo_e_exerc').data('dist_p_periodo_e_exerc')));

	var options = {
		type: 'bar',
		data: {
			labels: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
			datasets:[]
		},
		options: {
			responsive: true,
			legend: {
				position: 'top',
			},
			title: {
				display: false
			},
			scales: {
				xAxes: [{
					stacked: true,
				}],
					yAxes: [{
					stacked: true
				}]
			}
		}
	}

	var dataset_corrida = {
		label: 'Corrida',
		backgroundColor: '#FF4949',
		data: [0,0,0,0,0,0,0,0,0,0,0,0]
	};

	var dataset_pedalada = {
		label: 'Pedalada',
		backgroundColor: '#1FB6F2',
		data: [0,0,0,0,0,0,0,0,0,0,0,0]
	};

	var dataset_caminhada = {
		label: 'Caminhada',
		backgroundColor: '#51C15A',
		data: [0,0,0,0,0,0,0,0,0,0,0,0]
	};

	var dataset_corrida_e_caminhada = {
		label: 'Corrida e caminhada',
		backgroundColor: '#FFEB38',
		data: [0,0,0,0,0,0,0,0,0,0,0,0]
	};

	$(dados).each(function(i, v){
		switch(v.tipo){
			case 'Corrida':
				dataset_corrida.data[parseInt(v.mes) - 1] = v.distancia
				break;
			case 'Pedalada':
				dataset_pedalada.data[parseInt(v.mes) - 1] = v.distancia
				break;
			case 'Caminhada':
				dataset_caminhada.data[parseInt(v.mes) - 1] = v.distancia
				break;
			case 'Corrida e caminhada':
				dataset_corrida_e_caminhada.data[parseInt(v.mes) - 1] = v.distancia
				break;
		}
	});

	options.data.datasets.push(dataset_corrida)
	options.data.datasets.push(dataset_pedalada)
	options.data.datasets.push(dataset_caminhada)
	options.data.datasets.push(dataset_corrida_e_caminhada)
	
	var ctx = document.getElementById('canvas_dist_p_periodo_e_exerc').getContext('2d');
	chart = new Chart(ctx, options);
}

function ExibeGraficoDistanciasTotaisPorExercicio(){
	var data_obj = JSON.parse(JSON.stringify($('#canvas_dist_tot_p_exerc').data('dist_tot_p_exercio')));
	var options = {
			type: 'doughnut',
			data: {
				datasets: [{
					data: [],
					backgroundColor: [],
				}],
				labels: []
			},
			options: {
				responsive: true,
				legend: {
					position: 'right',
				},
				animation: {
					animateScale: true,
					animateRotate: true
				}
			}
		};
	$(data_obj).each(function(i, v){
		options.data.datasets[0].data.push(data_obj[i].distancia)
		options.data.datasets[0].backgroundColor.push(cores_graficos[i])
		options.data.labels.push(data_obj[i].tipo)
	});
	
	var ctx = document.getElementById('canvas_dist_tot_p_exerc').getContext('2d');
	chart = new Chart(ctx, options);
}
$(document).ready(function(){
	$("#data").datepicker(date_picker_config),
	$("#dwd_dados_skoob").on('click', function(e){
		var id_skoob = $("#id_skoob").val();
		var url_json_nao_lido = 'https://www.skoob.com.br/v1/book/' + id_skoob;

		$.ajax({
			url: url_json_nao_lido,
			type: "GET",
			dataType: 'json',
			success: function(data) {
				$("#titulo").val(data.response.titulo);
				$("#subtitulo").val(data.response.subtitulo);
				$("#ano").val(data.response.ano);
				$("#num_pags").val(data.response.paginas);
				$("#sinopse").val(data.response.sinopse);
				$("#sinopse").val(data.response.sinopse);
				$("#url_capa").val(data.response.capa_grande);
			}
		});
	});

	$("#salvar").on('click', function(e){
		$("#salvar").prop("disabled",true);
		leitura = {
			id: $("#id_leitura").val(),
			id_situacao : $("#id_situacao").val(),
			id_skoob : $("#id_skoob").val(),
			titulo : $("#titulo").val(),
			subtitulo : $("#subtitulo").val(),
			isbn : $("#isbn").val(),
			ids_autores : [],
			ids_editoras : [],
			ids_assuntos : [],
			ano : $("#ano").val(),
			num_pags : $("#num_pags").val(),
			nota : $("#nota").val(),
			id_tipo_midia : $("#id_tipo_midia").val(),
			data : $("#data").val(),
			digital : '',
			favorito : '',
			sinopse : $("#sinopse").val(),
			resenha : $("#resenha").val(),
			marcacoes : $("#marcacoes").val(),
			url_capa : $("#url_capa").val()
		}

		if($("#favorito").is(':checked')){
			leitura.favorito = $("#favorito").val();
		}else{
			leitura.favorito = 0;
		}

		if($("#digital").is(':checked')){
			leitura.digital = $("#digital").val();
		}else{
			leitura.digital = 0;
		}

		$("#ids_autores option:selected").each(function(){
			leitura.ids_autores.push($(this).val());
		});

		$("#ids_editoras option:selected").each(function(){
			leitura.ids_editoras.push($(this).val());
		});

		$("#ids_assuntos option:selected").each(function(){
			leitura.ids_assuntos.push($(this).val());
		});

		$.ajax({
			url: 'http://' + location.host + '/leituras/atualizar',
			type: "POST",
			data: leitura,
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");
				}else if (data.status == "s"){
					$("#info").addClass("alert-success");
				}
				$("#info").show();
				window.scrollTo(0, 0);
			},
			complete: function(data){
				$("#salvar").removeAttr("disabled");
			}
		});
	});

	$('#ids_autores').select2();
	$('#ids_editoras').select2();
	$('#ids_assuntos').select2();
});
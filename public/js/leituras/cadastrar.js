$(document).ready(function(){
	$("#data").datepicker(date_picker_config),
	$("#dwd_dados_skoob").on('click', function(e){
		var id_skoob = $("#id_skoob").val();
		var url_json_nao_lido = 'http://' + location.host + '/leituras/get_infos_skoob/' + id_skoob;

		$.ajax({
			url: url_json_nao_lido,
			type: "GET",
			dataType: 'json',
			success: function(data) {
				$("#titulo").val(data.response.titulo);
				$("#subtitulo").val(data.response.subtitulo);
				$("#ano").val(data.response.ano);
				$("#num_pags").val(data.response.paginas);
				$("#sinopse").val(data.response.sinopse);
				$("#sinopse").val(data.response.sinopse);
				$("#url_capa").val(data.response.capa_grande);
			}
		});
	});

	$("#cadastrar").on('click', function(e){
		$("#cadastrar").prop("disabled",true);
		leitura = {
			id_situacao : $("#id_situacao").val(),
			id_skoob : $("#id_skoob").val(),
			titulo : $("#titulo").val(),
			subtitulo : $("#subtitulo").val(),
			isbn : $("#isbn").val(),
			ids_autores : [],
			ids_editoras : [],
			ids_assuntos : [],
			ano : $("#ano").val(),
			num_pags : $("#num_pags").val(),
			nota : $("#nota").val(),
			id_tipo_midia : $("#id_tipo_midia").val(),
			data : $("#data").val(),
			digital : '',
			favorito : '',
			sinopse : $("#sinopse").val(),
			resenha : $("#resenha").val(),
			marcacoes : $("#marcacoes").val(),
			url_capa : $("#url_capa").val()
		}

		if($("#favorito").is(':checked')){
			leitura.favorito = $("#favorito").val();
		}else{
			leitura.favorito = 0;
		}

		if($("#digital").is(':checked')){
			leitura.digital = $("#digital").val();
		}else{
			leitura.digital = 0;
		}

		$("#ids_autores option:selected").each(function(){
			leitura.ids_autores.push($(this).val());
		});

		$("#ids_editoras option:selected").each(function(){
			leitura.ids_editoras.push($(this).val());
		});

		$("#ids_assuntos option:selected").each(function(){
			leitura.ids_assuntos.push($(this).val());
		});

		$.ajax({
			url: 'http://' + location.host + '/leituras/salvar',
			type: "POST",
			data: leitura,
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");
				}else if (data.status == "s"){
					LimpaCampos();
					$("#info").addClass("alert-success");
				}
				$("#info").show();
				window.scrollTo(0, 0);
			},
			complete: function(data){
				$("#cadastrar").removeAttr("disabled");
			}
		});
	});

	$('#add_autor').on('click', function(e){
		$('#md_add_autor').modal('toggle');
	});

	$('#add_editora').on('click', function(e){
		$('#md_add_editora').modal('toggle');
	});

	$('#add_assunto').on('click', function(e){
		$('#md_add_assunto').modal('toggle');
	});

	$('#bt_add_editora').on('click', function(e){
		$("#bt_add_editora").prop("disabled",true);
		$.ajax({
			url: 'http://' + location.host + '/editoras/salvar',
			type: "POST",
			data: {'editora' : $('#editora').val()},
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");
				}else if (data.status == "s"){
					$("#info").addClass("alert-success");
					$('#ids_editoras option:first').after($('<option />', { 'value': data.id, 'text': $('#editora').val()}));
				}
				$("#info").show();
			},
			complete: function(data){
				$("#bt_add_editora").removeAttr("disabled");
				$('#editora').val('');
				$('#md_add_editora').modal('toggle');
				$("select option[value='add_editora']").prop("selected", false);
			}
		});
	});

	$('#bt_add_assunto').on('click', function(e){
		$("#bt_add_assunto").prop("disabled",true);
		$.ajax({
			url: 'http://' + location.host + '/assuntos/salvar',
			type: "POST",
			data: {'assunto' : $('#assunto').val()},
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");
				}else if (data.status == "s"){
					$("#info").addClass("alert-success");
					$('#ids_assuntos option:first').after($('<option />', { 'value': data.id, 'text': $('#assunto').val()}));
				}
				$("#info").show();
			},
			complete: function(data){
				$("#bt_add_assunto").removeAttr("disabled");
				$('#assunto').val('');
				$('#md_add_assunto').modal('toggle');
				$("select option[value='add_assunto']").prop("selected", false);
			}
		});
	});

	$('#bt_add_autor').on('click', function(e){
		$("#bt_add_autor").prop("disabled",true);
		$.ajax({
			url: 'http://' + location.host + '/autores/salvar',
			type: "POST",
			data: {'autor' : $('#autor').val()},
			dataType: "json",
			success: function(data) {
				$("#info").removeClass("alert-danger");
				$("#info").removeClass("alert-success");
				
				$("#info").text(data.msg);
				if (data.status == "e"){
					$("#info").addClass("alert-danger");
				}else if (data.status == "s"){
					$("#info").addClass("alert-success");
					$('#ids_autores option:first').after($('<option />', { 'value': data.id, 'text': $('#autor').val()}));
				}
				$("#info").show();
			},
			complete: function(data){
				$("#bt_add_autor").removeAttr("disabled");
				$('#autor').val('');
				$('#md_add_autor').modal('toggle');
				$("select option[value='add_autor']").prop("selected", false);
			}
		});
	});

	$('#ids_autores').select2();
	$('#ids_editoras').select2();
	$('#ids_assuntos').select2();
});

function LimpaCampos()
{
	$('#dwd_dados_skoob').val('');
	$('#dwd_dados_skoob').val('');
	$('#titulo').val('');
	$('#subtitulo').val('');
	$('#isbn').val('');
	$('#ids_autores').val('').trigger('change');
	$('#ids_editoras').val('').trigger('change');
	$('#ids_assuntos').val('').trigger('change');
	$('#ano').val('');
	$('#num_pags').val('');
	$('#id_tipo_midia').prop("selectedIndex", 0);
	$('#id_situacao').prop("selectedIndex", 0);
	$('#favorito').removeProp('checked');
	$('#digital').removeProp('checked');
	$('#sinopse').val('');
	$('#resenha').val('');
	$('#marcacoes').text('');
	$('#id_skoob').val('');
}
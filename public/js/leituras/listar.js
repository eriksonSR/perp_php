$(document).ready(function(){
	$(".data").datepicker(date_picker_config);

	$('#bt_filtros').on('click', function(){
		$("#div_filtros").toggle();
	});

		$('#bt_filtrar').on('click', function(e){
		filtros = {
			'titulo' : $('#titulo').val(),
			'subtitulo' : $('#subtitulo').val(),
			'ids_autores' : [],
			'ids_editoras' : [],
			'ids_assuntos' : [],
			'id_tipo_midia' : $('#id_tipo_midia').val(),
			'id_situacao' : $('#id_situacao').val(),
			'favorito' : 0,
			'digital' : 0,
			'criterio_ini_dt_finalizacao' : $('#criterio_ini_dt_finalizacao :selected').val(),
			'data_ini' : $('#data_ini').val(),
			'criterio_final_dt_finalizacao' : $('#criterio_final_dt_finalizacao :selected').val(),
			'data_final' : $('#data_final').val()
		}

		$.each($("#ids_autores :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_autores.push($(this).val());
			}
		});

		$.each($("#ids_editoras :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_editoras.push($(this).val());
			}
		});

		$.each($("#ids_assuntos :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_assuntos.push($(this).val());
			}
		});

		if($("#digital").is(':checked')){
			filtros.digital = $("#digital").val();
		}

		if($("#favorito").is(':checked')){
			filtros.favorito = $("#favorito").val();
		}

		$.ajax({
			url: 'http://' + location.host + '/leituras/get_leituras_json',
			type: "POST",
			data: {"filtros":filtros},
			dataType: "json",
			success: function(data) {
				$('#tb_leituras tbody').html('');
				$(data).each(function(i, v){
					$('#tb_leituras tbody').append('<tr><td>'
						+ v.titulo
						+ '</td><td>'
						+ v.autores
						+ '</td><td>'
						+ v.editoras
						+ '</td><td>'
						+ v.tipo
						+ '</td><td>'
						+ v.situacao
						+ '</td><td>'
						+ v.formato
						+ '</td><td>'
						+ '<a href="editar/' + v.id + '" class="btn btn-success btn-sm text-center" target="_blank"><i class="fa fa-edit"></i></a>'
						+ '</td><td>'
						+ '<a href="ficha/' + v.id + '" class="btn btn-success btn-sm text-center" target="_blank"><i class="fa fa-search"></i></a>'
						+ '</td></tr>'
					);
				});
			}
		});

    	return false;
	});

	$('#bt_resetar').on('click', function(e){
		$('#ids_autores').prop("selectedIndex", 0);
		$('#ids_editoras').prop("selectedIndex", 0);
		$('#ids_assuntos').prop("selectedIndex", 0);
		$('#id_tipo_midia').prop("selectedIndex", 0);
		$('#id_situacao').prop("selectedIndex", 0);
		$('#criterio_ini_dt_finalizacao').prop("selectedIndex", 0);
		$('#criterio_final_dt_finalizacao').prop("selectedIndex", 0);
		$('#titulo').val('');
		$('#subtitulo').val('');
		$('#data_ini').val('');
		$('#data_final').val('');
		$('#digital').prop('checked', false);
		$('#favorito').prop('checked', false);
		
		$.ajax({
			url: 'http://' + location.host + '/leituras/get_leituras_json',
			type: "POST",
			data: {"filtros":''},
			dataType: "json",
			success: function(data) {
				$('#tb_leituras tbody').html('');
				$(data).each(function(i, v){
					$('#tb_leituras tbody').append('<tr><td>' 
						+ v.titulo
						+ '</td><td>'
						+ v.autores
						+ '</td><td>'
						+ v.editoras
						+ '</td><td>'
						+ v.tipo
						+ '</td><td>'
						+ v.situacao
						+ '</td><td>'
						+ v.formato
						+ '</td><td>'
						+ '<a href="editar/' + v.id + '" class="btn btn-success btn-sm text-center" target="_blank"><i class="fa fa-edit"></i></a>'
						+ '</td><td>'
						+ '<a href="ficha/' + v.id + '" class="btn btn-success btn-sm text-center" target="_blank"><i class="fa fa-search"></i></a>'
						+ '</td></tr>');
				});
			}
		});

    	return false;
	});

	$('#bt_excel').on('click', function(e){
		filtros = {
			'titulo' : $('#titulo').val(),
			'subtitulo' : $('#subtitulo').val(),
			'ids_autores' : [],
			'ids_editoras' : [],
			'ids_assuntos' : [],
			'id_tipo_midia' : $('#id_tipo_midia').val(),
			'id_situacao' : $('#id_situacao').val(),
			'favorito' : 0,
			'digital' : 0,
			'criterio_ini_dt_finalizacao' : $('#criterio_ini_dt_finalizacao :selected').val(),
			'data_ini' : $('#data_ini').val(),
			'criterio_final_dt_finalizacao' : $('#criterio_final_dt_finalizacao :selected').val(),
			'data_final' : $('#data_final').val()
		}

		$.each($("#ids_autores :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_autores.push($(this).val());
			}
		});

		$.each($("#ids_editoras :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_editoras.push($(this).val());
			}
		});

		$.each($("#ids_assuntos :selected"), function(){
			if($(this).val() > 0){
				filtros.ids_assuntos.push($(this).val());
			}
		});

		if($("#digital").is(':checked')){
			filtros.digital = $("#digital").val();
		}

		if($("#favorito").is(':checked')){
			filtros.favorito = $("#favorito").val();
		}

		var url = 'http://' + location.host + '/leituras/gera_excel?' + decodeURIComponent($.param(filtros));
		window.open(url, '_blank');
	});

	$('#ids_autores').select2();
	$('#ids_editoras').select2();
	$('#ids_assuntos').select2();
});
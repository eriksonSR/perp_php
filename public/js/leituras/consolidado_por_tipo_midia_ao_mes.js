$(document).ready(function(){
	$('#bt_filtrar').on('click', function(e){
		var ano = $('#ano').val();
		var tipos = []

		$("#tipos option:selected").each(function(){
			tipos.push($(this).val());
		});

		$.ajax({
			url: 'http://' + location.host + '/relatorios/leituras/consolidado_por_tipo_midia_ao_mes_json',
			type: "POST",
			data: {"ano":ano, "tipos":tipos},
			dataType: "json",
			success: function(data) {
				$('#tb_relatorio tbody').html('')
				$(data).each(function(i, v){
					$('#tb_relatorio tbody').append('<tr><td>' 
						+ v.tipo_midia 
						+ '</td><td>' 
						+ v.mes 
						+ '</td><td>' 
						+ v.ano 
						+ '</td><td>' 
						+ v.total 
						+ '</td><td class="text-center">'
						+ '<span class="fas fa-search" onclick="visualizar_leituras(\'' + v.ano +'\',\'' + v.mes + '\',\'' + v.tipo_midia + '\')"></span></td></tr>');
				});
			}
		});

    	return false;
	});

	$('#bt_filtros').on('click', function(){
		$("#div_filtros").toggle();
	})

	$('#resetar_filtros').on('click', function(e){
		$("#tipos").val('');
		$('#ano').val('');
		$.ajax({
			url: 'http://' + location.host + '/relatorios/leituras/consolidado_por_tipo_midia_ao_mes_json',
			type: "POST",
			data: {"ano":"", "tipos":""},
			dataType: "json",
			success: function(data) {
				$('#tb_relatorio tbody').html('')
				$(data).each(function(i, v){
					$('#tb_relatorio tbody').append('<tr><td>' 
						+ v.tipo_midia 
						+ '</td><td>' 
						+ v.mes 
						+ '</td><td>' 
						+ v.ano 
						+ '</td><td>' 
						+ v.total 
						+ '</td><td class="text-center">'
						+ '<span class="fas fa-search" onclick="visualizar_leituras(\'' + v.ano +'\',\'' + v.mes + '\',\'' + v.tipo_midia + '\')"></span></td></tr>');
				});
			}
		});

    	return false;
	});

	$('#bt_gera_grafico').on('click', function(){
		var ano = $('#ano').val();
		if(ano.length == 0){
			alert('Informe um ano');
			return false;
		}

		if(parseInt(ano) < 2000){
			alert('Informe um ano válido');
			return false;
		}

		var options = {
			type: 'bar',
			data: {
				labels: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
				datasets:[]
			},
			options: {
				responsive: true,
				legend: {
					position: 'top',
				},
				title: {
					display: true,
					text: 'Games, livros, hqs e mangás finalizados por mês'
				},
				scales: {
	                yAxes: [{
	                    ticks: {
	                        beginAtZero: true,
	                        stepSize: 1
	                    }
	                }]
            	}
			}
		}
		
		var dataset_games = {
			label: 'Games',
			backgroundColor: 'red',
			data: [0,0,0,0,0,0,0,0,0,0,0,0]
		};
		
		var dataset_livros = {
			label: 'Livros',
			backgroundColor: 'blue',
			data: [0,0,0,0,0,0,0,0,0,0,0,0]
		};
		
		var dataset_hqs = {
			label: 'Hqs',
			backgroundColor: 'green',
			data: [0,0,0,0,0,0,0,0,0,0,0,0]
		};
		
		var dataset_mangas = {
			label: 'Mangás',
			backgroundColor: 'yellow',
			data: [0,0,0,0,0,0,0,0,0,0,0,0]
		};

		$.ajax({
			url: 'http://' + location.host + '/relatorios/games_e_leituras_por_mes_json',
			type: "POST",
			data: {"ano":""},
			dataType: "json",
			success: function(data) {
				setaValorMes(dataset_games, data.games)
				options.data.datasets.push(dataset_games)
				setaValorMes(dataset_livros, data.livros)
				options.data.datasets.push(dataset_livros)
				setaValorMes(dataset_hqs, data.hqs)
				options.data.datasets.push(dataset_hqs)
				setaValorMes(dataset_mangas, data.mangas)
				options.data.datasets.push(dataset_mangas)

				var ctx = document.getElementById('grafico').getContext('2d');
				new Chart(ctx, options);
			}
		});

		$("#div_grafico").toggle();
	});


});

function setaValorMes(obj, dados) {
	$(dados).each(function(i, v){
		switch(v.mes){
			case 'Jan':
			obj.data[0] = v.total;
			break;
			case 'Fev':
			obj.data[1] = v.total;
			break;
			case 'Mar':
			obj.data[2] = v.total;
			break;
			case 'Abr':
			obj.data[3] = v.total;
			break;
			case 'Mai':
			obj.data[4] = v.total;
			break;
			case 'Jun':
			obj.data[5] = v.total;
			break;
			case 'Jul':
			obj.data[6] = v.total;
			break;
			case 'Ago':
			obj.data[7] = v.total;
			break;
			case 'Set':
			obj.data[8] = v.total;
			break;
			case 'Out':
			obj.data[9] = v.total;
			break;
			case 'Nov':
			obj.data[10] = v.total;
			break;
			case 'Dez':
			obj.data[11] = v.total;
			break;
		}
	});
}

function visualizar_leituras(ano, mes, tipo_midia){
	filtros = {'ano':ano, 'mes':mes, 'tipo_midia_text':tipo_midia, 'id_situacao':2}
	$.ajax({
		url: 'http://' + location.host + '/leituras/get_leituras_json',
		type: "POST",
		data: {"filtros":filtros},
		dataType: "json",
		success: function(data) {
			$('#tb_leituras tbody').html('')
			$(data).each(function(i, v){
				$('#tb_leituras tbody').append('<tr><td>' 
					+ v.titulo
					+ '</td><td>'
					+ v.autores
					+ '</td><td>'
					+ v.editoras
					+ '</td><td class="text-center">'
					+ '<a href="' + window.location.origin + '/leituras/ficha/' + v.id 
					+ '" class="btn btn-success btn-sm text-center" target="_blank">'
					+ '<i class="fa fa-search"></i></a></td></tr>');
			});
			$('#md_leituras_visualizar').modal('toggle');
		}
	});
    return false;
}
$(document).ready(function(){
	$('#bt_filtrar').on('click', function(e){
		var ano = $('#ano').val();
		
		$.ajax({
			url: 'http://' + location.host + '/relatorios/leituras/consolidado_por_ano_json',
			type: "POST",
			data: {"ano":ano},
			dataType: "json",
			success: function(data) {
				$('#tb_relatorio tbody').html('')
				$('#tb_relatorio tbody').append('<tr><td>' 
					+ data[0].ano 
					+ '</td><td>' 
					+ data[0].total 
					+ '</td><td class="text-center">'
					+ '<span class="fas fa-search" onclick="visualizar_leituras(' + data[0].ano + ')"></span></td></tr>');
			}
		});

    	return false;
	});

	$('#bt_filtros').on('click', function(){
		$("#div_filtros").toggle();
	})

	$('#resetar_filtros').on('click', function(e){
		$.ajax({
			url: 'http://' + location.host + '/relatorios/leituras/consolidado_por_ano_json',
			type: "POST",
			data: {"ano":""},
			dataType: "json",
			success: function(data) {
				$('#tb_relatorio tbody').html('')
				$(data).each(function(i, v){
					$('#tb_relatorio tbody').append('<tr><td>' 
						+ v.ano 
						+ '</td><td>' 
						+ v.total 
						+ '</td><td class="text-center">'
						+ '<span class="fas fa-search" onclick="visualizar_leituras(' + v.ano + ')"></span></td></tr>')
				});
			}
		});

    	return false;
	});
});

function visualizar_leituras(ano, total){
	filtros = {'ano':ano, 'id_situacao':2}
		
	$.ajax({
		url: 'http://' + location.host + '/leituras/get_leituras_json',
		type: "POST",
		data: {"filtros":filtros},
		dataType: "json",
		success: function(data) {
			$('#tb_leituras tbody').html('')
			$(data).each(function(i, v){
				$('#tb_leituras tbody').append('<tr><td>' 
					+ v.titulo 
					+ '</td><td>' 
					+ v.autores 
					+ '</td><td>' 
					+ v.editoras 
					+ '</td><td class="text-center">'
					+ '<a href="' + window.location.origin + '/leituras/ficha/' + v.id 
					+ '" class="btn btn-success btn-sm text-center" target="_blank">'
					+ '<i class="fa fa-search"></i></a></td></tr>');
			});
			$('#md_leituras_visualizar').modal('toggle');
		}
	});
    return false;
}
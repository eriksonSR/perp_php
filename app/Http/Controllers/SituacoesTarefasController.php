<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SituacoesTarefas;

class SituacoesTarefasController extends Controller
{
	public function GetSituacoesJson()
	{
		echo json_encode(SituacoesTarefas::select('id', 'situacao')->orderBy('situacao')->get());
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RelatoriosGames;
use App\Consoles;
use App\Studios;
use App\Generos;
use App\Plataformas;
use DB;

class RelatoriosGamesController extends Controller
{
    public function DlcVsJogoBase()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Games', 'DLC vs Jogo Base'],
    		'view' => 'relatorios.games.dlc_vs_jogo_base',
    		'dados' => RelatoriosGames::DlcVsJogoBase(),
            'scripts' => [['tipo' => 'local', 'caminho' => 'games/dlc_vs_jogo_base']]
    	]);
    }

    public function ConsolidadoPorAno()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Games', 'Consolidado por ano'],
    		'view' => 'relatorios.games.consolidado_por_ano',
    		'dados' => RelatoriosGames::ConsolidadoPorAno(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'games/consolidado_por_ano']]
    	]);
    }

    public function ConsolidadoPorAnoJson(Request $request)
    {
    	$dados = RelatoriosGames::ConsolidadoPorAno($request->input('ano'));
    	echo json_encode($dados);
    }

    public function ConsolidadoPorConsoleAoAno()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Games', 'Consolidado por console ano'],
    		'view' => 'relatorios.games.consolidado_por_console_ao_ano',
    		'dados' => RelatoriosGames::ConsolidadoPorConsoleAoAno(),
    		'consoles' => Consoles::orderBy('console')->get(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'games/consolidado_por_console_ao_ano']]
    	]);
    }

    public function ConsolidadoPorConsoleAoAnoJson(Request $request)
    {
    	$dados = RelatoriosGames::ConsolidadoPorConsoleAoAno($request->input('ano'), $request->input('consoles'));
    	echo json_encode($dados);
    }

    public function ConsolidadoPorDesenvolvedora()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Games', 'Consolidado por desenvolvedora'],
    		'view' => 'relatorios.games.consolidado_por_desenvolvedora',
    		'dados' => RelatoriosGames::ConsolidadoPorDesenvolvedora(),
    		'desenvolvedoras' => Studios::orderBy('studio')->get(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'games/consolidado_por_desenvolvedora']]
    	]);
    }

    public function ConsolidadoPorDesenvolvedoraJson(Request $request)
    {
    	$dados = RelatoriosGames::ConsolidadoPorDesenvolvedora($request->input('desenvolvedoras'));
    	echo json_encode($dados);
    }

    public function ConsolidadoPorGeneroAoAno()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Games', 'Consolidado por gênero ao ano'],
    		'view' => 'relatorios.games.consolidado_por_genero_ao_ano',
    		'dados' => RelatoriosGames::ConsolidadoPorGeneroAoAno(),
    		'generos' => Generos::orderBy('genero')->get(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'games/consolidado_por_genero_ao_ano']]
    	]);
    }

    public function ConsolidadoPorGeneroAoAnoJson(Request $request)
    {
    	$dados = RelatoriosGames::ConsolidadoPorGeneroAoAno($request->input('ano'), $request->input('generos'));
    	echo json_encode($dados);
    }

    public function ConsolidadoPorMes()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Games', 'Consolidado por mês'],
    		'view' => 'relatorios.games.consolidado_por_mes',
    		'dados' => RelatoriosGames::ConsolidadoPorMes(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'games/consolidado_por_mes']]
    	]);
    }

    public function ConsolidadoPorMesJson(Request $request)
    {
    	$dados = RelatoriosGames::ConsolidadoPorMes($request->input('ano'));
    	echo json_encode($dados);
    }

    public function ConsolidadoPorPlataformaAoAno()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Games', 'Consolidado por plataforma ao ano'],
    		'view' => 'relatorios.games.consolidado_por_plataforma_ao_ano',
    		'dados' => RelatoriosGames::ConsolidadoPorPlataformaAoAno(),
    		'plataformas' => Plataformas::orderBy('plataforma')->get(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'games/consolidado_por_plataforma_ao_ano']]
    	]);
    }

    public function ConsolidadoPorPlataformaAoAnoJson(Request $request)
    {
    	$dados = RelatoriosGames::ConsolidadoPorPlataformaAoAno($request->input('ano'), $request->input('plataformas'));
    	echo json_encode($dados);
    }

    public function ConsolidadoPorPublicadoraAoAno()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Games', 'Consolidado por publicadora ao ano'],
    		'view' => 'relatorios.games.consolidado_por_publicadora_ao_ano',
    		'dados' => RelatoriosGames::ConsolidadoPorPublicadoraAoAno(),
    		'publicadoras' => Studios::orderBy('studio')->get(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'games/consolidado_por_publicadora_ao_ano']]
    	]);
    }

    public function ConsolidadoPorPublicadoraAoAnoJson(Request $request)
    {
    	$dados = RelatoriosGames::ConsolidadoPorPublicadoraAoAno($request->input('ano'), $request->input('publicadoras'));
    	echo json_encode($dados);
    }

    public function ConsolidadoPortatilVsMesaAoAno()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Games', 'Consolidado por portátil vs mesa'],
    		'view' => 'relatorios.games.consolidado_por_portatil_vs_mesa_ao_ano',
    		'dados' => RelatoriosGames::ConsolidadoPortatilVsMesaAoAno(),
    		'publicadoras' => Studios::orderBy('studio')->get(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'games/consolidado_por_portatil_vs_mesa_ao_ano']]
    	]);
    }

    public function ConsolidadoPortatilVsMesaAoAnoJson(Request $request)
    {
    	$dados = RelatoriosGames::ConsolidadoPortatilVsMesaAoAno($request->input('ano'));
    	echo json_encode($dados);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Generos;

class GenerosController extends Controller
{

	public function ListarJson(Request $request)
	{
		$cols = ['id','genero'];
		if ($request->has('colunas')) {
			$cols = explode(',', $request->input('colunas'));
		}
		$consoles = Generos::select($cols)->orderBy('genero')->get();
		echo json_encode($consoles);
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plataformas;

class PlataformasController extends Controller
{

	public function ListarJson(Request $request)
	{
		$cols = ['id','plataforma'];
		if ($request->has('colunas')) {
			$cols = explode(',', $request->input('colunas'));
		}
		$plataformas = Plataformas::select($cols)->orderBy('plataforma')->get();
		echo json_encode($plataformas);
	}
}

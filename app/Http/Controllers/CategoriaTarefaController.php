<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoriaTarefa;

class CategoriaTarefaController extends Controller
{
	public function Cadastrar()
	{
    	return view('layout.app', [
    		'caminho' => ['Listas de tarefas', 'Categorias', 'Cadastrar'],
    		'view' => 'listas_tarefas.categorias.cadastrar',
    		'dados' => [],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'listas_tarefas/categorias/cadastrar']
    		]
    	]);
	}

	public function Salvar(Request $request)
	{
		$categoria = new CategoriaTarefa();
		$categoria->categoria = $request->input('categoria');
		if($categoria->save()){
			return json_encode(['status' => 's', 'msg' => 'Categoria salva com sucesso', 'id' => $categoria->id]);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
	}

	public function Listar()
	{
    	return view('layout.app', [
    		'caminho' => ['Listas de tarefas', 'Categorias', 'Listar'],
    		'view' => 'listas_tarefas.categorias.listar',
    		'dados' => [
    			'categorias' => CategoriaTarefa::orderBy('categoria')->get(),
    		],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'listas_tarefas/categorias/listar']
    		]
    	]);
	}

	public function GetCategoriaJson($id_categoria)
	{
		$categoria = CategoriaTarefa::find($id_categoria);
		echo json_encode($categoria);
	}

    public function Atualizar(Request $request)
    {
    	$categoria = CategoriaTarefa::find($request->input('id_categoria'));
		$categoria->categoria = $request->input('categoria');
		if($categoria->save()){
			return json_encode(['status' => 'Sucesso', 'msg' => 'Categoria atualizado com sucesso!']);
		}else{
			return json_encode(['status' => 'Erro', 'msg' => 'Erro ao atualizar categoria!']);
		}
    }

	public function GetCategoriasJson()
	{
		echo json_encode(CategoriaTarefa::select('id', 'categoria')->orderBy('categoria')->get());
	}
}

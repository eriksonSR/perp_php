<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RelatoriosLeituras;
use App\RelatoriosGames;
use App\Lancamentos;
use App\ListasTarefas;
use App\SituacoesTarefas;
use App\RelatoriosExercicios;

class HomeController extends Controller
{
	private $meses_nome = ['01'=>'Jan', '02'=>'Fev', '03'=>'Mar', '04'=>'Abr', '05'=>'Mai', '06'=>'Jun', '07'=>'Jul', '08'=>'Ago', '09'=>'Set', '10'=>'Out', '11'=>'Nov', '12'=>'Dez'];

	public function Index()
	{
		$ano_corrente = date("Y");

		$games_fin_p_mes = RelatoriosGames::ConsolidadoPorMes($ano_corrente);
    	$livros_fin_p_mes = RelatoriosLeituras::ConsolidadoPorTipoMidiaAoMes($ano_corrente, [1]);
    	$hqs_fin_p_mes = RelatoriosLeituras::ConsolidadoPorTipoMidiaAoMes($ano_corrente, [2]);
    	$mangas_fin_p_mes = RelatoriosLeituras::ConsolidadoPorTipoMidiaAoMes($ano_corrente, [3]);
    	foreach ($games_fin_p_mes as $k => $v) {
			$games_fin_p_mes[$k]->mes = $this->meses_nome[$v->mes];
		}

		foreach ($livros_fin_p_mes as $k => $v) {
			$livros_fin_p_mes[$k]->mes = $this->meses_nome[$v->mes];
		}

		foreach ($hqs_fin_p_mes as $k => $v) {
			$hqs_fin_p_mes[$k]->mes = $this->meses_nome[$v->mes];
		}

		foreach ($mangas_fin_p_mes as $k => $v) {
			$mangas_fin_p_mes[$k]->mes = $this->meses_nome[$v->mes];
		}

		return view('layout.app', [
    		'caminho' => ['Home'],
    		'view' => 'home',
    		'dados' => [
    			'ultimos_livros' => RelatoriosLeituras::UltimosLivrosLidos(),
    			'ultimos_hqs' => RelatoriosLeituras::UltimosHsLidos(),
    			'ultimos_mangas' => RelatoriosLeituras::UltimosMangasLidos(),
    			'qtd_midias_fisicas' => RelatoriosLeituras::QuantidadeMidiasFisicas(),
    			'assuntos_mais_lidos_livros' => RelatoriosLeituras::LivrosPorAssunto($ano_corrente),
    			'assuntos_mais_lidos_hqs' => RelatoriosLeituras::HqsPorAssunto($ano_corrente),
    			'principais_gastos' => Lancamentos::PrincipaisGastos($ano_corrente),
    			'ultimos_games' => RelatoriosGames::UltimosGamesFinalizados($ano_corrente),
    			'consoles_mais_jogados' => RelatoriosGames::ConsolesMaisJogadosNoAno($ano_corrente),
    			'games_fin_p_mes' => $games_fin_p_mes,
    			'livros_fin_p_mes' => $livros_fin_p_mes,
    			'hqs_fin_p_mes' => $hqs_fin_p_mes,
    			'mangas_fin_p_mes' => $mangas_fin_p_mes,
    			'midias_finalizadas_x_nao_finalizadas' => [
    				'qtd_livros_finalizados' => RelatoriosLeituras::QtdLivrosFinalizados(),
    				'qtd_livros_nao_iniciados' => RelatoriosLeituras::QtdLivrosNaoIniciados(),
    				'qtd_hqs_finalizados' => RelatoriosLeituras::QtdHqsFinalizados(),
    				'qtd_hqs_nao_iniciados' => RelatoriosLeituras::QtdHqsNaoIniciados(),
    				'qtd_mangas_finalizados' => RelatoriosLeituras::QtdMangasFinalizados(),
    				'qtd_mangas_nao_iniciados' => RelatoriosLeituras::QtdMangasNaoIniciados(),
    				'qtd_games_finalizados' => RelatoriosGames::QtdGamesFinalizados(),
    				'qtd_games_nao_iniciados' => RelatoriosGames::QtdGamesNaoIniciados()
    			],
                'listas_tarefas' => ListasTarefas::select('id', 'lista', 'data', 'dia')->orderBy('data', 'desc')->limit(7)->get(),
                'situacoes_tarefas' => SituacoesTarefas::orderBy('situacao')->get(),
                'dist_p_periodo_e_exerc' => RelatoriosExercicios::DistanciasPorPeriodo(),
                'dist_tot_p_exercio' => RelatoriosExercicios::DistanciaTotalPorExercicioAoAno()
    		],
    		'scripts' => [
                ['tipo' => 'url', 'caminho' => 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js'],
                ['tipo' => 'local', 'caminho' => 'home']
            ]
    	]);
	}
}

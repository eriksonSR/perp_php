<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Games;
use App\Leituras;
use App\Lancamentos;

class BuscaController extends Controller
{
	public function BuscaGeral($termo)
	{
		return view('layout.app', [
    		'caminho' => ['Busca', 'Busca geral'],
    		'view' => 'buscas.busca_geral',
    		'dados' => [
    			'termo' => $termo,
    			'games' => Games::BuscaGeral($termo),
    			'leituras' => Leituras::BuscaGeral($termo),
    			'lancamentos' => Lancamentos::BuscaGeral($termo)
    		]
    	]);
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ListasTarefas;
use App\DiasSemana;
use App\CategoriaTarefa;
use App\Tarefas;
use App\SituacoesTarefas;
use PgUtils;
use Illuminate\Support\Facades\DB;

class ListasTarefasController extends Controller
{
	public function Cadastrar()
	{
    	return view('layout.app', [
    		'caminho' => ['Listas de tarefas', 'Listas', 'Cadastrar'],
    		'view' => 'listas_tarefas.listas.cadastrar',
    		'dados' => [
    			'dias_semana' => DiasSemana::get(),
    			'ult_listas' => ListasTarefas::orderBy('data', 'desc')->take(10)->get(),
    			'categorias' => CategoriaTarefa::orderBy('categoria')->get(),
                'situacoes_tarefas' => SituacoesTarefas::orderBy('situacao')->get()
    		],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'listas_tarefas/listas/cadastrar']
    		]
    	]);
	}

	public function Salvar(Request $request)
	{
		$lista = new ListasTarefas();
		$data = explode('/', $request->input('data'));
		$data = $data[2] . '-' . $data[1] . '-' . $data[0];
		$lista->data = $data;
		$lista->dia = $request->input('dia');
		$lista->lista = $request->input('lista');
		$tarefas = $request->input('tarefas');

		if($lista->save()){
			$erro_tarefa = 0;
			$ult_listas = ListasTarefas::orderBy('data', 'desc')->take(10)->get();
			foreach ($ult_listas as $l) {
				$l->data = PgUtils::DataPgToBr($l->data, '/');
			}

			foreach ($tarefas as $t) {
				$tarefa = new Tarefas();
				$tarefa->id_lista = $lista->id;
				$tarefa->tarefa = $t['tarefa'];
				$tarefa->id_categoria = $t['id_categoria'];
				if(!$tarefa->save()){
					$erro_tarefa = 1;
				}
			}

			if($erro_tarefa){
				return json_encode([
					'status' => 'e', 
					'msg' => 'Lista salva, porém ocorreu um erro ao salvar tarefas.',
					'ult_listas' => $ult_listas
				]);
			}else{
				return json_encode([
					'status' => 's', 
					'msg' => 'Lista e tarefas salvas com sucesso!',
					'ult_listas' => $ult_listas
				]);
			}
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar lista']);
		}
	}

	public function Listar()
	{
		return view('layout.app', [
			'caminho' => ['Listas de tarefas', 'Listas', 'Listar'],
    		'view' => 'listas_tarefas.listas.listar',
    		'dados' => [
    			'dias_semana' => DiasSemana::get(),
    			'listas' => ListasTarefas::orderBy('data', 'desc')->get(),
    			'categorias' => CategoriaTarefa::orderBy('categoria')->get(),
                'situacoes_tarefas' => SituacoesTarefas::orderBy('situacao')->get()
    		],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'listas_tarefas/listas/listar']
    		]
    	]);
	}

	public function GetListaJson($id_lista)
	{
		$lista = ListasTarefas::find($id_lista);
		$lista->data = PgUtils::DataPgToBr($lista->data, '/');
		echo json_encode($lista);
	}

    public function Atualizar(Request $request)
    {
    	$lista = ListasTarefas::find($request->input('id_lista'));
		$lista->lista = $request->input('lista');
		$lista->data = PgUtils::DataBrToPg($request->input('data'), '/');
		$lista->dia = $request->input('dia');
		if($lista->save()){
			return json_encode(['status' => 'Sucesso', 'msg' => 'Lista atualizado com sucesso!']);
		}else{
			return json_encode(['status' => 'Erro', 'msg' => 'Erro ao atualizar lista!']);
		}
    }

    public function GetListaComTarefasJson($id_lista)
    {
    	$dados = DB::table('tb_tarefas')
    	->select([
    		'tb_listas_tarefas.lista',
    		'tb_listas_tarefas.data',
    		'tb_listas_tarefas.dia',
    		'tb_tarefas.id',
    		'tb_tarefas.tarefa',
    		'tb_categorias_tarefas.categoria',
    		'tb_tarefas.id_situacao'
    	])
        ->join('tb_listas_tarefas', 'tb_tarefas.id_lista', '=', 'tb_listas_tarefas.id')
        ->join('tb_categorias_tarefas', 'tb_tarefas.id_categoria', '=', 'tb_categorias_tarefas.id')
        ->where('tb_listas_tarefas.id', $id_lista)
        ->get();
        foreach ($dados as $d) {
        	$d->data = PgUtils::DataPgToBr($d->data, '/');
        }
    	return json_encode($dados);
    }
}

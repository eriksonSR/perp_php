<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Situacoes;
use App\Autores;
use App\Editoras;
use App\Assuntos;
use App\TiposMidia;
use App\Leituras;
use GuzzleHttp\Client;
use PgUtils;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class LeiturasController extends Controller
{
	public function Cadastrar()
	{
		$situacoes = new Situacoes();
		$autores = new Autores();
		$editoras = new Editoras();
		$assuntos = new Assuntos();
		$tipos_midia = new TiposMidia();
		return view('layout.app', [
    		'caminho' => ['Leituras', 'Cadastrar'],
    		'view' => 'leituras.cadastrar',
    		'dados' => [
    			'situacoes' => $situacoes::orderBy('situacao')->get(),
    			'autores' => $autores::orderBy('autor')->get(),
    			'editoras' => $editoras::orderBy('editora')->get(),
    			'assuntos' => $assuntos::orderBy('assunto')->get(),
    			'tipos_midia' => $tipos_midia::orderBy('tipo_midia')->get()
    		],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'leituras/cadastrar']
    		]
    	]);
	}

	public function Salvar(Request $request)
	{
		$leituras = new Leituras();
		$leituras->id_situacao = $request->input('id_situacao');
		$leituras->id_skoob = $request->input('id_skoob');
		$leituras->titulo = $request->input('titulo');
		$leituras->isbn = $request->input('isbn');
		$leituras->ids_autores = '{' . implode(',', $request->input('ids_autores')) . '}';
		$leituras->ids_editoras = '{' . implode(',', $request->input('ids_editoras')) . '}';
		$leituras->ids_assuntos = '{' . implode(',', $request->input('ids_assuntos')) . '}';
		$leituras->ano = $request->input('ano');
		$leituras->paginas = $request->input('num_pags');
		$leituras->nota = $request->input('nota');
		$leituras->id_tipo_midia = $request->input('id_tipo_midia');
		if(!empty($request->input('data'))){
			$leituras->dt_finalizacao = PgUtils::DataBrToPg($request->input('data'), '/');
		}
		$leituras->digital = $request->input('digital');
		$leituras->favorito = $request->input('favorito');
		$leituras->sinopse = $request->input('sinopse');
		$leituras->resenha = $request->input('resenha');
		$leituras->marcacoes = $request->input('marcacoes');
		$leituras->url_capa = $request->input('url_capa');
		if($leituras->save()){
			$retorno = $leituras->BaixarCapa($leituras->id_skoob, $leituras->url_capa);
			return json_encode($retorno);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
	}

	public function Listar()
	{
		$leituras = Leituras::select(['id','titulo', 'ids_autores', 'ids_editoras', 'id_tipo_midia', 'id_situacao', 'digital'])->orderBy('titulo')->get();
		
		foreach($leituras as $l) {
			$autores = [];
			$editoras = [];
			$str_ids_autores = str_replace('{', '', $l->ids_autores);
			$str_ids_autores = str_replace('}', '', $str_ids_autores);
			$ar_ids_autores = explode(',', $str_ids_autores);

			foreach ($ar_ids_autores as $a) {
				$autor = Autores::find($a);
				array_push($autores, $autor->autor);
			}

			$str_ids_editoras = str_replace('{', '', $l->ids_editoras);
			$str_ids_editoras = str_replace('}', '', $str_ids_editoras);
			$ar_ids_editoras = explode(',', $str_ids_editoras);
			foreach ($ar_ids_editoras as $e) {
				$editora = Editoras::find($e);
				array_push($editoras, $editora->editora);
			}
			$l->autores = implode(', ', $autores);
			$l->editoras = implode(', ', $editoras);
		}
		return view('layout.app', [
    		'caminho' => ['Leituras', 'Listar'],
    		'view' => 'leituras.listar',
    		'dados' => [
    			'leituras' => $leituras,
    			'autores' => Autores::orderBy('autor')->get(),
    			'assuntos' => Assuntos::orderBy('assunto')->get(),
    			'editoras' => Editoras::orderBy('editora')->get(),
    			'situacoes' => Situacoes::orderBy('situacao')->get(),
    			'tipos_midia' => TiposMidia::orderBy('tipo_midia')->get()
    		],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'leituras/listar']
    		]
    	]);
	}

	public function Editar($id)
	{
		$leitura = Leituras::find($id);
		return view('layout.app', [
    		'caminho' => ['Leituras', 'Editar'],
    		'view' => 'leituras.editar',
    		'dados' => [
    			'leitura' => $leitura,
    			'situacoes' => Situacoes::orderBy('situacao')->get(),
    			'autores' => Autores::orderBy('autor')->get(),
    			'editoras' => Editoras::orderBy('editora')->get(),
    			'assuntos' => Assuntos::orderBy('assunto')->get(),
    			'tipos_midia' => TiposMidia::orderBy('tipo_midia')->get()
    		],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'leituras/editar']
    		]
    	]);
	}

	public function Atualizar(Request $request)
	{
		$leituras = Leituras::find($request->input('id'));
		$leituras->id_situacao = $request->input('id_situacao');
		$leituras->id_skoob = $request->input('id_skoob');
		$leituras->titulo = $request->input('titulo');
		$leituras->isbn = $request->input('isbn');
		$leituras->ids_autores = PgUtils::ArrayPhpToArPg($request->input('ids_autores'));
		$leituras->ids_editoras = PgUtils::ArrayPhpToArPg($request->input('ids_editoras'));
		$leituras->ids_assuntos = PgUtils::ArrayPhpToArPg($request->input('ids_assuntos'));
		$leituras->ano = $request->input('ano');
		$leituras->paginas = $request->input('num_pags');
		$leituras->nota = $request->input('nota');
		$leituras->id_tipo_midia = $request->input('id_tipo_midia');
		if(!empty($request->input('data'))){
			$leituras->dt_finalizacao = PgUtils::DataBrToPg($request->input('data'), '/');
		}
		$leituras->digital = $request->input('digital');
		$leituras->favorito = $request->input('favorito');
		$leituras->sinopse = $request->input('sinopse');
		$leituras->resenha = $request->input('resenha');
		$leituras->marcacoes = $request->input('marcacoes');
		$leituras->url_capa = $request->input('url_capa');
		if($leituras->save()){
			return json_encode(['status' => 's', 'msg' => 'Leitura atualizada com sucesso!']);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao atualizar leitura!']);
		}
	}

	public function GetLeiturasJson(Request $request)
	{
		$filtros = $request->input('filtros');
		if(isset($request->input('filtros')['assunto_text'])) {
			$assunto = $request->input('filtros')['assunto_text'];
			$assunto = Assuntos::where('assunto', $assunto)->first();
			$filtros['ids_assuntos'] = [$assunto->id];
		}
		if(isset($request->input('filtros')['autor_text'])) {
			$autor = $request->input('filtros')['autor_text'];
			$autor = Autores::where('autor', $autor)->first();
			$filtros['ids_autores'] = [$autor->id];
		}
		if(isset($request->input('filtros')['editora_text'])) {
			$editora = $request->input('filtros')['editora_text'];
			$editora = Editoras::where('editora', $editora)->first();
			$filtros['ids_editoras'] = [$editora->id];
		}
		if(isset($request->input('filtros')['editora_text'])) {
			$editora = $request->input('filtros')['editora_text'];
			$editora = Editoras::where('editora', $editora)->first();
			$filtros['ids_editoras'] = [$editora->id];
		}
		if(isset($request->input('filtros')['formato_text'])) {
			$filtros['formato_text'] = $request->input('filtros')['formato_text'];
		}
		if(isset($request->input('filtros')['tipo_midia_text'])) {
			$tipo_midia = $request->input('filtros')['tipo_midia_text'];
			$tipo_midia = TiposMidia::where('tipo_midia', $tipo_midia)->first();
			$filtros['id_tipo_midia'] = $tipo_midia->id;
		}

		$leituras = Leituras::Listar($filtros);
		foreach ($leituras as $l) {
			$autores_str = [];
			$autores_ar = PgUtils::ArrayPgToArPhp($l->ids_autores);
			foreach ($autores_ar as $a) {
				array_push($autores_str, Autores::find($a)->autor);
			}
			$l->autores = implode(', ', $autores_str);

			$editoras_str = [];
			$editoras_ar = PgUtils::ArrayPgToArPhp($l->ids_editoras);
			foreach ($editoras_ar as $e) {
				array_push($editoras_str, Editoras::find($e)->editora);
			}
			$l->editoras = implode(', ', $editoras_str);
		}
		echo json_encode($leituras);
	}	

	public function GeraExcel()
	{
		$filtros = [
			'titulo' => Input::get('titulo'),
			'subtitulo' => Input::get('subtitulo'),
			'ids_autores' => Input::get('ids_autores'),
			'ids_editoras' => Input::get('ids_editoras'),
			'ids_assuntos' => Input::get('ids_assuntos'),
			'id_tipo_midia' => Input::get('id_tipo_midia'),
			'id_situacao' => Input::get('id_situacao'),
			'favorito' => Input::get('favorito'),
			'digital' => Input::get('digital'),
			'criterio_ini_dt_finalizacao' => Input::get('criterio_ini_dt_finalizacao'),
			'data_ini' => Input::get('data_ini'),
			'criterio_final_dt_finalizacao' => Input::get('criterio_final_dt_finalizacao'),
			'data_final' => Input::get('data_final')
		];
		$leituras = Leituras::Listar($filtros);

		$nome_arq = 'leituras_' . date('d/m/Y_H:i:s') . '.csv';
		$headers = array(
        	"Content-type" => "text/csv",
        	"Content-Disposition" => "attachment; filename={$nome_arq}",
        	"Pragma" => "no-cache",
        	"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        	"Expires" => "0"
    	);
    	$columns = array('ID', 'TITULO', 'SUBTITULO', 'AUTORES', 'EDITORAS', 'TIPO', 'SITUAÇÃO', 'FORMATO', 'ASSUNTOS', 'DATA FINALIZAÇÃO');

    	$callback = function() use ($leituras, $columns)
    	{
        	$file = fopen('php://output', 'w');
        	fputcsv($file, $columns);

			foreach ($leituras as $l) {
				$autores_str = [];
				$autores_ar = PgUtils::ArrayPgToArPhp($l->ids_autores);
				foreach ($autores_ar as $a) {
					array_push($autores_str, Autores::find($a)->autor);
				}
				$l->autores = implode(', ', $autores_str);

				$editoras_str = [];
				$editoras_ar = PgUtils::ArrayPgToArPhp($l->ids_editoras);
				foreach ($editoras_ar as $e) {
					array_push($editoras_str, Editoras::find($e)->editora);
				}
				$l->editoras = implode(', ', $editoras_str);

				$assuntos_str = [];
				$assuntos_ar = PgUtils::ArrayPgToArPhp($l->ids_assuntos);
				foreach ($assuntos_ar as $a) {
					array_push($assuntos_str, Assuntos::find($a)->assunto);
				}
				$l->assuntos = implode(', ', $assuntos_str);

				fputcsv($file, array($l->id, $l->titulo, $l->subtitulo, $l->autores, $l->editoras, $l->tipo, $l->situacao, $l->formato, $l->assuntos, $l->dt_finalizacao));
			}
        	
        	fclose($file);
    	};
    	
    	return Response::stream($callback, 200, $headers);
	}

	public function ExportarTabela()
	{
		$leituras = Leituras::ExportarTabela();

		$nome_arq = 'tb_leituras_' . date('d/m/Y_H:i:s') . '.csv';
		$headers = array(
        	"Content-type" => "text/csv",
        	"Content-Disposition" => "attachment; filename={$nome_arq}",
        	"Pragma" => "no-cache",
        	"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        	"Expires" => "0"
    	);
    	$columns = ['ID', 'TITULO', 'SUBTITULO', 'AUTOR', 'EDITORA', 'ASSUNTO', 'SITUAÇÃO', 'FORMATO', 'NOTA', 'PÁGINAS', 'ISBN', 'DATA FINALIZAÇÃO'];

    	$callback = function() use ($leituras, $columns)
    	{
        	$file = fopen('php://output', 'w');
        	fputcsv($file, $columns);

			foreach ($leituras as $l) {
				fputcsv($file, array($l->id, $l->titulo, $l->subtitulo, $l->autor, $l->editora, $l->assunto, $l->situacao, $l->formato, $l->nota, $l->paginas, $l->isbn, $l->dt_finalizacao));
			}
        	
        	fclose($file);
    	};
    	return Response::stream($callback, 200, $headers);
	}

	public function Ficha($id)
	{
		$leitura = Leituras::find($id);
		$autores_str = [];
		$autores_ar = PgUtils::ArrayPgToArPhp($leitura->ids_autores);
		foreach ($autores_ar as $a) {
			array_push($autores_str, Autores::find($a)->autor);
		}
		$leitura->autores = implode(', ', $autores_str);

		$editoras_str = [];
		$editoras_ar = PgUtils::ArrayPgToArPhp($leitura->ids_editoras);
		foreach ($editoras_ar as $e) {
			array_push($editoras_str, Editoras::find($e)->editora);
		}
		$leitura->editoras = implode(', ', $editoras_str);

		$assuntos_str = [];
		$assuntos_ar = PgUtils::ArrayPgToArPhp($leitura->ids_assuntos);
		foreach ($assuntos_ar as $a) {
			array_push($assuntos_str, Assuntos::find($a)->assunto);
		}
		$leitura->assuntos = implode(', ', $assuntos_str);

		return view('layout.app', [
    		'caminho' => ['Leituras', 'Ficha'],
    		'view' => 'leituras.ficha',
    		'dados' => [
    			'leitura' => $leitura
    		]
    	]);
	}

	public function GetInfosSkoob($id_skoob)
	{
		$url = 'https://www.skoob.com.br/v1/book/' . $id_skoob;
		$guzz = new Client();
		$resposta = $guzz->request('GET', $url);
		$json = json_decode($resposta->getBody()->getContents());
		echo json_encode($json);
	}

	public function ListarJson(Request $request)
	{
		$cols = [
			'id',
			'titulo', 
			'subtitulo',
			'dt_finalizacao',
			'isbn', 
			'ano', 
			'edicao', 
			'paginas', 
			'nota', 
			'favorito', 
			'digital',
			'id_idioma',
			'id_situacao',
			'id_tipo_midia',
			'ids_assuntos',
			'ids_autores',
			'ids_editoras'
		];

		if ($request->has('colunas')) {
			$cols = explode(',', $request->input('colunas'));
		}

		$leituras = Leituras::select($cols)->orderBy('titulo')->get();
		foreach ($leituras as $l) {
			if (isset($l->ids_assuntos) && !empty($l->ids_assuntos)) {
				$l->ids_assuntos = implode(',', PgUtils::ArrayPgToArPhp($l->ids_assuntos));
			}
			if (isset($l->ids_autores) && !empty($l->ids_autores)) {
				$l->ids_autores = implode(',', PgUtils::ArrayPgToArPhp($l->ids_autores));
			}
			if (isset($l->ids_editoras) && !empty($l->ids_editoras)) {
				$l->ids_editoras = implode(',', PgUtils::ArrayPgToArPhp($l->ids_editoras));
			}
		}
		echo json_encode($leituras);
	}
}
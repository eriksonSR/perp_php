<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrigemGames;

class OrigemGamesController extends Controller
{

	public function ListarJson(Request $request)
	{
		$cols = ['id','origem'];
		if ($request->has('colunas')) {
			$cols = explode(',', $request->input('colunas'));
		}
		$origens = OrigemGames::select($cols)->orderBy('origem')->get();
		echo json_encode($origens);
	}
}

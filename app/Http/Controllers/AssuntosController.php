<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Assuntos;

class AssuntosController extends Controller
{
	public function Salvar(Request $request)
	{
		$assunto = new Assuntos();
		$assunto->assunto = $request->input('assunto');
		if($assunto->save()){
			return json_encode(['status' => 's', 'msg' => 'Assunto salvo com sucesso', 'id' => $assunto->id]);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
	}

	public function ListarJson(Request $request)
	{
		$cols = ['id','assunto'];
		if ($request->has('colunas')) {
			$cols = explode(',', $request->input('colunas'));
		}
		$assuntos = Assuntos::select($cols)->orderBy('assunto')->get();
		echo json_encode($assuntos);
	}
}

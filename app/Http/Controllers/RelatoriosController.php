<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RelatoriosGames;
use App\RelatoriosLeituras;
use App\RelatoriosExercicios;

class RelatoriosController extends Controller
{
	private $meses_nome = ['01'=>'Jan', '02'=>'Fev', '03'=>'Mar', '04'=>'Abr', '05'=>'Mai', '06'=>'Jun', '07'=>'Jul', '08'=>'Ago', '09'=>'Out', '10'=>'Set', '11'=>'Nov', '12'=>'Dez'];

    public function GraficoGamesEleiturasPorMes()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Gráficos', 'Games e leituras por mês'],
    		'view' => 'relatorios.grafico_games_e_leituras_por_mes',
    		'scripts' => [
                ['tipo' => 'url', 'caminho' => 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js'],
                ['tipo' => 'local', 'caminho' => 'relatorios/graficos/games_e_leituras_por_mes']
            ]
    	]);
    }

	public function GamesEleiturasPorMesJson(Request $request)
	{
		$dados['games'] = RelatoriosGames::ConsolidadoPorMes($request->input('ano'));
		$dados['livros'] = RelatoriosLeituras::ConsolidadoPorTipoMidiaAoMes($request->input('ano'), [1]);
		$dados['hqs'] = RelatoriosLeituras::ConsolidadoPorTipoMidiaAoMes($request->input('ano'), [2]);
		$dados['mangas'] = RelatoriosLeituras::ConsolidadoPorTipoMidiaAoMes($request->input('ano'), [3]);

		foreach ($dados['games'] as $k => $v) {
			$dados['games'][$k]->mes = $this->meses_nome[$v->mes];
		}

		foreach ($dados['livros'] as $k => $v) {
			$dados['livros'][$k]->mes = $this->meses_nome[$v->mes];
		}

		foreach ($dados['hqs'] as $k => $v) {
			$dados['hqs'][$k]->mes = $this->meses_nome[$v->mes];
		}

		foreach ($dados['mangas'] as $k => $v) {
			$dados['mangas'][$k]->mes = $this->meses_nome[$v->mes];
		}

		echo json_encode($dados);
	}

	public function DistanciasPorPeriodoJson(Request $request)
	{
		$dados = RelatoriosExercicios::DistanciasPorPeriodo();
		echo json_encode($dados);
	}
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tarefas;

class TarefasController extends Controller
{

	public function MudarSituacao($id_tarefa, $id_situacao)
	{
		$tarefa = Tarefas::find($id_tarefa);
		$tarefa->id_situacao = $id_situacao;
		if($tarefa->save()){
			return json_encode(['status' => 'Sucesso', 'msg' => 'Tarefa atualizada com sucesso!']);
		}else{
			return json_encode(['status' => 'Erro', 'msg' => 'Erro ao atualizar tarefa!']);
		}
	}

	public function Excluir($id_tarefa)
	{
		$tarefa = Tarefas::find($id_tarefa);
		if ($tarefa->delete()) {
			json_encode(['status' => 'Sucesso', 'msg' => 'Tarefa excluída com sucesso']);
		}else{
			json_encode(['status' => 'Erro', 'msg' => 'Ocorreu um erro ao excluir a tarefa']);
		}
	}

	public function Salvar(Request $request)
	{
		$tarefa = new Tarefas();
		$tarefa->id_lista = $request->input('id_lista');
		$tarefa->id_categoria = $request->input('id_categoria');
		$tarefa->id_situacao = $request->input('id_situacao');
		$tarefa->tarefa = $request->input('tarefa');
		if($tarefa->save()){
			return json_encode(['status' => 's', 'msg' => 'Tarefa salva com sucesso', 'id' => $tarefa->id]);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
	}

	public function GetTarefaJson($id_tarefa)
	{
		$tarefa = Tarefas::where('id', $id_tarefa)->with('Situacao')->with('Categoria')->get();
		return json_encode($tarefa[0]);
	}

    public function Atualizar(Request $request)
    {
    	$tarefa = Tarefas::find($request->input('id_tarefa'));
		$tarefa->tarefa = $request->input('tarefa');
		$tarefa->id_categoria = $request->input('categoria');
		$tarefa->id_situacao = $request->input('situacao');
		if($tarefa->save()){
			return json_encode(['status' => 'Sucesso', 'msg' => 'Tarefa atualizada com sucesso!']);
		}else{
			return json_encode(['status' => 'Erro', 'msg' => 'Erro ao atualizar tarefa!']);
		}
    }
}
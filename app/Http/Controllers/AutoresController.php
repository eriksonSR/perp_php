<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Autores;

class AutoresController extends Controller
{
	public function Listar()
	{
		$autores = Autores::orderBy('autor')->get();
		return view('layout.app', [
    		'caminho' => ['Autores', 'Listar'],
    		'view' => 'autores.listar',
    		'dados' => ['autores' => $autores]
    	]);

	}

	public function Cadastrar()
	{
		return view('layout.app', [
    		'caminho' => ['Autores', 'Cadastrar'],
    		'view' => 'autores.cadastrar',
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'autores/cadastrar']
    		]
    	]);
	}

	public function Salvar(Request $request)
	{
		$autor = new Autores();
		$autor->autor = $request->input('autor');
		if($autor->save()){
			return json_encode(['status' => 's', 'msg' => 'Autor salvo com sucesso', 'id' => $autor->id]);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
	}

	public function Editar($id)
	{
		$autor = Autores::find($id);
		return view('layout.app', [
    		'caminho' => ['Autores', 'Editar'],
    		'view' => 'autores.editar',
    		'dados' => [
    			'autor' => $autor
    		],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'autores/editar']
    		]
    	]);
	}

	public function Atualizar(Request $request)
	{
		$autor = Autores::find($request->input('id_autor'));
		$autor->autor = $request->input('autor');
		if($autor->save()){
			return json_encode(['status' => 's', 'msg' => 'Autor salvo com sucesso']);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
	}

	public function ListarJson(Request $request)
	{
		$cols = ['id','autor'];
		if ($request->has('colunas')) {
			$cols = explode(',', $request->input('colunas'));
		}
		$autores = Autores::select($cols)->orderBy('autor')->get();
		echo json_encode($autores);
	}
}

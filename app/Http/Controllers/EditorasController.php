<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Editoras;

class EditorasController extends Controller
{
	public function Listar()
	{
		$editoras = Editoras::orderBy('editora')->get();
		return view('layout.app', [
    		'caminho' => ['Editoras', 'Listar'],
    		'view' => 'editoras.listar',
    		'dados' => ['editoras' => $editoras]
    	]);
	}

	public function Cadastrar()
	{
		return view('layout.app', [
    		'caminho' => ['Editoras', 'Cadastrar'],
    		'view' => 'editoras.cadastrar',
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'editoras/cadastrar']
    		]
    	]);
	}

	public function Salvar(Request $request)
	{
		$editora = new Editoras();
		$editora->editora = $request->input('editora');
		if($editora->save()){
			return json_encode(['status' => 's', 'msg' => 'Editora salvo com sucesso', 'id' => $editora->id]);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
	}

	public function Editar($id)
	{
		$editora = Editoras::find($id);
		return view('layout.app', [
    		'caminho' => ['Editoras', 'Editar'],
    		'view' => 'editoras.editar',
    		'dados' => [
    			'editora' => $editora
    		],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'editoras/editar']
    		]
    	]);
	}

	public function Atualizar(Request $request)
	{
		$editora = Editoras::find($request->input('id_editora'));
		$editora->editora = $request->input('editora');
		if($editora->save()){
			return json_encode(['status' => 's', 'msg' => 'Editora salva com sucesso', 'id' => $editora->id]);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
	}

	public function ListarJson(Request $request)
	{
		$cols = ['id','editora'];
		if ($request->has('colunas')) {
			$cols = explode(',', $request->input('colunas'));
		}
		$editoras = Editoras::select($cols)->orderBy('editora')->get();
		echo json_encode($editoras);
	}
}
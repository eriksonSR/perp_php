<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Situacoes;

class SituacoesController extends Controller
{
	public function ListarJson(Request $request)
	{
		$cols = ['id','situacao'];
		if ($request->has('colunas')) {
			$cols = explode(',', $request->input('colunas'));
		}
		$situacoes = Situacoes::select($cols)->orderBy('situacao')->get();
		echo json_encode($situacoes);
	}
}

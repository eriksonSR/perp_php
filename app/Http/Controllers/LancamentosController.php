<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Lancamentos;
use App\PlanoContas;
use PgUtils;

class LancamentosController extends Controller
{
    public function Cadastrar()
    {
    	$conta = new PlanoContas();
    	return view('layout.app', [
    		'caminho' => ['Contábil', 'Lançamentos', 'Cadastrar'],
    		'view' => 'contabil.lancamentos.cadastrar',
    		'dados' => [
    			'contas' => $conta::orderBy('conta')->get(),
    			'lancamentos_hist' => Lancamentos::orderBy('data', 'desc')->orderBy('id','desc')->take(15)->get(),
    			'lancamentos_inc' => Lancamentos::orderBy('created_at', 'desc')->take(15)->get()
    		],
    		'scripts' => [
    				['tipo' => 'link', 'caminho' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js'],
    				['tipo' => 'local', 'caminho' => 'contabil/lancamentos/cadastrar']
    		]
    	]);
    }

    public function Salvar(Request $request)
	{
		$lancamento = new Lancamentos();
		$data = explode('/', $request->input('data'));
		$data = $data[2] . '-' . $data[1] . '-' . $data[0];
		$valor = str_replace('.', '', $request->input('valor'));
		$valor = str_replace(',', '.', $valor);
		$lancamento->historico = $request->input('historico');
		$lancamento->id_conta_debitada = $request->input('id_conta_debitada');
		$lancamento->id_conta_creditada = $request->input('id_conta_creditada');
		$lancamento->valor = $valor;
		$lancamento->data = $data;

		if($lancamento->save()){
			$lancamentos_hist = Lancamentos::orderBy('data', 'desc')->orderBy('id','desc')->take(15)->get();
			$lancamentos_inc = Lancamentos::orderBy('created_at', 'desc')->take(15)->get();
			foreach ($lancamentos_hist as $l) {
				$l->cc = $l->ContaCreditada->conta;
				$l->cd = $l->ContaDebitada->conta;
				$l->data = PgUtils::DataPgToBr($l->data, '/');
				$l->valor = number_format($l->valor, 2,',','.');
			}
			foreach ($lancamentos_inc as $l) {
				$l->cc = $l->ContaCreditada->conta;
				$l->cd = $l->ContaDebitada->conta;
				$l->data = PgUtils::DataPgToBr($l->data, '/');
				$l->valor = number_format($l->valor, 2,',','.');
			}
			return json_encode([
				'status' => 's', 
				'msg' => 'Lançamento salvo com sucesso',
				'lancamentos_hist' => $lancamentos_hist,
				'lancamentos_inc' => $lancamentos_inc
			]);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
	}

	public function Listar()
	{
		$lancamentos = new Lancamentos();
		$conta = new PlanoContas();
    	return view('layout.app', [
    		'caminho' => ['Contábil', 'Lançamentos', 'Listar'],
    		'view' => 'contabil.lancamentos.listar',
    		'dados' => [
    			'lancamentos' => $lancamentos->Listar(),
    			'contas' => PlanoContas::orderBy('conta')->get()
    		],
    		'scripts' => [
    				['tipo' => 'local', 'caminho' => 'contabil/lancamentos/listar'],
    				['tipo' => 'link', 'caminho' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js'],
    				['tipo' => 'local', 'caminho' => 'contabil/lancamentos/cadastrar']
    			]
    	]);
	}

	public function GetLancamentosJson(Request $request)
	{
		$filtros = $request->input('filtros');
		$lancamentos = new Lancamentos();
		$lancamentos = $lancamentos->Listar($filtros);
		$valor_consolidado = 0;
		foreach ($lancamentos as $v) {
			$valor_consolidado = $valor_consolidado + $v->valor;
			$v->valor = number_format($v->valor, 2,',','.');
		}
		
		$v = (object) ['id' => '', 
			'id_conta_creditada' => '', 
			'id_conta_debitada' => '', 
			'c_cre' => '', 
			'c_deb' => '', 
			'historico' => 'TOTAL',
			'valor' =>  'R$ ' . number_format($valor_consolidado, 2,',','.'),
			'dt' => ''];
		array_push($lancamentos, $v);
		echo json_encode($lancamentos);
	}

	public function GeraExcel(Request $request)
	{
		$filtros = [
			'conta_cred' => $_GET['conta_cred'],
			'conta_deb' => $_GET['conta_deb'],
			'historico' => $_GET['historico'],
			'criterio_valor' => $_GET['criterio_valor'],
			'valor' => $_GET['valor'],
			'criterio_data_ini' => $_GET['criterio_data_ini'],
			'data_ini' => $_GET['data_ini'],
			'criterio_data_fim' => $_GET['criterio_data_fim'],
			'data_fim' => $_GET['data_fim']
		];
		$lancamentos = new Lancamentos();
		$lancamentos = $lancamentos->Listar($filtros);

		$nome_arq = 'lançamentos_' . date('d/m/Y_H:i:s') . '.csv';

		$headers = array(
        	"Content-type" => "text/csv",
        	"Content-Disposition" => "attachment; filename={$nome_arq}",
        	"Pragma" => "no-cache",
        	"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        	"Expires" => "0"
    	);

    	$columns = array('ID', 'CONTA CRED', 'CONTA DEB', 'HISTORICO', 'VALOR', 'DATA');

    	$callback = function() use ($lancamentos, $columns)
    	{
        	$file = fopen('php://output', 'w');
        	fputcsv($file, $columns);

        	foreach($lancamentos as $l) {
            	fputcsv($file, array($l->id, $l->c_cre, $l->c_deb, $l->historico, $l->valor, $l->dt));
        	}
        	
        	fclose($file);
    	};
    	
    	return Response::stream($callback, 200, $headers);
	}

    public function Editar($id)
    {
    	return view('layout.app', [
    		'caminho' => ['Contábil', 'Lançamentos', 'Editar'],
    		'view' => 'contabil.lancamentos.editar',
    		'dados' => [
    			'lancamento' => Lancamentos::find($id),
    			'contas' => PlanoContas::orderBy('conta')->get()
    		],
    		'scripts' => [
    				['tipo' => 'link', 'caminho' => 'https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js'],
    				['tipo' => 'local', 'caminho' => 'contabil/lancamentos/editar']
    		]
    	]);
    }

    public function Atualizar(Request $request)
    {
		$valor = str_replace('.', '', $request->input('valor'));
		$valor = str_replace(',', '.', $valor);
    	$lancamento = Lancamentos::find($request->input('id_lancamento'));
		$lancamento->historico = $request->input('historico');
		$lancamento->id_conta_debitada = $request->input('id_conta_debitada');
		$lancamento->id_conta_creditada = $request->input('id_conta_creditada');
		$lancamento->data = PgUtils::DataBrToPg($request->input('data'), '/');
		$lancamento->valor = $valor;
		if($lancamento->save()){
			return json_encode(['status' => 'Sucesso', 'msg' => 'Lançamento atualizado com sucesso!']);
		}else{
			return json_encode(['status' => 'Erro', 'msg' => 'Erro ao atualizar lnaçamento!']);
		}
    }

    public function GetLancamentoJson($id)
    {
    	$lancamento = Lancamentos::find($id);
		$lancamento->valor = str_replace('.', ',', $lancamento->valor);
		$lancamento->data = PgUtils::DataPgToBr($lancamento->data, '/');
    	echo json_encode($lancamento);
    }
}
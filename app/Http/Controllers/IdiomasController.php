<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Idiomas;

class IdiomasController extends Controller
{
	public function ListarJson(Request $request)
	{
		$cols = ['id','idioma'];
		if ($request->has('colunas')) {
			$cols = explode(',', $request->input('colunas'));
		}
		$idiomas = Idiomas::select($cols)->orderBy('idioma')->get();
		echo json_encode($idiomas);
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pesos;
use PgUtils;
use App\Helpers\Datas;

class PesosController extends Controller
{
    public function Listar()
    {
		return view('layout.app', [
    		'caminho' => ['Pesos', 'Listar'],
    		'view' => 'pesos.listar',
    		'dados' => ['pesos' => Pesos::orderBy('data', 'desc')->get()],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'pesos/listar'],
    			['tipo' => 'local', 'caminho' => 'jquery.mask.min']
    		]
    	]);
    }

    public function Adicionar(Request $request)
    {
    	$peso = new Pesos();
		$peso->data = PgUtils::DataBrToPg($request->input('data'), '/');
		$peso->peso = str_replace(',', '.', $request->input('peso'));
		if($peso->save()){
			return json_encode(['status' => 's', 'msg' => 'Peso adicionado com sucesso', 'id' => $peso->id]);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
    }

    public function ListarJson()
    {
    	$pesos = Pesos::orderBy('data', 'desc')->get();
    	foreach ($pesos as $p) {
    		$p->data_c_dia = Datas::GetDiaDaSemana($p->data)['extenso'] . ',' . PgUtils::DataPgToBr($p->data, '/');
    		$p->peso_br = str_replace('.', ',', $p->peso) . ' Kg';
    	}
    	return json_encode($pesos);
    }

    public function Excluir($id)
    {
        try {
            Pesos::find($id)->delete();
            return json_encode(['status' => 'Sucesso', 'msg' => 'Peso excluído com sucesso!']);
        } catch (Exception $e) {
            return json_encode(['status' => 'Erro', 'msg' => 'Ocorreu um erro ao excluir o peso.']);
        }
    }
}

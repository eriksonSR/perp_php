<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Studios;

class StudiosController extends Controller
{
	public function Listar()
	{
		$studios = Studios::orderBy('studio')->get();
		return view('layout.app', [
    		'caminho' => ['Studios', 'Listar'],
    		'view' => 'studios.listar',
    		'dados' => ['studios' => $studios]
    	]);
	}

	public function Cadastrar()
	{
		return view('layout.app', [
    		'caminho' => ['Studios', 'Cadastrar'],
    		'view' => 'studios.cadastrar',
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'studios/cadastrar']
    		]
    	]);
	}

	public function Salvar(Request $request)
	{
		$studio = new Studios();
		$studio->studio = $request->input('studio');
		if($studio->save()){
			return json_encode(['status' => 's', 'msg' => 'Studio salvo com sucesso', 'id' => $studio->id]);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
	}

	public function Editar($id)
	{
		$studio = Studios::find($id);
		return view('layout.app', [
    		'caminho' => ['Studios', 'Editar'],
    		'view' => 'studios.editar',
    		'dados' => [
    			'studio' => $studio
    		],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'studios/editar']
    		]
    	]);
	}

	public function Atualizar(Request $request)
	{
		$studio = Studios::find($request->input('id_studio'));
		$studio->studio = $request->input('studio');
		if($studio->save()){
			return json_encode(['status' => 's', 'msg' => 'Studio salvo com sucesso']);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
	}

	public function ListarJson(Request $request)
	{
		$cols = ['id','studio'];
		if ($request->has('colunas')) {
			$cols = explode(',', $request->input('colunas'));
		}
		$studios = Studios::select($cols)->orderBy('studio')->get();
		echo json_encode($studios);
	}
}
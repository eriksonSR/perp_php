<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RelatoriosContabeis;
use App\PlanoContas;
use App\Helpers\Datas;

class RelatoriosContabilController extends Controller
{
	public function GastosPorContaPeriodo()
	{
		$conta = new PlanoContas();
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Contábil', 'Gastos por conta e período'],
    		'view' => 'relatorios.contabil.gastos_p_conta_periodo',
    		'dados' => [
    			'gastos' => RelatoriosContabeis::ConsolidadoPorContaNoPeriodo(),
    			'contas' => $conta->GetListaContasComGraduacao()
    		],
            'scripts' => [['tipo' => 'local', 'caminho' => 'contabil/gastos_p_conta_periodo']]
    	]);
	}

	public function GastosPorContaPeriodoJson(Request $request)
	{
		$filtros = $request->input('filtros');
		$gastos = RelatoriosContabeis::ConsolidadoPorContaNoPeriodo($filtros);
		echo json_encode($gastos);
	}
}

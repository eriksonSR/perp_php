<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlanoContas;

class PlanoContasController extends Controller
{
    public function Cadastrar()
    {
        $conta = new PlanoContas();
    	return view('layout.app', [
    		'caminho' => ['Contábil', 'Plano de contas', 'Cadastrar'],
    		'view' => 'contabil.plano_contas.cadastrar',
    		'dados' => $conta->whereNull('id_conta_mae')->orderBy('grau')->get(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'contabil/plano_contas/cadastrar']]
    	]);
    }

    public function Salvar(Request $request)
    {
    	$conta = new PlanoContas();
		$conta->conta = $request->input('conta');
		$conta->tipo = $request->input('tipo');
		$conta->analitica = $request->input('analitica');
		$conta->grau = $request->input('grau');
		$conta->id_conta_mae = $request->input('id_conta_mae');

		if($conta->save()){
			return $conta->id;
		}else{
			return 0;
		}
    }

    public function GetContasJson(Request $request)
    {
    	$filtros = $request->input('filtros');
    	$conta = new PlanoContas();
    	$contas = $conta::where($filtros)->orderBy('id', 'grau')->get();
    	echo json_encode($contas);
    }

    public function Listar()
    {
        $conta = new PlanoContas();
        return view('layout.app', [
            'caminho' => ['Contábil', 'Plano de contas', 'Listar'],
            'view' => 'contabil.plano_contas.listar',
            'dados' => $conta->GetListaContasComGraduacao(),
            'scripts' => [['tipo' => 'local', 'caminho' => 'contabil/plano_contas/listar']]
        ]);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TiposMidia;

class TiposMidiasController extends Controller
{
	public function ListarJson(Request $request)
	{
		$cols = ['id','tipo_midia'];
		if ($request->has('colunas')) {
			$cols = explode(',', $request->input('colunas'));
		}
		$tiposmidia = TiposMidia::select($cols)->orderBy('tipo_midia')->get();
		echo json_encode($tiposmidia);
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consoles;

class ConsolesController extends Controller
{
	public function GetConsolesJson()
	{
		$dados = Consoles::orderBy('console')->get();
		echo json_encode($dados);
	}

	public function ListarJson(Request $request)
	{
		$cols = ['id','id_plataforma','portatil'];
		if ($request->has('colunas')) {
			$cols = explode(',', $request->input('colunas'));
		}
		$consoles = Consoles::select($cols)->orderBy('console')->get();
		echo json_encode($consoles);
	}
}
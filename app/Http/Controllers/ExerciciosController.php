<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exercicios;

class ExerciciosController extends Controller
{
    public function Listar()
    {
		return view('layout.app', [
    		'caminho' => ['Exercicios'],
    		'view' => 'exercicios.listar',
    		'dados' => ['exercicios' => Exercicios::orderBy('data', 'desc')->get()],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'exercicios/listar']
    		]
    	]);
    }
}

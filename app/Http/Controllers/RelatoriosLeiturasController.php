<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RelatoriosLeituras;
use App\Assuntos;
use App\Autores;
use App\Editoras;
use App\TiposMidia;

class RelatoriosLeiturasController extends Controller
{
    public function ConsolidadoPorAno()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Leituras', 'Consolidado por ano'],
    		'view' => 'relatorios.leituras.consolidado_por_ano',
    		'dados' => RelatoriosLeituras::ConsolidadoPorAno(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'leituras/consolidado_por_ano']]
    	]);
    }

    public function ConsolidadoPorAnoJson(Request $request)
    {
    	$dados = RelatoriosLeituras::ConsolidadoPorAno($request->input('ano'));
    	echo json_encode($dados);
    }
    public function ConsolidadoPorAssuntoAoAno()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Leituras', 'Consolidado por assunto ano'],
    		'view' => 'relatorios.leituras.consolidado_por_assunto_ao_ano',
    		'dados' => RelatoriosLeituras::ConsolidadoPorAssuntoAoAno(),
    		'assuntos' => Assuntos::orderBy('assunto')->get(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'leituras/consolidado_por_assunto_ao_ano']]
    	]);
    }

    public function ConsolidadoPorAssuntoAoAnoJson(Request $request)
    {
    	$dados = RelatoriosLeituras::ConsolidadoPorAssuntoAoAno($request->input('ano'), $request->input('assuntos'));
    	echo json_encode($dados);
    }
    public function ConsolidadoPorAutor()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Leituras', 'Consolidado por autor'],
    		'view' => 'relatorios.leituras.consolidado_por_autor',
    		'dados' => RelatoriosLeituras::ConsolidadoPorAutor(),
    		'autores' => Autores::orderBy('autor')->get(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'leituras/consolidado_por_autor']]
    	]);
    }

    public function ConsolidadoPorAutorJson(Request $request)
    {
    	$dados = RelatoriosLeituras::ConsolidadoPorAutor($request->input('autores'));
    	echo json_encode($dados);
    }
    public function ConsolidadoPorEditora()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Leituras', 'Consolidado por editora'],
    		'view' => 'relatorios.leituras.consolidado_por_editora',
    		'dados' => RelatoriosLeituras::ConsolidadoPorEditora(),
    		'editoras' => Editoras::orderBy('editora')->get(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'leituras/consolidado_por_editora']]
    	]);
    }

    public function ConsolidadoPorEditoraJson(Request $request)
    {
    	$dados = RelatoriosLeituras::ConsolidadoPorEditora($request->input('editoras'));
    	echo json_encode($dados);
    }

    public function ConsolidadoPorFormatoAoAno()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Leituras', 'Consolidado por formato ao ano'],
    		'view' => 'relatorios.leituras.consolidado_por_formato_ao_ano',
    		'dados' => RelatoriosLeituras::ConsolidadoPorFormatoAoAno(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'leituras/consolidado_por_formato_ao_ano']]
    	]);
    }

    public function ConsolidadoPorFormatoAoAnoJson(Request $request)
    {
    	$dados = RelatoriosLeituras::ConsolidadoPorFormatoAoAno($request->input('ano'));
    	echo json_encode($dados);
    }

    public function ConsolidadoPorMes()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Leituras', 'Consolidado por mês'],
    		'view' => 'relatorios.leituras.consolidado_por_mes',
    		'dados' => RelatoriosLeituras::ConsolidadoPorMes(),
    		'scripts' => [['tipo' => 'local', 'caminho' => 'leituras/consolidado_por_mes']]
    	]);
    }

    public function ConsolidadoPorMesJson(Request $request)
    {
    	$dados = RelatoriosLeituras::ConsolidadoPorMes($request->input('ano'));
    	echo json_encode($dados);
    }

    public function ConsolidadoPorTipoMidiaAoMes()
    {
    	return view('layout.app', [
    		'caminho' => ['Relatórios', 'Leituras', 'Consolidado por tipo mídia ao mês'],
    		'view' => 'relatorios.leituras.consolidado_por_tipo_midia_ao_mes',
    		'dados' => RelatoriosLeituras::ConsolidadoPorTipoMidiaAoMes(),
    		'tipos' => TiposMidia::orderBy('tipo_midia')->get(),
    		'scripts' => [
                ['tipo' => 'url', 'caminho' => 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js'],
                ['tipo' => 'local', 'caminho' => 'leituras/consolidado_por_tipo_midia_ao_mes']
            ]
    	]);
    }

    public function ConsolidadoPorTipoMidiaAoMesJson(Request $request)
    {
    	$dados = RelatoriosLeituras::ConsolidadoPorTipoMidiaAoMes($request->input('ano'), $request->input('tipos'));
    	echo json_encode($dados);
    }
}
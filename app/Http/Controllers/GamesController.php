<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Generos;
use App\Situacoes;
use App\Studios;
use App\Consoles;
use App\Games;
use App\Plataformas;
use App\OrigemGames;
use App\Lancamentos;
use App\PlanoContas;
use PgUtils;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Helpers\IgdbCrawlerHelper;

class GamesController extends Controller
{
	public function Cadastrar()
	{
		return view('layout.app', [
    		'caminho' => ['Games', 'Cadastrar'],
    		'view' => 'games.cadastrar',
    		'dados' => [
    			'situacoes' => Situacoes::orderBy('situacao')->get(),
    			'generos' => Generos::orderBy('genero')->get(),
    			'studios' => Studios::orderBy('studio')->get(),
    			'consoles' => Consoles::orderBy('console')->get(),
    			'origens' => OrigemGames::orderBy('origem')->get()
    		],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'games/cadastrar']
    		]
    	]);
	}

	public function Salvar(Request $request)
	{
		$id_igdb = $request->input('id_igdb');
		$hp_igdb = new IgdbCrawlerHelper($request->input('url_igdb'));
		$hp_igdb->GetJsonIgdbLocal($id_igdb);
		$imagens = [
			'boxes' => $hp_igdb->GetBoxarts(), 
			'screens' => $hp_igdb->GetScreens(), 
			'arts' => $hp_igdb->GetArtworks()
		];

		$game = new Games();
		$game->id_gamesdb = 0;
		$game->id_igdb = $id_igdb;
		$game->url_igdb = $request->input('url_igdb');
		$game->id_situacao = $request->input('id_situacao');
		$game->id_console = $request->input('id_console');
		$game->id_origem = $request->input('id_origem');
		$game->ids_desenvolvedoras = PgUtils::ArrayPhpToArPg($request->input('ids_desenvolvedoras'));
		$game->ids_publicadoras = PgUtils::ArrayPhpToArPg($request->input('ids_publicadoras'));
		$game->ids_generos = PgUtils::ArrayPhpToArPg($request->input('ids_generos'));
		$game->dt_release =  PgUtils::DataBrToPg($request->input('data_release'), '/');
		if(!empty($request->input('data_termino'))){
			$game->dt_finalizacao =  PgUtils::DataBrToPg($request->input('data_termino'), '/');
		}
		$game->titulo = $request->input('titulo');
		$game->sinopse = $request->input('sinopse');
		$game->dlc = $request->input('dlc');
		$game->digital = $request->input('digital');
		$game->tenho = $request->input('tenho');
		if (!empty($hp_igdb->igdb_json->videos)) {
			$game->videos = json_encode($hp_igdb->igdb_json->videos);
		}
		$game->imagens = json_encode($imagens);
		$game->infos_json_igdb = json_encode($hp_igdb->igdb_json);
		if($game->save()){
			$hp_igdb->SalvarCover($game->id);
			$hp_igdb->SalvarScreens($game->id);
			$hp_igdb->SalvarArtworks($game->id);

			return json_encode(['status' => 's', 'msg' => 'Jogo salvo com sucesso']);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
	}

	public function Listar()
	{
		$games = Games::select(['id','titulo', 'id_console', 'ids_publicadoras', 'digital'])->orderBy('dt_finalizacao', 'desc')->get();
		
		foreach($games as $g) {
			$publicadoras = [];
			$str_ids_publicadoras = str_replace('{', '', $g->ids_publicadoras);
			$str_ids_publicadoras = str_replace('}', '', $str_ids_publicadoras);
			$ar_ids_publicadoras = explode(',', $str_ids_publicadoras);

			foreach ($ar_ids_publicadoras as $p) {
				$studio = Studios::find($p);
				array_push($publicadoras, $studio->studio);
			}

			$g->publicadoras = implode(', ', $publicadoras);
		}
		return view('layout.app', [
    		'caminho' => ['Games', 'Listar'],
    		'view' => 'games.listar',
    		'dados' => [
    			'games' => $games,
    			'consoles' => Consoles::orderBy('console')->get(),
    			'generos' => Generos::orderBy('genero')->get(),
    			'studios' => Studios::orderBy('studio')->get(),
    			'situacoes' => Situacoes::orderBy('situacao')->get(),
    			'plataformas' => Plataformas::orderBy('plataforma')->get()
    		],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'games/listar']
    		]
    	]);
	}

	public function Editar($id)
	{
		$game = Games::find($id);
		return view('layout.app', [
    		'caminho' => ['Games', 'Editar'],
    		'view' => 'games.editar',
    		'dados' => [
    			'game' => $game,
    			'generos' => Generos::orderBy('genero')->get(),
    			'studios' => Studios::orderBy('studio')->get(),
    			'consoles' => Consoles::orderBy('console')->get(),
    			'situacoes' => Situacoes::orderBy('situacao')->get(),
    			'origens' => OrigemGames::orderBy('origem')->get()
    		],
    		'scripts' => [
    			['tipo' => 'local', 'caminho' => 'games/editar']
    		]
    	]);
	}

	public function Atualizar(Request $request)
	{
		$game = Games::find($request->input('id_game'));
		$game->url_igdb = $request->input('url_igdb');
		$game->id_igdb = $request->input('id_igdb');
		$game->id_situacao = $request->input('id_situacao');
		$game->id_console = $request->input('id_console');
		$game->id_origem = $request->input('id_origem');
		$game->ids_desenvolvedoras = PgUtils::ArrayPhpToArPg($request->input('ids_desenvolvedoras'));
		$game->ids_publicadoras = PgUtils::ArrayPhpToArPg($request->input('ids_publicadoras'));
		$game->ids_generos = PgUtils::ArrayPhpToArPg($request->input('ids_generos'));
		if (!empty($request->input('data_release'))) {
			$game->dt_release = PgUtils::DataBrToPg($request->input('data_release'), '/');
		}
		if (!empty($request->input('data_termino'))) {
			$game->dt_finalizacao = PgUtils::DataBrToPg($request->input('data_termino'), '/');
		}
		$game->titulo = $request->input('titulo');
		$game->sinopse = $request->input('sinopse');
		$game->dlc = $request->input('dlc');
		$game->digital = $request->input('digital');
		if($game->save()){
			return json_encode(['status' => 's', 'msg' => 'Jogo salvo com sucesso']);
		}else{
			return json_encode(['status' => 'e', 'msg' => 'Erro ao salvar os dados']);
		}
	}

	public function GetGamesJson(Request $request)
	{
		$filtros = $request->input('filtros');
		if(isset($request->input('filtros')['desenvolvedora_text'])) {
			$dev = $request->input('filtros')['desenvolvedora_text'];
			$studio = Studios::where('studio', $dev)->first();
			$filtros['ids_desenvolvedoras'] = [$studio->id];
		}
		if(isset($request->input('filtros')['genero_text'])) {
			$gen = $request->input('filtros')['genero_text'];
			$genero = Generos::where('genero', $gen)->first();
			$filtros['ids_generos'] = [$genero->id];
		}
		if(isset($request->input('filtros')['plataforma_text'])) {
			$plataforma = $request->input('filtros')['plataforma_text'];
			$plataforma = Plataformas::where('plataforma', $plataforma)->first();
			$filtros['ids_plataformas'] = [$plataforma->id];
		}
		if(isset($request->input('filtros')['publicadora_text'])) {
			$pub = $request->input('filtros')['publicadora_text'];
			$pub = Studios::where('studio', $pub)->first();
			$filtros['ids_publicadoras'] = [$pub->id];
		}
		$games = Games::Listar($filtros);
		foreach ($games as $g) {
			$publicadoras_str = [];
			$publicadoras_ar = PgUtils::ArrayPgToArPhp($g->ids_publicadoras);
			foreach ($publicadoras_ar as $p) {
				array_push($publicadoras_str, Studios::find($p)->studio);
			}
			$g->publicadoras = implode(', ', $publicadoras_str);
			
			if($g->digital == 1){
				$g->digital = 'Digital';
			}else{
				$g->digital = 'Fisíco';
			}
		}
		echo json_encode($games);
	}

	public function GeraExcel()
	{
		$filtros = [
			'titulo' => Input::get('titulo'),
			'ids_consoles' => Input::get('ids_consoles'),
			'ids_plataformas' => Input::get('ids_plataformas'),
			'ids_generos' => Input::get('ids_generos'),
			'id_situacao' => Input::get('id_situacao'),
			'ids_publicadoras' => Input::get('ids_publicadoras'),
			'ids_desenvolvedoras' => Input::get('ids_desenvolvedoras'),
			'criterio_ini_dt_finalizacao' => Input::get('criterio_ini_dt_finalizacao'),
			'data_ini' => Input::get('data_ini'),
			'criterio_final_dt_finalizacao' => Input::get('criterio_final_dt_finalizacao'),
			'data_final' => Input::get('data_final'),
			'dlc' => Input::get('dlc'),
			'digital' => Input::get('digital'),
			'tenho' => Input::get('tenho')
		];

		$games = Games::Listar($filtros);
		
		$nome_arq = 'games_' . date('d/m/Y_H:i:s') . '.csv';
		$headers = array(
        	"Content-type" => "text/csv",
        	"Content-Disposition" => "attachment; filename={$nome_arq}",
        	"Pragma" => "no-cache",
        	"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        	"Expires" => "0"
    	);
    	$columns = array('ID', 'TITULO', 'CONSOLE', 'PUBLICADORA', 'DIGITAL');

    	$callback = function() use ($games, $columns)
    	{
        	$file = fopen('php://output', 'w');
        	fputcsv($file, $columns);

        	foreach($games as $g) {
				$publicadoras_ar = PgUtils::ArrayPgToArPhp($g->ids_publicadoras);
				foreach ($publicadoras_ar as $p) {
					$digital = $g->digital == 1 ? 'Digital' : 'Fisíco';
            		fputcsv($file, array($g->id, $g->titulo, $g->console, Studios::find($p)->studio, $digital));
				}
        	}
        	
        	fclose($file);
    	};
    	
    	return Response::stream($callback, 200, $headers);
	}

	public function InfosApis(Request $request)
	{
		$dados = [
			'titulo' => '',
			'id_igdb' => 0,
			'dt_release' => '',
			'sinopse' => ''
		];

		$hp_igdb = new IgdbCrawlerHelper($request->input('url_igdb'));
		$hp_igdb->GetJsonIgdb();

		if ($hp_igdb->igdb_json->status == 'Sucesso') {
			$hp_igdb->SalvarJson();
			$dados['titulo'] = $hp_igdb->igdb_json->dados->name;
			$dados['id_igdb'] = $hp_igdb->igdb_json->dados->id;

			if (isset($hp_igdb->igdb_json->dados->summary)) {
				$dados['sinopse'] = strip_tags($hp_igdb->igdb_json->dados->summary);
			}

			if(isset($hp_igdb->igdb_json->dados->release_date)) {
				$dados['dt_release'] = date('d/m/Y', strtotime($hp_igdb->igdb_json->dados->release_date));
			}
		}

		return json_encode($dados);
	}

	public function Ficha($id)
	{
		$game = Games::find($id);
		$boxarts = [];
		$fanarts = [];
		$screenshots = [];
		
		if(is_dir(public_path('/images/games/boxarts/' . $id))){
			$boxarts = array_diff(scandir(public_path('/images/games/boxarts/' . $id)), array('.', '..'));
		}
		if(is_dir(public_path('/images/games/fanarts/' . $id . '/thumbs'))){
			$fanarts = array_diff(scandir(public_path('/images/games/fanarts/' . $id . '/thumbs')), array('.', '..'));
		}
		if(is_dir(public_path('/images/games/screenshots/' . $id . '/thumbs'))){
			$screenshots = array_diff(scandir(public_path('/images/games/screenshots/' . $id . '/thumbs')), array('.', '..'));
		}

		$publicadoras_str = [];
		$publicadoras_ar = PgUtils::ArrayPgToArPhp($game->ids_publicadoras);
		foreach ($publicadoras_ar as $p) {
			array_push($publicadoras_str, Studios::find($p)->studio);
		}
		$game->publicadoras = implode(', ', $publicadoras_str);

		$desenvolvedoras_str = [];
		$desenvolvedoras_ar = PgUtils::ArrayPgToArPhp($game->ids_desenvolvedoras);
		foreach ($desenvolvedoras_ar as $d) {
			array_push($desenvolvedoras_str, Studios::find($d)->studio);
		}
		$game->desenvolvedoras = implode(', ', $desenvolvedoras_str);

		$generos_str = [];
		$generos_ar = PgUtils::ArrayPgToArPhp($game->ids_generos);
		foreach ($generos_ar as $g) {
			array_push($generos_str, Generos::find($g)->genero);
		}
		$game->generos = implode(', ', $generos_str);
		$game->videos = json_decode($game->videos);
		return view('layout.app', [
    		'caminho' => ['Games', 'Ficha'],
    		'view' => 'games.ficha',
    		'dados' => [
    			'game' => $game,
    			'boxarts' => $boxarts,
    			'fanarts' => $fanarts,
    			'screenshots' => $screenshots
    		]
    	]);
    }

	public function ExportarTabela()
	{
		$games = Games::ExportarTabela();

		$nome_arq = 'tb_games_' . date('d/m/Y_H:i:s') . '.csv';
		$headers = array(
        	"Content-type" => "text/csv",
        	"Content-Disposition" => "attachment; filename={$nome_arq}",
        	"Pragma" => "no-cache",
        	"Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        	"Expires" => "0"
    	);
    	$columns = ['ID', 'ID_IGDB', 'ID_GAMESDB', 'CONSOLE', 'PORTÁTIL', 'TITULO', 'PUBLICADORA', 'DESENVOLVEDORA', 'GÊNERO', 'DIGITAL', 'FINALIZAÇÃO', 'RELEASE'];

    	$callback = function() use ($games, $columns)
    	{
        	$file = fopen('php://output', 'w');
        	fputcsv($file, $columns);

			foreach ($games as $g) {
				fputcsv($file, array($g->id, $g->id_igdb, $g->id_gamesdb, $g->console, $g->portatil, $g->titulo, $g->pub, $g->dev, $g->genero, $g->digital, $g->dt_finalizacao, $g->dt_release));
			}
        	
        	fclose($file);
    	};
    	return Response::stream($callback, 200, $headers);
	}

	public function ListarJson(Request $request)
	{
		$cols = [
			'id',
			'titulo', 
			'digital',
			'dlc',
			'tenho',
			'dt_finalizacao',
			'dt_release',
			'id_console',
			'id_origem',
			'id_situacao',
			'ids_desenvolvedoras',
			'ids_generos',
			'ids_publicadoras'
		];

		if ($request->has('colunas')) {
			$cols = explode(',', $request->input('colunas'));
		}

		$games = Games::select($cols)->orderBy('titulo')->get();
		foreach ($games as $g) {
			if (isset($g->ids_desenvolvedoras) && !empty($g->ids_desenvolvedoras)) {
				$g->ids_desenvolvedoras = implode(',', PgUtils::ArrayPgToArPhp($g->ids_desenvolvedoras));
			}
			if (isset($g->ids_generos) && !empty($g->ids_generos)) {
				$g->ids_generos = implode(',', PgUtils::ArrayPgToArPhp($g->ids_generos));
			}
			if (isset($g->ids_publicadoras) && !empty($g->ids_publicadoras)) {
				$g->ids_publicadoras = implode(',', PgUtils::ArrayPgToArPhp($g->ids_publicadoras));
			}
		}
		echo json_encode($games);
	}
}
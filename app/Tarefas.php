<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarefas extends Model
{

	protected $table = 'tb_tarefas';

	public function getDateFormat()
	{
		return 'Y-m-d H:i:s.u';
	}

    public function Categoria()
    {
        return $this->belongsTo('App\CategoriaTarefa', 'id_categoria');
    }

    public function Situacao()
    {
        return $this->belongsTo('App\SituacoesTarefas', 'id_situacao');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercicios extends Model
{    
    public $timestamps = false;
	protected $table = 'tb_exercicios';

	public function TipoExercicio(){
        return $this->belongsTo('App\TiposExercicios', 'id_tipo_exercicio');
    }
}

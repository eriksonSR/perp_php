<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RelatoriosLeituras extends Model
{
	public $timestamps = false;
	protected $table = 'tb_letiruas';

	public static function ConsolidadoPorAno($ano = null)
	{		
		$parametros = [];
		$filtros = "WHERE l.id_situacao = 2 ";
		if(isset($ano)){
			$parametros['ano'] = $ano;
			$filtros .= 'AND to_char(l.dt_finalizacao, \'YYYY\') = :ano';
		}
		$select = "
			SELECT 
				to_char(dt_finalizacao, 'YYYY') ano, count(*) total
			FROM tb_leituras l
			$filtros
			GROUP BY 1
			ORDER BY ano
		";
		$dados = DB::select($select, $parametros);
		return $dados;
	}

	public static function ConsolidadoPorAssuntoAoAno($ano = null, $assuntos = null)
	{
		$parametros = [];
		$filtros = "WHERE l.id_situacao = 2 ";
		if(isset($ano)){
			$parametros['ano'] = $ano;
			$filtros .= ' AND to_char(l.dt_finalizacao, \'YYYY\') = :ano';
		}
		if(isset($assuntos)){
			$filtros .= ' AND a.id IN (';
			$cont = 1;
			//Para cada assunto, adiciona um parâmetro nomeado
			foreach($assuntos as $a){
				$filtros .= ':assunto' . $cont . ',';
				$parametros['assunto'.$cont] = $a;
				$cont++;
			}
			//Remove a última vírgula e fecha o IN
			$filtros = substr($filtros, 0, -1);
			$filtros .= ')';
		}

		$select = "
			SELECT 
				to_char(l.dt_finalizacao, 'YYYY') ano, a.assunto, count(*) total
			FROM tb_leituras l
			INNER JOIN tb_assuntos a on a.id = ANY(l.ids_assuntos)
			$filtros
			GROUP BY 1, assunto
			ORDER BY ano, assunto ASC
			";
		return DB::select($select, $parametros);
	}

	public static function ConsolidadoPorAutor($autores = null)
	{
		$parametros = [];
		$filtros = "WHERE l.id_situacao = 2 ";
		if(isset($autores)){
			$filtros .= 'AND a.id IN (';
			$cont = 1;
			//Para cada autor, adiciona um parâmetro nomeado
			foreach($autores as $a){
				$filtros .= ':autor' . $cont . ',';
				$parametros['autor'.$cont] = $a;
				$cont++;
			}
			//Remove a última vírgula e fecha o IN
			$filtros = substr($filtros, 0, -1);
			$filtros .= ')';
		}

		$select = "
			SELECT 
				a.autor, count(*) total
			FROM tb_leituras l
			INNER JOIN tb_autores a on a.id = ANY(l.ids_autores)
			$filtros
			GROUP BY a.autor
			ORDER BY total DESC
			";
		return DB::select($select, $parametros);
	}

	public static function ConsolidadoPorEditora($editoras = null)
	{
		$parametros = [];
		$filtros = "WHERE l.id_situacao = 2 ";
		if(isset($editoras)){
			$filtros .= 'AND e.id IN (';
			$cont = 1;
			//Para cada editora, adiciona um parâmetro nomeado
			foreach($editoras as $a){
				$filtros .= ':editora' . $cont . ',';
				$parametros['editora'.$cont] = $a;
				$cont++;
			}
			//Remove a última vírgula e fecha o IN
			$filtros = substr($filtros, 0, -1);
			$filtros .= ')';
		}

		$select = "
			SELECT 
				e.editora, count(*) total
			FROM tb_leituras l
			INNER JOIN tb_editoras e ON e.id = ANY(l.ids_editoras)
			$filtros
			GROUP BY e.editora
			ORDER BY editora
			";
		return DB::select($select, $parametros);
	}

	public static function ConsolidadoPorFormatoAoAno($ano = null)
	{
		$parametros = [];
		$filtros = "WHERE l.id_situacao = 2 ";
		if(isset($ano)){
			$parametros['ano'] = $ano;
			$filtros .= 'AND to_char(l.dt_finalizacao, \'YYYY\') = :ano';
		}

		$select = "
			SELECT
			    CASE 
			        WHEN digital = 1 THEN 'Digital'
			        ELSE
						'Fisíco'
			    END AS formato,
			    to_char(dt_finalizacao, 'YYYY') ano,
			    count(*) total
			FROM tb_leituras l
			$filtros
			GROUP BY formato, 2
			ORDER BY 2, formato
			";
		return DB::select($select, $parametros);
	}

	public static function ConsolidadoPorMes($ano = null)
	{
		$parametros = [];
		$filtros = "WHERE l.id_situacao = 2 ";
		if(isset($ano)){
			$parametros['ano'] = $ano;
			$filtros .= 'AND to_char(l.dt_finalizacao, \'YYYY\') = :ano';
		}

		$select = "
			SELECT 
				to_char(dt_finalizacao, 'MM') mes, to_char(dt_finalizacao, 'YYYY') ano, count(*) total
			FROM tb_leituras l
			$filtros
			GROUP BY 2, 1
			ORDER BY ano, mes
			";
		return DB::select($select, $parametros);
	}

	public static function ConsolidadoPorTipoMidiaAoMes($ano = null, $tipos = null)
	{
		$parametros = [];
		$filtros = "WHERE l.id_situacao = 2";
		if(isset($ano)){
			$parametros['ano'] = $ano;
			$filtros .= ' AND to_char(l.dt_finalizacao, \'YYYY\') = :ano';
		}
		if(isset($tipos)){
			$filtros .= ' AND tm.id IN (';
			$cont = 1;
			//Para cada tipo, adiciona um parâmetro nomeado
			foreach($tipos as $t){
				$filtros .= ':tipo' . $cont . ',';
				$parametros['tipo'.$cont] = $t;
				$cont++;
			}
			//Remove a última vírgula e fecha o IN
			$filtros = substr($filtros, 0, -1);
			$filtros .= ')';
		}

		$select = "
			SELECT
			    tm.tipo_midia, to_char(dt_finalizacao, 'MM') mes, to_char(l.dt_finalizacao, 'YYYY') ano, count(*) total
			FROM tb_leituras l
			INNER JOIN tb_tipos_midias tm ON tm.id = l.id_tipo_midia
			$filtros
			GROUP BY tm.tipo_midia, 3, 2
			ORDER BY 3, 2
			";
		return DB::select($select, $parametros);
	}

	public static function UltimosLivrosLidos()
	{
		$select = "
			SELECT titulo, dt_finalizacao 
			FROM tb_leituras 
			WHERE id_situacao = 2
			AND id_tipo_midia = 1
			ORDER BY dt_finalizacao DESC
			LIMIT 5
		";
		return DB::select($select);
	}

	public static function UltimosHsLidos()
	{
		$select = "
			SELECT titulo, dt_finalizacao 
			FROM tb_leituras 
			WHERE id_situacao = 2
			AND id_tipo_midia = 2
			ORDER BY dt_finalizacao DESC
			LIMIT 5
		";
		return DB::select($select);
	}

	public static function UltimosMangasLidos()
	{
		$select = "
			SELECT titulo, dt_finalizacao 
			FROM tb_leituras 
			WHERE id_situacao = 2
			AND id_tipo_midia = 3
			ORDER BY dt_finalizacao DESC
			LIMIT 5
		";
		return DB::select($select);
	}

	public static function QuantidadeMidiasFisicas()
	{
		$select = "
				SELECT tm.tipo_midia, count(*) total
				FROM tb_leituras l
				INNER JOIN tb_tipos_midias tm ON tm.id = l.id_tipo_midia
				WHERE l.digital = 0
				GROUP BY tm.tipo_midia
				";
		return DB::select($select);
	}

	public static function LivrosPorAssunto($ano)
	{
		$select = "
			SELECT a.assunto, count(*) qtd
			FROM tb_leituras l
			INNER JOIN tb_assuntos a ON a.id = any(l.ids_assuntos)
			WHERE l.id_situacao = 2
			AND to_char(dt_finalizacao, 'YYYY') = '{$ano}'
			AND l.id_tipo_midia = 1
			GROUP BY assunto
			ORDER BY qtd DESC
			LIMIT 5
		";
		return DB::select($select);
	}

	public static function HqsPorAssunto($ano)
	{
		$select = "
			SELECT a.assunto, count(*) qtd
			FROM tb_leituras l
			INNER JOIN tb_assuntos a ON a.id = any(l.ids_assuntos)
			WHERE l.id_situacao = 2
			AND to_char(dt_finalizacao, 'YYYY') = '{$ano}'
			AND l.id_tipo_midia = 2
			GROUP BY assunto
			ORDER BY qtd DESC
			LIMIT 5
		";
		return DB::select($select);
	}

	public static function QtdLivrosFinalizados()
	{
		$select = "
			SELECT count(*) tot FROM tb_leituras
			WHERE id_situacao = 2
			AND id_tipo_midia = 1
		";
		return DB::select($select);
	}

	public static function QtdLivrosNaoIniciados()
	{
		$select = "
			SELECT count(*) tot FROM tb_leituras
			WHERE id_situacao = 3
			AND id_tipo_midia = 1
		";
		return DB::select($select);
	}

	public static function QtdHqsFinalizados()
	{
		$select = "
			SELECT count(*) tot FROM tb_leituras
			WHERE id_situacao = 2
			AND id_tipo_midia = 2
		";
		return DB::select($select);
	}

	public static function QtdHqsNaoIniciados()
	{
		$select = "
			SELECT count(*) tot FROM tb_leituras
			WHERE id_situacao = 3
			AND id_tipo_midia = 2
		";
		return DB::select($select);
	}

	public static function QtdMangasFinalizados()
	{
		$select = "
			SELECT count(*) tot FROM tb_leituras
			WHERE id_situacao = 2
			AND id_tipo_midia = 3
		";
		return DB::select($select);
	}

	public static function QtdMangasNaoIniciados()
	{
		$select = "
			SELECT count(*) tot FROM tb_leituras
			WHERE id_situacao = 3
			AND id_tipo_midia = 3
		";
		return DB::select($select);
	}
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposExercicios extends Model
{
    public $timestamps = false;
	protected $table = 'tb_tipos_exercicio';
}

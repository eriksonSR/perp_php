<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use PgUtils;

class Leituras extends Model
{
    protected $table = 'tb_leituras';
    public $dir_capas = '';

    public function __construct()
    {
        $this->dir_capas = env('DIR_IMAGE_LEITURAS');
    }
	
	public function BaixarCapa($id_skoob, $url_capa)
    {
    	$ar_status = ['status' => '', 'msg' => ''];
    	if($this->CriaDiretorioCapa($id_skoob)) {
    		if($this->SalvarCapa($id_skoob, $url_capa)){
    			$ar_status['status'] = 's';
    			$ar_status['msg'] = 'Capa salva com sucesso!';
    			return $ar_status;
    		}else{
    			$ar_status['status'] = 'e';
    			$ar_status['msg'] = 'Erro ao salvar capa!';
    			return $ar_status;
    		}
    	}else{
    		$ar_status['status'] = 'e';
    		$ar_status['msg'] = 'Erro ao criar diretório!';
    		return $ar_status;
    	}
    }

    public function CriaDiretorioCapa($id_skoob)
    {
    	return mkdir($this->dir_capas . '/' . $id_skoob);
    }

    public function SalvarCapa($id_skoob, $url_capa)
    {
		return copy($url_capa, $this->dir_capas . $id_skoob . '/capa_grande.jpg');
    }

    public function TipoMidia(){
        return $this->belongsTo('App\TiposMidia', 'id_tipo_midia');
    }

    public function Situacao(){
        return $this->belongsTo('App\Situacoes', 'id_situacao');
    }

    public static function Listar($filtros = null)
    {
        $filtros_str = '';
        $parametros = [];

        if (!empty($filtros)) {
            $filtros_str = ' WHERE 0 = 0 ';

            if(!empty($filtros['titulo'])){
                $filtros_str = $filtros_str . " AND titulo ILIKE '%" . $filtros['titulo'] . "%'";
            }

            if(!empty($filtros['subtitulo'])){
                $filtros_str = $filtros_str . " AND subtitulo ILIKE '%" . $filtros['subtitulo'] . "%'";
            }

            if(!empty($filtros['ids_autores'])){
                $filtros_str .= ' AND l.ids_autores && ARRAY[' . implode(',', $filtros['ids_autores']) . ']';
            }

            if(!empty($filtros['ids_editoras'])){
                $filtros_str .= ' AND l.ids_editoras && ARRAY[' . implode(',', $filtros['ids_editoras']) . ']';
            }

            if(!empty($filtros['ids_assuntos'])){
                $filtros_str .= ' AND l.ids_assuntos && ARRAY[' . implode(',', $filtros['ids_assuntos']) . ']';
            }

            if(!empty($filtros['data_ini'])){
                $filtros_str = $filtros_str . ' AND dt_finalizacao ' . $filtros['criterio_ini_dt_finalizacao'] . ' :data_ini';
                $parametros['data_ini'] = PgUtils::DataBrToPg($filtros['data_ini'], '/');
            }

            if(!empty($filtros['data_final'])){
                $filtros_str = $filtros_str . ' AND dt_finalizacao ' . $filtros['criterio_final_dt_finalizacao'] . ' :data_final';
                $parametros['data_final'] = PgUtils::DataBrToPg($filtros['data_final'], '/');
            }

            if(!empty($filtros['favorito'])){
                $filtros_str = $filtros_str . ' AND favorito = 1';
            }

            if(!empty($filtros['digital'])){
                $filtros_str = $filtros_str . ' AND digital = 1';
            }

            if(!empty($filtros['id_situacao'])){
                $filtros_str = $filtros_str . ' AND id_situacao = :id_situacao';
                $parametros['id_situacao'] = $filtros['id_situacao'];
            }

            if(!empty($filtros['id_tipo_midia'])){
                $filtros_str = $filtros_str . ' AND id_tipo_midia = :id_tipo_midia';
                $parametros['id_tipo_midia'] = $filtros['id_tipo_midia'];
            }

            if(!empty($filtros['ano'])){
                $filtros_str = $filtros_str . ' AND to_char(l.dt_finalizacao, \'YYYY\') = \'' . $filtros['ano'] . '\'';
            }

            if(isset($filtros['formato_text'])){
                if ($filtros['formato_text'] == 'Digital') {
                    $filtros_str = $filtros_str . ' AND l.digital = 1';
                }else{
                    $filtros_str = $filtros_str . ' AND l.digital = 0';
                }
            }

            if(!empty($filtros['mes'])){
                $filtros_str = $filtros_str . ' AND to_char(l.dt_finalizacao, \'MM\') = \'' . $filtros['mes'] . '\'';
            }
        }
        
        $select = "
            SELECT
                l.id,
                l.titulo,
                l.subtitulo,
                l.ids_autores,
                l.ids_editoras,
                tm.tipo_midia tipo,
                ts.situacao,
                l.ids_assuntos,
                CASE l.digital
                    WHEN 1 THEN
                        'Fisíco'
                    ELSE
                        'Digital'
                END formato,
                to_char(dt_finalizacao, 'dd/mm/yyyy') dt_finalizacao
            FROM tb_leituras l
            INNER JOIN tb_tipos_midias tm ON tm.id = l.id_tipo_midia
            INNER JOIN tb_situacoes ts ON ts.id = l.id_situacao
            $filtros_str
            ORDER BY l.dt_finalizacao DESC";
        return DB::select($select, $parametros);
    }

    public static function ExportarTabela()
    {
        $select = "
            SELECT
                l.id,
                l.titulo,
                l.subtitulo,
                ta.autor,
                te.editora,
                tm.tipo_midia tipo,
                ts.situacao,
                CASE l.digital
                    WHEN 1 THEN
                        'Fisíco'
                    ELSE
                        'Digital'
                    END formato,
                tast.assunto,
                to_char(dt_finalizacao, 'dd/mm/yyyy') dt_finalizacao,
                l.nota,
                l.paginas,
                l.isbn
                FROM tb_leituras l
            INNER JOIN tb_tipos_midias tm ON tm.id = l.id_tipo_midia
            INNER JOIN tb_situacoes ts ON ts.id = l.id_situacao
            INNER JOIN tb_autores ta ON ta.id = any (l.ids_autores)
            INNER JOIN tb_editoras te ON te.id = any (l.ids_editoras)
            INNER JOIN tb_assuntos tast ON tast.id = any (l.ids_assuntos)
            ORDER BY l.dt_finalizacao DESC";

        return DB::select($select);
    }

    public static function BuscaGeral($termo)
    {
        $select = "
            SELECT
                l.id,
                l.titulo,
                tm.tipo_midia tipo,
                ts.situacao
            FROM tb_leituras l
            INNER JOIN tb_tipos_midias tm ON tm.id = l.id_tipo_midia
            INNER JOIN tb_situacoes ts ON ts.id = l.id_situacao
            WHERE l.titulo ILIKE '%{$termo}%'
            ORDER BY l.titulo";
        return DB::select($select);
    }
}
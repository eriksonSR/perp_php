import requests, json
from bs4 import BeautifulSoup
from sys import argv

class IGDBCrawler():
    def __init__(self, url):
        self.base_url = url
        self.headers = {
            'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15',
            'Accept-Language':'en-gb',
            'Accept-Encoding': 'br, gzip, deflate',
            'Accept':'test/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Referer':'http://www.google.com/'
        }
        self.url_img = 'https://images.igdb.com/igdb/image/upload/t_1080p/'
        self.html = False
        self.bs = False
        self.json_pag = False
        self.dados = {
            'titulo': False,
            'sinopse': False,
            'dt_release': False,
            'img_capa': False,
            'imgs_screen': [],
            'imgs_art': []
        }
        self.erro = False

    def acessarPagina(self):
        try:
            r = requests.get(self.base_url, headers=self.headers)
            if r.status_code == 200:
                self.html = r.text
                self.bs = BeautifulSoup(self.html, 'html.parser')
            else:
                self.erro = {
                    'etapa': 'Acessar Página',
                    'erro': r'Erro no requisição: {r.status_code}'
                }
        except Exception as e:
            self.erro = {
                'etapa': 'Acessar Página',
                'erro': str(e)
            }

    def getJson(self):
        try:
            self.json_pag = self.bs.select('div#content-page > div[data-react-class="GamePageHeader"]')[0].get('data-react-props')
            self.json_pag = json.loads(self.json_pag)
        except Exception as e:
            self.erro = {
                'etapa': 'Get Json',
                'erro': str(e)
            }

    def _getImagensPagina(self, bs):
        divs_imagens = bs.select('div.media-thumbs-item.media-thumbs-item-image')
        # Nome dos screenshots e artworks
        for di in divs_imagens:
            tipo_img = di.select_one('div.media-thumbs-item-title-wrapper div.media-thumbs-item-title').text
            if tipo_img == 'Artwork':
                self.dados['imgs_art'].append(self.url_img + di.select_one('img').get('src').split('/')[-1])
            elif tipo_img == 'Screenshot':
                self.dados['imgs_screen'].append(self.url_img + di.select_one('img').get('src').split('/')[-1])

        self.dados['img_capa'] = self.url_img + bs.find('meta', {'property':'og:image'}).get('content').split('/')[-1]

    def _getTitulo(self, bso):
        self.dados['titulo'] = bso.select('h1.banner-title')[0].get_text()

c = IGDBCrawler(argv[1])
c.acessarPagina()
if c.erro == False:
    c.getJson()

json_retorno = {
    'status':'',
    'dados': {}
}
if c.erro == False:
    json_retorno['status'] = 'Sucesso'
    json_retorno['dados'] = c.json_pag
else:
    json_retorno['status'] = 'Erro'
    json_retorno['dados'] = c.erro

print(json.dumps(json_retorno))
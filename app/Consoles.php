<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consoles extends Model
{
    public $timestamps = false;
	protected $table = 'tb_consoles';
}
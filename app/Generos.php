<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Generos extends Model
{
    public $timestamps = false;
	protected $table = 'tb_generos';
}

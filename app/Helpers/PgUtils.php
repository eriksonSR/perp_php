<?php
namespace App\Helpers;

class PgUtils
{
    public static function ArrayPgToArPhp(string $str)
    {
        $str = str_replace('{', '', $str);
		$str = str_replace('}', '', $str);
		return explode(',', $str);
    }

    public static function ArrayPhpToArPg(array $ar)
    {
    	$str = '{' . implode(',', $ar) . '}';
    	return $str;
    }

    public static function DataBrToPg($data, $separador)
    {
    	$dt = explode($separador, $data);
		$dt = $dt[2] . '-' . $dt[1] . '-' . $dt[0];
		return $dt;
    }

    public static function DataPgToBr($data, $separador, $tem_hr = 0)
    {
        $dt = '';
        if ($tem_hr) {
            $dt = explode(' ', $data);
            $dt = explode('-', $dt[0]);
            $dt = $dt[2] . $separador . $dt[1] . $separador . $dt[0];
        }else{
            $dt = explode('-', $data);
            $dt = $dt[2] . $separador . $dt[1] . $separador . $dt[0];
        }
		return $dt;
    }
}
?>
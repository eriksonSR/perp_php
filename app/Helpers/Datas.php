<?php
namespace App\Helpers;

class Datas
{
    private static $meses = [
    	'01' => ['abreviado' => 'Jan', 'extenso' => 'Janeiro'],
    	'1' => ['abreviado' => 'Jan', 'extenso' => 'Janeiro'],
    	'Jan' => ['dois_dig' => '01', 'um_dig' => '1'],
    	'Janeiro' => ['dois_dig' => '01', 'um_dig' => '1'],
    	'02' => ['abreviado' => 'Fev', 'extenso' => 'Fevereiro'],
    	'2' => ['abreviado' => 'Fev', 'extenso' => 'Fevereiro'],
        'Fev' => ['dois_dig' => '02', 'um_dig' => '2'],
    	'Feb' => ['dois_dig' => '02', 'um_dig' => '2'],
    	'Fevereiro' => ['dois_dig' => '02', 'um_dig' => '2'],
    	'03' => ['abreviado' => 'Mar', 'extenso' => 'Março'],
    	'3' => ['abreviado' => 'Mar', 'extenso' => 'Março'],
    	'Mar' => ['dois_dig' => '03', 'um_dig' => '3'],
    	'Março' => ['dois_dig' => '03', 'um_dig' => '3'],
    	'04' => ['abreviado' => 'Abr', 'extenso' => 'Abril'],
    	'4' => ['abreviado' => 'Abr', 'extenso' => 'Abril'],
        'Abr' => ['dois_dig' => '04', 'um_dig' => '4'],
    	'Apr' => ['dois_dig' => '04', 'um_dig' => '4'],
    	'Abril' => ['dois_dig' => '04', 'um_dig' => '4'],
    	'05' => ['abreviado' => 'Mai', 'extenso' => 'Maio'],
    	'5' => ['abreviado' => 'Mai', 'extenso' => 'Maio'],
        'Mai' => ['dois_dig' => '05', 'um_dig' => '5'],
    	'May' => ['dois_dig' => '05', 'um_dig' => '5'],
    	'Maio' => ['dois_dig' => '05', 'um_dig' => '5'],
    	'06' => ['abreviado' => 'Jun', 'extenso' => 'Junho'],
    	'6' => ['abreviado' => 'Jun', 'extenso' => 'Junho'],
    	'Jun' => ['dois_dig' => '06', 'um_dig' => '6'],
    	'Junho' => ['dois_dig' => '06', 'um_dig' => '6'],
    	'07' => ['abreviado' => 'Jul', 'extenso' => 'Julho'],
    	'7' => ['abreviado' => 'Jul', 'extenso' => 'Julho'],
    	'Jul' => ['dois_dig' => '07', 'um_dig' => '7'],
    	'Julho' => ['dois_dig' => '07', 'um_dig' => '7'],
    	'08' => ['abreviado' => 'Ago', 'extenso' => 'Agosto'],
    	'8' => ['abreviado' => 'Ago', 'extenso' => 'Agosto'],
        'Ago' => ['dois_dig' => '08', 'um_dig' => '8'],
    	'Aug' => ['dois_dig' => '08', 'um_dig' => '8'],
    	'Agosto' => ['dois_dig' => '08', 'um_dig' => '8'],
    	'09' => ['abreviado' => 'Set', 'extenso' => 'Setembro'],
    	'9' => ['abreviado' => 'Set', 'extenso' => 'Setembro'],
        'Set' => ['dois_dig' => '09', 'um_dig' => '9'],
    	'Sep' => ['dois_dig' => '09', 'um_dig' => '9'],
    	'Setembro' => ['dois_dig' => '09', 'um_dig' => '9'],
    	'10' => ['abreviado' => 'Out', 'extenso' => 'Outubro'],
        'Out' => ['dois_dig' => '10', 'um_dig' => '10'],
    	'Oct' => ['dois_dig' => '10', 'um_dig' => '10'],
    	'Outubro' => ['dois_dig' => '10', 'um_dig' => '10'],
    	'11' => ['abreviado' => 'Nov', 'extenso' => 'Novembro'],
    	'Nov' => ['dois_dig' => '11', 'um_dig' => '11'],
    	'Novembro' => ['dois_dig' => '11', 'um_dig' => '11'],
    	'12' => ['abreviado' => 'Dez', 'extenso' => 'Dezembro'],
        'Dez' => ['dois_dig' => '12', 'um_dig' => '12'],
    	'Dec' => ['dois_dig' => '12', 'um_dig' => '12'],
    	'Dezembro' => ['dois_dig' => '12', 'um_dig' => '12']
    ];
    
    public static function GetMesExtensoByNumMes(string $n_mes)
    {
        return self::$meses[$n_mes]['extenso'];
    }

    public static function GetUltimosDias($qtd_dias = 7)
    {
        $dt_atual = new \DateTime();
        $cont = 1;
        $datas = [];
        array_push($datas, $dt_atual->format('Y-m-d'));
        while ($cont < $qtd_dias) {
            $intervalo = new \DateInterval('P1D');
            $dt_atual = new \DateTime($dt_atual->sub($intervalo)->format('Y-m-d'));
            array_push($datas, $dt_atual->format('Y-m-d'));
            $cont++;
        }
        return $datas;
    }

    public static function GetDiaDaSemana($data)
    {
        $dias = [
            'Monday' => [
                'abrv' => 'Seg',
                'extenso' => 'Segunda'
            ],
            'Tuesday' => [
                'abrv' => 'Ter',
                'extenso' => 'Terça'
            ],
            'Wednesday' => [
                'abrv' => 'Qua',
                'extenso' => 'Quarta'
            ],
            'Thursday' => [
                'abrv' => 'Qui',
                'extenso' => 'Quinta'
            ],
            'Friday' => [
                'abrv' => 'Sex',
                'extenso' => 'Sexta'
            ],
            'Saturday' => [
                'abrv' => 'Sab',
                'extenso' => 'Sábado'
            ],
            'Sunday' => [
                'abrv' => 'Dom',
                'extenso' => 'Domingo'
            ]
        ];

        return $dias[\DateTime::createFromFormat('Y-m-d', $data)->format('l')];
    }
}
<?php
namespace App\Helpers;

class ListaTarefasHelper
{
    public static function CorDaLista($tarefas)
    {
        $qtd_n_cumpridas = 0;
        $qtd_cumpridas = 0;
        $qtd_ultrapassados = 0;
        $qtd_imprevistos = 0;
        $qtd_parciais = 0;
        $qtd_falhas = 0;

        foreach ($tarefas as $t){
            // echo $t->id_situacao . '<br>';
            if($t->id_situacao == 1 || $t->id_situacao == 5){
                $qtd_cumpridas += 1;
            }

            if($t->id_situacao == 2 || $t->id_situacao == 3 || $t->id_situacao == 4) {
                $qtd_n_cumpridas += 1;
            }

            if($t->id_situacao == 2){
                $qtd_falhas += 1;
            }

            if($t->id_situacao == 3){
                $qtd_imprevistos += 1;
            }

            if($t->id_situacao == 4){
                $qtd_parciais += 1;
            }

            if($t->id_situacao == 5){
                $qtd_ultrapassados += 1;
            }
        }
        if ($qtd_n_cumpridas == 0 && $qtd_parciais == 0 && $qtd_imprevistos == 0) {
            return 'success';
        }

        if ($qtd_cumpridas == 0 && $qtd_parciais == 0 && $qtd_imprevistos == 0) {
            return 'danger';
        }

        if ($qtd_n_cumpridas >= ($qtd_cumpridas + $qtd_ultrapassados)) {
            return 'danger';
        }

        if(($qtd_cumpridas + $qtd_ultrapassados) > $qtd_n_cumpridas) {
            return 'warning';
        }

    }
}
?>
<?php
namespace App\Helpers;

use GuzzleHttp\Client;

class IgdbCrawlerHelper
{
	public $url_igdb = '';
	public $igdb_json = '';
	public $igdb_crawler = '';
	public $dir_json = '';
	public $id_igdb = 0;
	public $boxarts = [];
	public $screenshots = [];
	public $artworks = [];

	public function __construct($url_igdb, $download = False)
	{
		$this->url_igdb = $url_igdb;
		$this->igdb_crawler = env('PY_IGDB_CRAWLER');
		$this->dir_json = base_path('app/dados_apis/igdb/json/');
		$this->dir_boxarts = env('DIR_GAMES_BOXARTS');
		$this->dir_fanarts = env('DIR_GAMES_FANARTS');
		$this->dir_screenshots = env('DIR_GAMES_SCREENSHOTS');
		$this->base_url_img = env('BASE_URL_IMG_IGDB');
	}

	public function GetJsonIgdb()
	{
		$this->igdb_json = json_decode(exec('python3 ' . $this->igdb_crawler . ' ' . $this->url_igdb));
	}

	public function SalvarJson()
	{
		$fp = fopen($this->dir_json . $this->igdb_json->dados->id . '.json', 'w');
		fwrite($fp, json_encode($this->igdb_json->dados));
		fclose($fp);
	}

	public function GetJsonIgdbLocal($id_igdb)
	{
		$this->id_igdb = $id_igdb;
		$this->igdb_json = json_decode(file_get_contents($this->dir_json . $id_igdb . '.json'));
	}

	public function GetBoxarts()
	{
		if(!empty($this->igdb_json->cover)){
			array_push($this->boxarts, $this->base_url_img . $this->igdb_json->cover->cloudinary_id . '.jpg');
		}
		return $this->boxarts;
	}

	public function SalvarCover($id_game)
	{
		$url_img = $this->base_url_img . $this->igdb_json->cover->cloudinary_id . '.jpg';
		$dir_boxarts = $this->dir_boxarts . $id_game . '/';
        $dir_thumbs = $this->dir_boxarts . $id_game . '/thumbs/';
        $arq = $dir_boxarts . 1 . '.jpg';
        	
        if (!is_dir($dir_boxarts)) {
	        mkdir($dir_boxarts);         
	    }
	    if (!is_dir($dir_thumbs)) {
	        mkdir($dir_thumbs);         
	    }

	    copy($url_img, $arq);
        exec('convert ' . $arq . ' -resize 120x200 ' . $dir_thumbs . 1 . '.jpg');
	}

	public function GetScreens()
	{
		if(!empty($this->igdb_json->screenshots)){
			foreach ($this->igdb_json->screenshots as $s) {
				array_push($this->screenshots, $this->base_url_img . $s->cloudinary_id . '.jpg');
			}
		}
		return $this->screenshots;
	}

	public function SalvarScreens($id_game)
	{
		$cont = 0;
		foreach ($this->screenshots as $url) {
            $cont = $cont + 1;
			$dir_screens = $this->dir_screenshots . $id_game . '/';
	        $dir_thumbs = $this->dir_screenshots . $id_game . '/thumbs/';
	        $arq = $dir_screens . $cont . '.jpg';
	        	
	        if (!is_dir($dir_screens)) {
		        mkdir($dir_screens);
		    }
		    if (!is_dir($dir_thumbs)) {
		        mkdir($dir_thumbs);         
		    }

		    copy($url, $arq);
	        exec('convert ' . $arq . ' -resize 120x200 ' . $dir_thumbs . $cont . '.jpg');
		}
	}

	public function GetArtworks()
	{
		if(!empty($this->igdb_json->artwork)){
			foreach ($this->igdb_json->artwork as $s) {
				array_push($this->artworks, $this->base_url_img . $s->cloudinary_id . '.jpg');
			}
		}
		return $this->artworks;
	}

	public function SalvarArtworks($id_game)
	{
		$cont = 0;
		foreach ($this->artworks as $url) {
            $cont = $cont + 1;
			$dir_fanarts = $this->dir_fanarts . $id_game . '/';
	        $dir_thumbs = $this->dir_fanarts . $id_game . '/thumbs/';
	        $arq = $dir_fanarts . $cont . '.jpg';
	        	
	        if (!is_dir($dir_fanarts)) {
		        mkdir($dir_fanarts);
		    }
		    if (!is_dir($dir_thumbs)) {
		        mkdir($dir_thumbs);         
		    }

		    copy($url, $arq);
	        exec('convert ' . $arq . ' -resize 120x200 ' . $dir_thumbs . $cont . '.jpg');
		}
	}
}
?>
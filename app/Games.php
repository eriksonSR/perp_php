<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use PgUtils;

class Games extends Model
{
    protected $table = 'tb_games';

    public function Console(){
        return $this->belongsTo('App\Consoles', 'id_console');
    }

    public static function Listar($filtros = null)
    {
        $filtros_str = '';
        $parametros = [];
        if (!empty($filtros)) {
            $filtros_str = ' WHERE 0 = 0 ';

            if(!empty($filtros['titulo'])){
                $filtros_str = $filtros_str . " AND titulo ILIKE '%" . $filtros['titulo'] . "%'";
            }
            
            if(!empty($filtros['ids_consoles'])){
                $filtros_str .= ' AND g.id_console IN (';
                $cont = 1;
                //Para cada console, adiciona um parâmetro nomeado
                foreach($filtros['ids_consoles'] as $c){
                    $filtros_str .= ':console' . $cont . ',';
                    $parametros['console'.$cont] = $c;
                    $cont++;
                }
                //Remove a última vírgula e fecha o IN
                $filtros_str = substr($filtros_str, 0, -1);
                $filtros_str .= ')';
            }
            
            if(!empty($filtros['ids_plataformas'])){
                $filtros_str .= ' AND c.id_plataforma IN (';
                $cont = 1;
                //Para cada plataforma, adiciona um parâmetro nomeado
                foreach($filtros['ids_plataformas'] as $c){
                    $filtros_str .= ':plataforma' . $cont . ',';
                    $parametros['plataforma'.$cont] = $c;
                    $cont++;
                }
                //Remove a última vírgula e fecha o IN
                $filtros_str = substr($filtros_str, 0, -1);
                $filtros_str .= ')';
            }

            if(!empty($filtros['ids_generos'])){
                $filtros_str .= ' AND g.ids_generos && ARRAY[' . implode(',', $filtros['ids_generos']) . ']';
            }

            if(!empty($filtros['id_situacao'])){
                $filtros_str .= ' AND g.id_situacao = :id_situacao';
                $parametros['id_situacao'] = $filtros['id_situacao'];
            }

            if(!empty($filtros['ids_publicadoras'])){
                $filtros_str .= ' AND g.ids_publicadoras && ARRAY[' . implode(',', $filtros['ids_publicadoras']) . ']';
            }

            if(!empty($filtros['ids_desenvolvedoras'])){
                $filtros_str .= ' AND g.ids_desenvolvedoras && ARRAY[' . implode(',', $filtros['ids_desenvolvedoras']) . ']';
            }

            if(!empty($filtros['data_ini'])){
                $filtros_str = $filtros_str . ' AND dt_finalizacao ' . $filtros['criterio_ini_dt_finalizacao'] . ' :data_ini';
                $parametros['data_ini'] = PgUtils::DataBrToPg($filtros['data_ini'], '/');
            }

            if(!empty($filtros['data_final'])){
                $filtros_str = $filtros_str . ' AND dt_finalizacao ' . $filtros['criterio_final_dt_finalizacao'] . ' :data_final';
                $parametros['data_final'] = PgUtils::DataBrToPg($filtros['data_final'], '/');
            }

            if(!empty($filtros['dlc'])){
                $filtros_str = $filtros_str . ' AND dlc = ' . $filtros['dlc'];
            }

            if(!empty($filtros['digital'])){
                $filtros_str = $filtros_str . ' AND digital = 1';
            }

            if(!empty($filtros['tenho'])){
                $filtros_str = $filtros_str . ' AND tenho = 1';
            }

            if(!empty($filtros['ano'])){
                $filtros_str = $filtros_str . ' AND to_char(g.dt_finalizacao, \'YYYY\') = \'' . $filtros['ano'] . '\'';
            }

            if(!empty($filtros['console_text'])){
                $filtros_str = $filtros_str . ' AND c.console = \'' . $filtros['console_text'] . '\'';
            }

            if(!empty($filtros['mes'])){
                $filtros_str = $filtros_str . ' AND to_char(g.dt_finalizacao, \'MM\') = \'' . $filtros['mes'] . '\'';
            }

            if(isset($filtros['portatil'])){
                $filtros_str = $filtros_str . ' AND c.portatil = ' . $filtros['portatil'];
            }
        }

        $select = "
            SELECT 
                g.id, 
                g.titulo, 
                c.console, 
                g.ids_publicadoras, 
                g.digital
            FROM tb_games g
            INNER JOIN tb_consoles c ON c.id = g.id_console
            INNER JOIN tb_plataformas p ON p.id = c.id_plataforma
            $filtros_str
            ORDER BY g.dt_finalizacao DESC";
        return DB::select($select, $parametros);
    }

    public static function ExportarTabela()
    {
        $select = "
            SELECT 
                g.id, 
                g.id_igdb,
                g.id_gamesdb,
                c.console,
                c.portatil,
                g.titulo,
                pub.studio pub,
                dev.studio dev,
                gen.genero,
                g.digital,
                to_char(dt_finalizacao, 'dd/mm/yyyy') dt_finalizacao,
                to_char(dt_release, 'dd/mm/yyyy') dt_release
            FROM tb_games g
            INNER JOIN tb_consoles c ON c.id = g.id_console
            INNER JOIN tb_plataformas p ON p.id = c.id_plataforma
            INNER JOIN tb_studios pub ON pub.id = ANY(g.ids_publicadoras)
            INNER JOIN tb_studios dev ON dev.id = ANY(g.ids_desenvolvedoras)
            INNER JOIN tb_generos gen ON gen.id = ANY(g.ids_generos)
            ORDER BY g.dt_finalizacao DESC
        ";

        return DB::select($select);
    }

    public static function BuscaGeral($termo)
    {
        $select = "
            SELECT 
                g.id, 
                g.titulo, 
                c.console
            FROM tb_games g
            INNER JOIN tb_consoles c ON c.id = g.id_console
            WHERE g.titulo  ILIKE '%{$termo}%'
            OR c.console  ILIKE '%{$termo}%'
            ORDER BY g.titulo";
        return DB::select($select);
    }

    public function Origem(){
        return $this->belongsTo('App\OrigemGames', 'id_origem');
    }
}
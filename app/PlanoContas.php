<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PlanoContas extends Model
{
	protected $table = 'tb_plano_contas';

	public function GetContasBase()
	{
		$colunas = [
			'id',
			'id_conta_mae',
			'analitica',
			'conta',
			'tipo',
			'grau'
		];
		$contas = DB::table($this->table)->select($colunas)->whereNull('id_conta_mae')->orderBy('grau')->get();
		return $contas;
	}

	public function GetContasFilhas($id_conta_mae)
	{
		$colunas = [
			'id',
			'id_conta_mae',
			'analitica',
			'conta',
			'tipo',
			'grau'
		];
		$contas = DB::table($this->table)->select($colunas)->where('id_conta_mae', $id_conta_mae)->orderBy('grau')->get();
		return $contas;
	}

	public function GetContasAninhadas()
	{
		$contas = $this->GetContasBase();
        foreach ($contas as $c) {
            if ($c->analitica == 0){
                $this->AninhaContas($c);
            }
        }

        return $contas;
	}

    public function AninhaContas(&$conta){
        $fila = [];
        $conta->filhas = $this->GetContasFilhas($conta->id);
        array_push($fila, $conta->filhas);

        while (!empty($fila)){
            $no = array_shift($fila);
            foreach ($no as $v) {
                if($v->analitica == 0){
                    $this->AninhaContas($v);
                }
            }
        }
    }

    public function GetListaContasComGraduacao()
    {
        $contas = $this->GetContasAninhadas();
        $lista = [];
        foreach ($contas as $v) {
            array_push($lista, ['id' => $v->id, 'grau' => $v->grau, 'conta' => $v->conta, 'tipo' => $v->tipo, 'analitica' => $v->analitica]);
            if(isset($v->filhas)){
                $ca = $this->MontaArrayContasComGraduacao($v->filhas, [$v->grau], $lista);
            }
        }

        return $lista;
    }

    protected function MontaArrayContasComGraduacao($contas, $graduacao, &$lista)
    {
        $gs = $graduacao;
        foreach ($contas as $v) {
            array_push($lista, ['id' => $v->id, 'grau' => implode('.', $gs) . '.' . $v->grau, 'conta' => $v->conta, 'tipo' => $v->tipo, 'analitica' => $v->analitica]);
            if(isset($v->filhas)){
                array_push($gs, $v->grau);
                $this->MontaArrayContasComGraduacao($v->filhas, $gs, $lista);
            }
        }
    }
}
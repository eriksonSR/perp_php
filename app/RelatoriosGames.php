<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RelatoriosGames extends Model
{
	public $timestamps = false;
	protected $table = 'tb_games';

	public static function DlcVsJogoBase()
	{
		$dados = DB::select('
			SELECT 
			    CASE 
			        WHEN dlc = 1 THEN \'DLC\'
			        ELSE
			            \'Base\'
			    END AS tipo,
			    COUNT(*) total
			FROM tb_games
			WHERE id_situacao = 2
			GROUP BY tipo
			ORDER BY tipo
		');
		return $dados;
	}

	public static function ConsolidadoPorAno($ano = null)
	{
		$parametros = [];
		$filtros = 'WHERE g.id_situacao = 2 ';
		if(isset($ano)){
			$parametros['ano'] = $ano;
			$filtros .= 'AND to_char(g.dt_finalizacao, \'YYYY\') = :ano';
		}

		$select = "
			SELECT 
				to_char(g.dt_finalizacao, 'YYYY') ano, count(*) total
			FROM tb_games g
			$filtros
			GROUP BY ano
			ORDER BY ano
		";
		$dados = DB::select($select, $parametros);
		return $dados;
	}

	public static function ConsolidadoPorConsoleAoAno($ano = null, $consoles = null)
	{
		$parametros = [];
		$filtros = 'WHERE g.id_situacao = 2 ';
		if(isset($ano)){
			$parametros['ano'] = $ano;
			$filtros .= ' AND to_char(g.dt_finalizacao, \'YYYY\') = :ano';
		}
		if(isset($consoles)){
			$filtros .= ' AND c.id IN (';
			$cont = 1;
			//Para cada console, adiciona um parâmetro nomeado
			foreach($consoles as $c){
				$filtros .= ':console' . $cont . ',';
				$parametros['console'.$cont] = $c;
				$cont++;
			}
			//Remove a última vírgula e fecha o IN
			$filtros = substr($filtros, 0, -1);
			$filtros .= ')';
		}

		$select = "
			SELECT 
				c.console, to_char(g.dt_finalizacao, 'YYYY') ano, count(*) total
			FROM tb_consoles c
			INNER JOIN tb_games g ON g.id_console = c.id
			$filtros
			GROUP BY c.console, ano
			ORDER BY ano, c.console
			";
		return DB::select($select, $parametros);
	}

	public static function ConsolidadoPorDesenvolvedora($desenvolvedoras = null)
	{
		$parametros = [];
		$filtros = 'WHERE g.id_situacao = 2 ';
		if(isset($desenvolvedoras)){
			$filtros .= 'AND s.id IN (';
			$cont = 1;
			//Para cada desenvolvedora, adiciona um parâmetro nomeado
			foreach($desenvolvedoras as $d){
				$filtros .= ':desenvolvedora' . $cont . ',';
				$parametros['desenvolvedora'.$cont] = $d;
				$cont++;
			}
			//Remove a última vírgula e fecha o IN
			$filtros = substr($filtros, 0, -1);
			$filtros .= ')';
		}
		$select = "
			SELECT 
				s.studio desenvolvedora, count(*) total
			FROM tb_games g
			INNER JOIN tb_studios s ON s.id = ANY(g.ids_desenvolvedoras)
			$filtros
			GROUP BY s.studio
			ORDER BY s.studio
			";
		return DB::select($select, $parametros);
	}

	public static function ConsolidadoPorGeneroAoAno($ano = null, $generos = null)
	{
		$parametros = [];
		$filtros = 'WHERE g.id_situacao = 2 ';
		if(isset($ano)){
			$parametros['ano'] = $ano;
			$filtros .= ' AND to_char(g.dt_finalizacao, \'YYYY\') = :ano';
		}
		if(isset($generos)){
			$filtros .= ' AND gn.id IN (';
			$cont = 1;
			//Para cada gênero, adiciona um parâmetro nomeado
			foreach($generos as $g){
				$filtros .= ':genero' . $cont . ',';
				$parametros['genero'.$cont] = $g;
				$cont++;
			}
			//Remove a última vírgula e fecha o IN
			$filtros = substr($filtros, 0, -1);
			$filtros .= ')';
		}

		$select = "
			SELECT 
				gn.genero, count(*) total, to_char(g.dt_finalizacao, 'YYYY') ano
			FROM tb_games g
			INNER JOIN tb_generos gn ON gn.id = ANY(g.ids_generos)
			$filtros
			GROUP BY ano, genero
			ORDER BY ano, genero ASC
			";
		return DB::select($select, $parametros);
	}

	public static function ConsolidadoPorMes($ano = null)
	{
		$parametros = [];
		$filtros = 'WHERE g.id_situacao = 2 ';
		if(isset($ano)){
			$parametros['ano'] = $ano;
			$filtros .= 'AND to_char(g.dt_finalizacao, \'YYYY\') = :ano';
		}

		$select = "
			SELECT 
				to_char(dt_finalizacao, 'MM') mes, to_char(dt_finalizacao, 'YYYY') ano, count(*) total
			FROM tb_games g
			$filtros
			GROUP BY 2, 1
			ORDER BY ano, mes
		";
		return DB::select($select, $parametros);
	}

	public static function ConsolidadoPorPlataformaAoAno($ano = null, $plataformas = null)
	{
		$parametros = [];
		$filtros = 'WHERE g.id_situacao = 2 ';
		if(isset($ano)){
			$parametros['ano'] = $ano;
			$filtros .= ' AND to_char(g.dt_finalizacao, \'YYYY\') = :ano';
		}
		if(isset($plataformas)){
			$filtros .= ' AND p.id IN (';
			$cont = 1;
			//Para cada plataforma, adiciona um parâmetro nomeado
			foreach($plataformas as $p){
				$filtros .= ':plataforma' . $cont . ',';
				$parametros['plataforma'.$cont] = $p;
				$cont++;
			}
			//Remove a última vírgula e fecha o IN
			$filtros = substr($filtros, 0, -1);
			$filtros .= ')';
		}
		$select = "
			SELECT 
				p.plataforma, to_char(g.dt_finalizacao, 'YYYY') ano, count(p.id) total
			FROM tb_plataformas p
			INNER JOIN tb_consoles c ON c.id_plataforma = p.id
			INNER JOIN tb_games g ON g.id_console = c.id
			$filtros
			GROUP BY p.plataforma, ano
			ORDER BY ano, plataforma ASC
		";
		return DB::select($select, $parametros);
	}

	public static function ConsolidadoPorPublicadoraAoAno($ano = null, $publicadoras = null)
	{
		$parametros = [];
		$filtros = 'WHERE g.id_situacao = 2 ';
		if(isset($ano)){
			$parametros['ano'] = $ano;
			$filtros .= ' AND to_char(g.dt_finalizacao, \'YYYY\') = :ano';
		}
		if(isset($publicadoras)){
			$filtros .= ' AND s.id IN (';
			$cont = 1;
			//Para cada publicadora, adiciona um parâmetro nomeado
			foreach($publicadoras as $p){
				$filtros .= ':publicadora' . $cont . ',';
				$parametros['publicadora'.$cont] = $p;
				$cont++;
			}
			//Remove a última vírgula e fecha o IN
			$filtros = substr($filtros, 0, -1);
			$filtros .= ')';
		}
		$select = "
			SELECT 
				s.studio publicadora, to_char(g.dt_finalizacao, 'YYYY') ano, count(*) total
			FROM tb_games g
			INNER JOIN tb_studios s ON s.id = ANY(g.ids_publicadoras)
			$filtros
			GROUP BY ano, publicadora
			ORDER BY ano, publicadora DESC
		";
		return DB::select($select, $parametros);
	}

	public static function ConsolidadoPortatilVsMesaAoAno($ano = null)
	{
		$parametros = [];
		$filtros = 'WHERE g.id_situacao = 2 ';
		if(isset($ano)){
			$parametros['ano'] = $ano;
			$filtros .= 'AND to_char(g.dt_finalizacao, \'YYYY\') = :ano';
		}
		$select = "
			SELECT
				to_char(g.dt_finalizacao, 'YYYY') ano,
			    CASE 
			        WHEN c.portatil = 1 THEN 'Portátil'
			        ELSE
			            'Mesa'
			    END AS tipo,
			    COUNT(*) total
			FROM tb_games g
			INNER JOIN tb_consoles c ON c.id = g.id_console
			$filtros
			GROUP BY tipo, ano
			ORDER BY ano
		";
		return DB::select($select, $parametros);
	}

	public static function QtdGamesNaoIniciados()
	{
		$select = "
			SELECT count(*) tot FROM tb_games
			WHERE id_situacao = 3
		";
		return DB::select($select);
	}

	public static function QtdGamesFinalizados()
	{
		$select = "
			SELECT count(*) tot FROM tb_games
			WHERE id_situacao = 2
		";
		return DB::select($select);
	}

	public static function UltimosGamesFinalizados()
	{
		$select = "
			SELECT titulo, dt_finalizacao
			FROM tb_games
			WHERE id_situacao = 2
			ORDER BY dt_finalizacao DESC
			LIMIT 5
		";
		return DB::select($select);
	}

	public static function ConsolesMaisJogadosNoAno($ano)
	{
		$select = "
			SELECT c.console, count(*) total
			FROM tb_games g
			INNER JOIN tb_consoles c ON g.id_console = c.id
			WHERE id_situacao = 2
			AND to_char(g.dt_finalizacao, 'YYYY') = '{$ano}'
			GROUP BY console
			ORDER BY total desc
			LIMIT 3
		";
		return DB::select($select);
	}
}
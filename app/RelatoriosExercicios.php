<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RelatoriosExercicios extends Model
{
    public static function DistanciasPorPeriodo($mes = null, $ano = null)
    {
    	$filtros = 'WHERE 0 = 0';
    	if(isset($mes)) {
   			$filtros .= ' AND EXTRACT(month FROM tbe.data) = ' . $mes;
    	}
    	if(isset($ano)) {
   			$filtros .= ' AND EXTRACT(year FROM tbe.data) = ' . $ano;
    	}

    	$select = "
			SELECT to_char(tbe.data, 'MM') mes, tbte.tipo, SUM(tbe.distancia) distancia
			FROM tb_exercicios tbe
			INNER JOIN tb_tipos_exercicio tbte ON tbte.id = tbe.id_tipo_exercicio
			$filtros
			GROUP BY mes, tipo
			ORDER BY mes
    	";

    	$dados = DB::select($select);
    	return $dados;
    }

    public static function DistanciaTotalPorExercicioAoAno($ano = null)
    {
    	$filtros = '';
    	if(isset($ano)) {
   			$filtros .= 'WHERE EXTRACT(year FROM tbe.data) = ' . $ano;
    	}

    	$select = "
    		SELECT to_char(tbe.data, 'YYYY') ano, tbte.tipo, SUM(tbe.distancia) distancia
			FROM tb_exercicios tbe
			INNER JOIN tb_tipos_exercicio tbte ON tbte.id = tbe.id_tipo_exercicio
			$filtros
			GROUP BY ano, tipo
			ORDER BY ano
    	";

    	$dados = DB::select($select);
    	return $dados;
    }
}

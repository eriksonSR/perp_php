<?php

namespace App\Apis;
use GuzzleHttp\Client;

class Gdb
{
	public $id_gdb;
	public $api_key = 'fb8e1fbf2d63928bcf80ef498fc1cd5bfbc20b1bc99a8cb196db9e65d1549343';
	public $url_api = 'https://api.thegamesdb.net/';
	public $url_img = 'https://cdn.thegamesdb.net/images/large/';
	public $url_request;
	public $fields = 'Games/Images?&filter[type]=boxart,fanart,screenshot';
	public $dir_json;
	public $download;

	public function __construct($id_gdb, $download = False)
	{
		$this->id_gdb = $id_gdb;
		$this->dir_json = base_path('app/dados_apis/gamesdb/json/');
		$this->url_request = $this->url_api . $this->fields . '&games_id=' . $this->id_gdb . '&apikey=' . $this->api_key;

		if ($download) {
			$this->SalvaJson();
		}
	}

	public function SalvaJson()
	{
		$guzz = new Client();
		$resposta = $guzz->request('GET', $this->url_request);
		$conteudo = $resposta->getBody()->getContents();

		$fp = fopen($this->dir_json . $this->id_gdb . '.json', 'w');
		fwrite($fp, $conteudo);
		fclose($fp);
	}

	public function GetJson()
	{
		return json_decode(file_get_contents($this->dir_json . $this->id_gdb . '.json'))->data;
	}

	public function GetArtworks()
	{
		$json = json_decode(file_get_contents($this->dir_json . $this->id_gdb . '.json'))->data;
		$artworks = [];

		foreach ($json->images as $id_gdb => $images) {
			foreach ($images as $v) {
				if($v->type == 'fanart'){
					array_push($artworks, $this->url_img . $v->filename);
				}
			}
		}
		return $artworks;
	}

	public function GetScreenshots()
	{
		$json = json_decode(file_get_contents($this->dir_json . $this->id_gdb . '.json'))->data;
		$screenshots = [];

		foreach ($json->images as $id_gdb => $images) {
			foreach ($images as $v) {
				if($v->type == 'screenshot'){
					array_push($screenshots, $this->url_img . $v->filename);
				}
			}
		}
		return $screenshots;
	}

	public function GetCover()
	{
		$json = json_decode(file_get_contents($this->dir_json . $this->id_gdb . '.json'))->data;

		foreach ($json->images as $id_gdb => $images) {
			foreach ($images as $v) {
				if($v->type == 'boxart' && $v->side == 'front'){
					return $this->url_img . $v->filename;
				}
			}
		}
	}
}
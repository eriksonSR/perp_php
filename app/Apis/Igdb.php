<?php

namespace App\Apis;
use GuzzleHttp\Client;

class Igdb
{
	public $id_igdb;
	public $api_headers = [
		'User-Key' => 'b1624d2d0066cb8afcb9a330a170fd8e'
	];
	public $url_api = 'https://api-v3.igdb.com/games/';
	public $url_img = 'https://images.igdb.com/igdb/image/upload/t_1080p/';
	public $url_request;
	public $fields = '?fields=id,name,url,summary,dlcs.*,release_dates.*,websites.*,alternative_names.*,screenshots.*,artworks.*,videos.*,cover.*,websites.*';
	public $dir_json;
	public $download;

	public function __construct($id_igdb, $download = False)
	{
		$this->id_igdb = $id_igdb;
		$this->dir_json = base_path('app/dados_apis/igdb/json/');
		$this->url_request = $this->url_api . $this->id_igdb . '/' . $this->fields;

		if ($download) {
			$this->SalvaJson();
		}
	}

	public function SalvaJson()
	{
		$guzz = new Client();
		$resposta = $guzz->request('GET', $this->url_request, ['headers' => $this->api_headers]);
		$conteudo = $resposta->getBody()->getContents();

		$fp = fopen($this->dir_json . $this->id_igdb . '.json', 'w');
		fwrite($fp, $conteudo);
		fclose($fp);
	}

	public function GetJson()
	{
		return json_decode(file_get_contents($this->dir_json . $this->id_igdb . '.json'))[0];
	}

	public function GetArtworks()
	{
		$json = json_decode(file_get_contents($this->dir_json . $this->id_igdb . '.json'))[0];
		$artworks = [];
		if(!empty($json->artworks)){
			foreach ($json->artworks as $v) {
				$ext = explode('/', $v->url);
				$ext = explode('.', end($ext))[1];
				array_push($artworks, $this->url_img . $v->image_id . '.' . $ext);
			}
		}
		return $artworks;
	}

	public function GetScreenshots()
	{
		$json = json_decode(file_get_contents($this->dir_json . $this->id_igdb . '.json'))[0];
		$screenshots = [];
		if(!empty($json->screenshots)){
			foreach ($json->screenshots as $v) {
				$ext = explode('/', $v->url);
				$ext = explode('.', end($ext))[1];
				array_push($screenshots, $this->url_img . $v->image_id . '.' . $ext);
			}
		}
		return $screenshots;
	}

	public function GetCover()
	{
		$json = json_decode(file_get_contents($this->dir_json . $this->id_igdb . '.json'))[0];
		if (isset($json->cover->url)) {
			$ext = explode('/', $json->cover->url);
			$ext = explode('.', end($ext))[1];
			return $this->url_img . $json->cover->image_id . '.' . $ext;
		}else{
			return '';
		}
	}

	public function GetVideos()
	{
		
	}
}
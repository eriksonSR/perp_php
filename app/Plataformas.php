<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plataformas extends Model
{
    public $timestamps = false;
	protected $table = 'tb_plataformas';
}

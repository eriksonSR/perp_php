<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListasTarefas extends Model
{
	protected $table = 'tb_listas_tarefas';
    public $timestamps = false;
}

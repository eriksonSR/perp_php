<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaTarefa extends Model
{
	protected $table = 'tb_categorias_tarefas';

	public function getDateFormat()
	{
		return 'Y-m-d H:i:s.u';
	}
}

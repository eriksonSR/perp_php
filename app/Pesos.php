<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesos extends Model
{
	protected $table = 'tb_pesos';
    public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SituacoesTarefas extends Model
{
    protected $table = 'tb_situacoes_tarefas';

	public function getDateFormat()
	{
		return 'Y-m-d H:i:s.u';
	}
}

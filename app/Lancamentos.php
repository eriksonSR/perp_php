<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Lancamentos extends Model
{
	protected $table = 'tb_lancamentos';

	public function Listar($filtros = null, $qtd_results=null)
	{
		$filtros_str = '';
		$parametros = [];

		if (!empty($filtros)) {
			$filtros_str = ' WHERE 0 = 0 ';
			if(!empty($filtros['conta_cred'])){
				$filtros_str = $filtros_str . ' AND id_conta_creditada = :conta_cred';
				$parametros['conta_cred'] = $filtros['conta_cred'];
			}

			if(!empty($filtros['conta_deb'])){
				$filtros_str = $filtros_str . ' AND id_conta_debitada = :conta_deb';
				$parametros['conta_deb'] = $filtros['conta_deb'];
			}

			if(!empty($filtros['historico'])){
				$filtros_str = $filtros_str . " AND historico ILIKE '%" . $filtros['historico'] . "%'";
			}

			if(!empty($filtros['valor'])){
				$valor = str_replace('.', '', $filtros['valor']);
				$valor = str_replace(',', '.', $valor);
				$filtros_str = $filtros_str . ' AND valor ' . $filtros['criterio_valor'] . ' :valor';
				$parametros['valor'] = $valor;
			}

			if(!empty($filtros['data_ini'])){
				$dt = explode('/', $filtros['data_ini']);
				$dt = $dt['2'] . '-' . $dt['1'] . '-' . $dt['0'];
				$filtros_str = $filtros_str . ' AND data ' . $filtros['criterio_data_ini'] . ' :data_ini';
				$parametros['data_ini'] = $dt;
			}

			if(!empty($filtros['data_fim'])){
				$dt = explode('/', $filtros['data_fim']);
				$dt = $dt['2'] . '-' . $dt['1'] . '-' . $dt['0'];
				$filtros_str = $filtros_str . ' AND data ' . $filtros['criterio_data_fim'] . ' :data_fim';
				$parametros['data_fim'] = $dt;
			}

			if(!empty($filtros['mes'])){
				$filtros_str = $filtros_str . ' AND to_char(data, \'MM\') = \'' . $filtros['mes'] . '\'';
			}

			if(!empty($filtros['ano'])){
				$filtros_str = $filtros_str . ' AND to_char(data, \'YYYY\') = \'' . $filtros['ano'] . '\'';
			}
		}
		$select = "
			SELECT
    			tl.id, 
    			id_conta_creditada, 
    			id_conta_debitada, 
    			tcc.conta c_cre, 
    			tcd.conta c_deb, 
    			historico, 
    			valor,
    			to_char(data, 'DD/MM/YYYY') dt
			FROM tb_lancamentos tl
			INNER JOIN tb_plano_contas tcc on tl.id_conta_creditada = tcc.id
			INNER JOIN tb_plano_contas tcd on tl.id_conta_debitada = tcd.id
			$filtros_str
			ORDER BY tl.data DESC";
		if ($qtd_results) {
			$select .= 'LIMIT ' . $qtd_results;
		}
		return DB::select($select, $parametros);
	}

	public function ContaCreditada()
    {
        return $this->belongsTo('App\PlanoContas', 'id_conta_creditada');
    }

	public function ContaDebitada()
    {
        return $this->belongsTo('App\PlanoContas', 'id_conta_debitada');
    }

	public static function PrincipaisGastos($ano)
	{
		$select = "
			SELECT cc.conta, sum(tl.valor) valor 
			FROM tb_lancamentos tl
			INNER JOIN tb_plano_contas cc ON cc.id = tl.id_conta_creditada
			WHERE cc.analitica = 1 
			AND cc.tipo = 'd'
			AND to_char(tl.data, 'YYYY') = '{$ano}'
			GROUP BY conta
			ORDER BY valor DESC
			LIMIT 5
		";
		return DB::select($select);
	}

	public static function BuscaGeral($termo)
	{		
		$select = "
			SELECT
    			tl.id, 
    			id_conta_creditada, 
    			id_conta_debitada, 
    			tcc.conta c_cre, 
    			tcd.conta c_deb, 
    			historico, 
    			valor,
    			to_char(data, 'DD/MM/YYYY') dt
			FROM tb_lancamentos tl
			INNER JOIN tb_plano_contas tcc on tl.id_conta_creditada = tcc.id
			INNER JOIN tb_plano_contas tcd on tl.id_conta_debitada = tcd.id
			WHERE historico ILIKE '%$termo%'
			OR tcc.conta ILIKE '%$termo%'
			OR tcd.conta ILIKE '%$termo%'
			ORDER BY tl.data DESC";

		return DB::select($select);
	}
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrigemGames extends Model
{
    public $timestamps = false;
	protected $table = 'tb_origem_games';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiasSemana extends Model
{
	protected $table = 'tb_dias_semana';
}

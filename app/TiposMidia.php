<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposMidia extends Model
{
    public $timestamps = false;
	protected $table = 'tb_tipos_midias';
}

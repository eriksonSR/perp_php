<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class RelatoriosContabeis extends Model
{
    public static function ConsolidadoPorContaNoPeriodo($filtros = null)
    {
    	$parametros = [];
    	$filtros_str = '';
    	if(isset($filtros['ano'])){
    		$parametros['ano'] = $filtros['ano'];
    		$filtros_str .= ' AND to_char(tl.data, \'YYYY\') = :ano';
    	}
		if(isset($filtros['ids_contas'])){
			$filtros_str .= ' AND tl.id_conta_creditada IN (';
			$cont = 1;
			//Para cada conta, adiciona um parâmetro nomeado
			foreach($filtros['ids_contas'] as $c){
				$filtros_str .= ':conta' . $cont . ',';
				$parametros['conta' . $cont] = $c;
				$cont++;
			}
			//Remove a última vírgula e fecha o IN
			$filtros_str = substr($filtros_str, 0, -1);
			$filtros_str .= ')';
		}

    	$select = "
    		SELECT 
    			cc.id,
    			cc.conta, 
    			to_char(tl.data, 'YYYY') ano, 
    			to_char(tl.data, 'MM') mes, 
    			sum(tl.valor) valor 
    		FROM tb_lancamentos tl
			INNER JOIN tb_plano_contas cc ON cc.id = tl.id_conta_creditada
			WHERE cc.analitica = 1
			AND cc.tipo = 'd'
			$filtros_str
			GROUP BY cc.id, conta, ano, mes
			ORDER BY ano DESC, mes DESC, valor DESC
			";
		return DB::select($select, $parametros);
    }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('listar_autores', 'AutoresController@ListarJson');
Route::get('listar_editoras', 'EditorasController@ListarJson');
Route::get('listar_assuntos', 'AssuntosController@ListarJson');
Route::get('listar_tipos_midias', 'TiposMidiasController@ListarJson');
Route::get('listar_situacoes', 'SituacoesController@ListarJson');
Route::get('listar_idiomas', 'IdiomasController@ListarJson');
Route::get('listar_leituras', 'LeiturasController@ListarJson');
Route::get('listar_consoles', 'ConsolesController@ListarJson');
Route::get('listar_plataformas', 'PlataformasController@ListarJson');
Route::get('listar_generos', 'GenerosController@ListarJson');
Route::get('listar_origem_games', 'OrigemGamesController@ListarJson');
Route::get('listar_studios', 'StudiosController@ListarJson');
Route::get('listar_games', 'GamesController@ListarJson');
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'relatorios', 'middleware' => 'auth'], function(){
	Route::group(['prefix' => 'games'], function(){
		Route::get('dlc_vs_jogo_base', 'RelatoriosGamesController@DlcVsJogoBase')->name('games.dlc_vs_jogo_base');
		Route::get('consolidado_por_ano', 'RelatoriosGamesController@ConsolidadoPorAno')->name('games.consolidado_por_ano');
		Route::post('consolidado_por_ano_json', 'RelatoriosGamesController@ConsolidadoPorAnoJson');
		Route::get('consolidado_por_console_ao_ano', 'RelatoriosGamesController@ConsolidadoPorConsoleAoAno')->name('games.consolidado_por_console_ao_ano');
		Route::post('consolidado_por_console_ao_ano_json', 'RelatoriosGamesController@ConsolidadoPorConsoleAoAnoJson');
		Route::get('consolidado_por_desenvolvedora', 'RelatoriosGamesController@ConsolidadoPorDesenvolvedora')->name('games.consolidado_por_desenvolvedora');
		Route::post('consolidado_por_desenvolvedora_json', 'RelatoriosGamesController@ConsolidadoPorDesenvolvedoraJson');
		Route::get('consolidado_por_genero_ao_ano', 'RelatoriosGamesController@ConsolidadoPorGeneroAoAno')->name('games.consolidado_por_genero_ao_ano');
		Route::post('consolidado_por_genero_ao_ano_json', 'RelatoriosGamesController@ConsolidadoPorGeneroAoAnoJson');
		Route::get('consolidado_por_mes', 'RelatoriosGamesController@ConsolidadoPorMes')->name('games.consolidado_por_mes');
		Route::post('consolidado_por_mes_json', 'RelatoriosGamesController@ConsolidadoPorMesJson');
		Route::get('consolidado_por_plataforma_ao_ano', 'RelatoriosGamesController@ConsolidadoPorPlataformaAoAno')->name('games.consolidado_por_plataforma_ao_ano');
		Route::post('consolidado_por_plataforma_ao_ano_json', 'RelatoriosGamesController@ConsolidadoPorPlataformaAoAnoJson');
		Route::get('consolidado_por_publicadora_ao_ano', 'RelatoriosGamesController@ConsolidadoPorPublicadoraAoAno')->name('games.consolidado_por_publicadora_ao_ano');
		Route::post('consolidado_por_publicadora_ao_ano_json', 'RelatoriosGamesController@ConsolidadoPorPublicadoraAoAnoJson');
		Route::get('consolidado_por_portatil_vs_mesa_ao_ano', 'RelatoriosGamesController@ConsolidadoPortatilVsMesaAoAno')->name('games.consolidado_por_portatil_vs_mesa_ao_ano');
		Route::post('consolidado_portatil_vs_mesa_ao_ano_json', 'RelatoriosGamesController@ConsolidadoPortatilVsMesaAoAnoJson');
	});

	Route::group(['prefix' => 'leituras'], function(){
		Route::get('consolidado_por_ano', 'RelatoriosLeiturasController@ConsolidadoPorAno')->name('leituras.consolidado_por_ano');
		Route::post('consolidado_por_ano_json', 'RelatoriosLeiturasController@ConsolidadoPorAnoJson');
		Route::get('consolidado_por_assunto_ao_ano', 'RelatoriosLeiturasController@ConsolidadoPorAssuntoAoAno')->name('leituras.consolidado_por_assunto_ao_ano');
		Route::post('consolidado_por_assunto_ao_ano_json', 'RelatoriosLeiturasController@ConsolidadoPorAssuntoAoAnoJson');
		Route::get('consolidado_por_autor', 'RelatoriosLeiturasController@ConsolidadoPorAutor')->name('leituras.consolidado_por_autor');
		Route::post('consolidado_por_autor_json', 'RelatoriosLeiturasController@ConsolidadoPorAutorJson');
		Route::get('consolidado_por_editora', 'RelatoriosLeiturasController@ConsolidadoPorEditora')->name('leituras.consolidado_por_editora');
		Route::post('consolidado_por_editora_json', 'RelatoriosLeiturasController@ConsolidadoPorEditoraJson');
		Route::get('consolidado_por_formato_ao_ano', 'RelatoriosLeiturasController@ConsolidadoPorFormatoAoAno')->name('leituras.consolidado_por_formato_ao_ano');
		Route::post('consolidado_por_formato_ao_ano_json', 'RelatoriosLeiturasController@ConsolidadoPorFormatoAoAnoJson');
		Route::get('consolidado_por_mes', 'RelatoriosLeiturasController@ConsolidadoPorMes')->name('leituras.consolidado_por_mes');
		Route::post('consolidado_por_mes_json', 'RelatoriosLeiturasController@ConsolidadoPorMesJson');
		Route::get('consolidado_por_tipo_midia_ao_mes', 'RelatoriosLeiturasController@ConsolidadoPorTipoMidiaAoMes')->name('leituras.consolidado_por_tipo_midia_ao_mes');
		Route::post('consolidado_por_tipo_midia_ao_mes_json', 'RelatoriosLeiturasController@ConsolidadoPorTipoMidiaAoMesJson');
	});

	Route::group(['prefix' => 'contabil'], function(){
		Route::get('gastos_p_mes_grafico', 'RelatoriosController@GraficoGastosPorMes')->name('graficos.gastos_p_mes');
		Route::get('gastos_p_conta_e_periodo', 'RelatoriosContabilController@GastosPorContaPeriodo')->name('relatorios.gastos_conta_periodo');
		Route::post('gastos_p_conta_e_periodo_json', 'RelatoriosContabilController@GastosPorContaPeriodoJson');
	});

	Route::get('games_e_leituras_por_mes', 'RelatoriosController@GraficoGamesEleiturasPorMes')->name('graficos.games_e_leituras_por_mes');
	Route::post('games_e_leituras_por_mes_json', 'RelatoriosController@GamesEleiturasPorMesJson');
	Route::get('distancia_por_periodo', 'RelatoriosController@DistanciasPorPeriodoJson');
});

Route::group(['prefix' => 'contabil', 'middleware' => 'auth'], function(){
	Route::group(['prefix' => 'plano_contas'], function(){
		Route::get('cadastrar', 'PlanoContasController@Cadastrar')->name('plano_contas.cadastrar');
		Route::get('listar', 'PlanoContasController@Listar')->name('plano_contas.listar');
		Route::post('salvar', 'PlanoContasController@salvar');
		Route::post('get_contas_json', 'PlanoContasController@GetContasJson');
	});

	Route::group(['prefix' => 'lancamentos', 'middleware' => 'auth'], function(){
		Route::get('cadastrar', 'LancamentosController@Cadastrar')->name('lancamentos.cadastrar');
		Route::post('salvar', 'LancamentosController@salvar');
		Route::post('atualizar', 'LancamentosController@Atualizar');
		Route::get('listar', 'LancamentosController@listar')->name('lancamentos.listar');
		Route::post('get_lancamentos_json', 'LancamentosController@GetLancamentosJson');
		Route::get('get_lancamento_json/{id}', 'LancamentosController@GetLancamentoJson');
		Route::get('gera_excel', 'LancamentosController@GeraExcel');
	});
});

Route::group(['prefix' => 'leituras', 'middleware' => 'auth'], function(){
	Route::get('cadastrar', 'LeiturasController@Cadastrar')->name('leituras.cadastrar');
	Route::post('salvar', 'LeiturasController@Salvar');
	Route::get('listar', 'LeiturasController@Listar')->name('leituras.listar');
	Route::get('editar/{id}', 'LeiturasController@Editar')->name('leituras.editar');
	Route::post('atualizar/', 'LeiturasController@Atualizar');
	Route::post('get_leituras_json', 'LeiturasController@GetLeiturasJson');
	Route::get('ficha/{id}', 'LeiturasController@Ficha')->name('leituras.ficha');
	Route::get('gera_excel', 'LeiturasController@GeraExcel');
	Route::get('exporta_tabela', 'LeiturasController@ExportarTabela')->name('leituras.exp_tabela');
	Route::get('ficha/{id}', 'LeiturasController@Ficha')->name('leituras.ficha');
	Route::get('get_infos_skoob/{id_skoob}', 'LeiturasController@GetInfosSkoob');
});

Route::group(['prefix' => 'games', 'middleware' => 'auth'], function(){
	Route::get('cadastrar', 'GamesController@Cadastrar')->name('games.cadastrar');
	Route::post('salvar', 'GamesController@Salvar');
	Route::get('listar', 'GamesController@listar')->name('games.listar');
	Route::get('editar/{id}', 'GamesController@Editar')->name('games.editar');
	Route::post('atualizar/', 'GamesController@Atualizar');
	Route::post('get_games_json', 'GamesController@GetGamesJson');
	Route::get('gera_excel', 'GamesController@GeraExcel');
	Route::get('infos_apis', 'GamesController@InfosApis');
	Route::get('ficha/{id}', 'GamesController@Ficha')->name('games.ficha');
	Route::get('exportar_tabela', 'GamesController@ExportarTabela')->name('games.exp_tabela');
});

Route::group(['prefix' => 'autores', 'middleware' => 'auth'], function(){
	Route::get('listar', 'AutoresController@listar')->name('autores.listar');
	Route::get('cadastrar', 'AutoresController@Cadastrar')->name('autores.cadastrar');
	Route::post('salvar', 'AutoresController@Salvar');
	Route::get('editar/{id}', 'AutoresController@Editar')->name('autores.editar');
	Route::post('atualizar/', 'AutoresController@Atualizar');
	Route::get('editar/{id}', 'AutoresController@Editar')->name('autores.editar');
});

Route::group(['prefix' => 'studios', 'middleware' => 'auth'], function(){
	Route::get('listar', 'StudiosController@listar')->name('studios.listar');
	Route::get('cadastrar', 'StudiosController@Cadastrar')->name('studios.cadastrar');
	Route::post('salvar', 'StudiosController@Salvar');
	Route::get('editar/{id}', 'StudiosController@Editar')->name('studios.editar');
	Route::post('atualizar/', 'StudiosController@Atualizar');
});

Route::group(['prefix' => 'editoras', 'middleware' => 'auth'], function(){
	Route::get('listar', 'EditorasController@listar')->name('editoras.listar');
	Route::get('cadastrar', 'EditorasController@Cadastrar')->name('editoras.cadastrar');
	Route::post('salvar', 'EditorasController@Salvar');
	Route::get('editar/{id}', 'EditorasController@Editar')->name('editoras.editar');
	Route::post('atualizar/', 'EditorasController@Atualizar');
});

Route::group(['prefix' => 'listas_tarefas', 'middleware' => 'auth'], function(){
	Route::group(['prefix' => 'listas'], function(){
		Route::get('cadastrar', 'ListasTarefasController@Cadastrar')->name('listas.cadastrar');
		Route::post('salvar', 'ListasTarefasController@Salvar');
		Route::get('listar', 'ListasTarefasController@listar')->name('listas.listar');
		Route::get('get_lista_json/{id}', 'ListasTarefasController@GetListaJson');
		Route::post('atualizar/', 'ListasTarefasController@Atualizar');
		Route::get('get_lista_c_tarefas_json/{id}', 'ListasTarefasController@GetListaComTarefasJson');
	});
	Route::group(['prefix' => 'categorias'], function(){
		Route::get('cadastrar', 'CategoriaTarefaController@Cadastrar')->name('categorias_tarefa.cadastrar');
		Route::post('salvar', 'CategoriaTarefaController@Salvar');
		Route::get('listar', 'CategoriaTarefaController@Listar')->name('categorias_tarefa.listar');
		Route::get('get_categoria_json/{id}', 'CategoriaTarefaController@GetCategoriaJson');
		Route::post('atualizar/', 'CategoriaTarefaController@Atualizar');
		Route::get('get_categorias_json/', 'CategoriaTarefaController@GetCategoriasJson');
	});
	Route::group(['prefix' => 'tarefas'], function(){
		Route::get('mudarsituacao/{id_tarefa}/{id_situacao}', 'TarefasController@MudarSituacao');
		Route::delete('excluir/{id_tarefa}', 'TarefasController@Excluir');
		Route::post('salvar', 'TarefasController@Salvar');
		Route::get('get_tarefa_json/{id_tarefa}', 'TarefasController@GetTarefaJson');
		Route::post('atualizar/', 'TarefasController@Atualizar');
	});
	Route::group(['prefix' => 'situacoes'], function(){
		Route::get('get_situacoes_json/', 'SituacoesTarefasController@GetSituacoesJson');
	});
});

Route::get('busca/{txt_buscado}', 'BuscaController@BuscaGeral')->middleware('auth');

Route::group(['prefix' => 'assuntos', 'middleware' => 'auth'], function(){
	Route::post('salvar', 'AssuntosController@Salvar');
});

Route::group(['prefix' => 'pesos', 'middleware' => 'auth'], function(){
	Route::get('listar', 'PesosController@Listar')->name('pesos.listar');
	Route::post('adicionar', 'PesosController@Adicionar');
	Route::get('listar_json', 'PesosController@ListarJson');
	Route::delete('excluir/{id}', 'PesosController@Excluir');
});

Route::group(['prefix' => 'exercicios', 'middleware' => 'auth'], function(){
	Route::get('listar', 'ExerciciosController@Listar')->name('exercicios.listar');
});

Route::group(['middleware' => 'auth'], function(){
	Route::get('/', 'HomeController@Index')->name('home');
    //Rotas de registro
    Route::get('register','Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register','Auth\RegisterController@register');
});

//Rotas de autenticação
Route::get('login','Auth\LoginController@showLoginForm')->name('login');
Route::post('login','Auth\LoginController@login');
Route::get('logout','Auth\LoginController@logout')->name('logout');
//Rotas de redefinição de senhas
Route::get('password/reset','Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email','Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}','Auth\ResetPasswordController@showResetForm');
Route::post('password/reset','Auth\ResetPasswordController@reset');

Route::get('/teste', 'TesteController@Teste');